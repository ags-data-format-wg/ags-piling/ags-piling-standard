


# AGS Piling Standard / X. Bored

## X.X. agspileBoredElement

### X.X.1. Object description

Summary construction data for bored piles. The embedded child objects can be used to provide full construction details.

The parent object of [agspileBoredElement](./Standard_Bored_agspileBoredElement.md) is agspileScheduleAsBuilt

[agspileBoredElement](./Standard_Bored_agspileBoredElement.md) contains the following embedded child objects: 

- [agspileBoredProgress](./Standard_Bored_agspileBoredProgress.md)
- [agspileBoredDepth](./Standard_Bored_agspileBoredDepth.md)
- [agspileRftMain](./Standard_Common_agspileRftMain.md)
- [agspileRftShear](./Standard_Common_agspileRftShear.md)
- [agspileIntegrity](./Standard_Common_agspileIntegrity.md)
- [agspileInstallation](./Standard_Common_agspileInstallation.md)
- [agspileNonConformance](./Standard_Common_agspileNonConformance.md)
- [agspileObservation](./Standard_Common_agspileObservation.md)
- [agspileInspection](./Standard_Common_agspileInspection.md)
- [agspileGeology](./Standard_Common_agspileGeology.md)
- [agspileDataValue](./Standard_Common_agspileDataValue.md)

[agspileBoredElement](./Standard_Bored_agspileBoredElement.md) has associations (reference links) with the following objects: 

- [agspileDesignElement](./Standard_Design_agspileDesignElement.md)
- [agspileTest](./Standard_PileTest_agspileTest.md)
- [agsProjectDocumentSet](./Standard_Project_agsProjectDocumentSet.md)

[agspileBoredElement](./Standard_Bored_agspileBoredElement.md) has the following attributes:


- [elementID](#elementid)
- [elementUUID](#elementuuid)
- [elementAssetID](#elementassetid)
- [designElementID](#designelementid)
- [elementCategory](#elementcategory)
- [elementType](#elementtype)
- [isTemporaryCasing](#istemporarycasing)
- [supportFluid](#supportfluid)
- [elementSize](#elementsize)
- [elementDetail](#elementdetail)
- [isInProgress](#isinprogress)
- [infoStatus](#infostatus)
- [group](#group)
- [location](#location)
- [coordinates](#coordinates)
- [chainage](#chainage)
- [offset](#offset)
- [platformElevation](#platformelevation)
- [machineElevation](#machineelevation)
- [castElevation](#castelevation)
- [cutoffElevation](#cutoffelevation)
- [length](#length)
- [bottomElevation](#bottomelevation)
- [casingTopElevation](#casingtopelevation)
- [casingLength](#casinglength)
- [verticalityRake](#verticalityrake)
- [verticalityBearing](#verticalitybearing)
- [verticalityMethod](#verticalitymethod)
- [prebore](#prebore)
- [elementStart](#elementstart)
- [elementEnd](#elementend)
- [elementEndAll](#elementendall)
- [agspileBoredProgress](#agspileboredprogress)
- [agspileBoredDepth](#agspileboreddepth)
- [geologySummary](#geologysummary)
- [agspileGeology](#agspilegeology)
- [baseHardnessGrade](#basehardnessgrade)
- [baseHardnessMethod](#basehardnessmethod)
- [baseHardnessRemarks](#basehardnessremarks)
- [concreteMix](#concretemix)
- [concreteStrength](#concretestrength)
- [concreteDC](#concretedc)
- [volumeDelivered](#volumedelivered)
- [volumeTheoretical](#volumetheoretical)


### X.X.2. Attributes

##### elementID
Pile/wall element reference. Must be unique within a file. May be referenced by other objects, e.g. [agspileTest](./Standard_PileTest_agspileTest.md).  
*Type:* string (identifier)  
*Condition:* Required  
*Example:* ``B1-P001``

##### elementUUID
Universal/global unique identifier (UUID) for the element. This is optional and is not referenced anywhere else in the schema, but it may be beneficial to include this to help with data control and integrity.  
*Type:* string  
*Example:* ``1fb599ab-c040-408d-aba0-85b18bb506c2``

##### elementAssetID
Unique asset ID for the element  
*Type:* string  


##### designElementID
Reference to elementID of agspileDesign object for corresponding pile in design schedule, if applicable  
*Type:* string (reference to elementID of agspileDesign object)  
*Example:* ``B1-P001``

##### elementCategory
Category of element, i.e what it is in general term (not to be confused with type of element).  
*Type:* string (recommend using term from [vocabulary](./Codes_Vocab.md))  
*Example:* ``Bearing pile``

##### elementType
Element (pile/wall) type input as relevant code from list provided.  
*Type:* string (enum from list below)  
\- None  
*Example:* ``BORED``

##### isTemporaryCasing
Idenfity whether temporary casing used (true if yes).  
*Type:* boolean  
*Example:* ``true``

##### supportFluid
Support fluid type. Full details of usage to be included in [agspileBoredProgress](./Standard_Bored_agspileBoredProgress.md).  
*Type:* string  
*Example:* ``Bentonite``

##### elementSize
Nominal diameter or section size/reference, as applicable. Units defined in parent [agspileConstructionSchedule](./Standard_Construction_agspileConstructionSchedule.md) object.  
*Type:* string  
*Example:* ``900``

##### elementDetail
Summary of additional important construction details, .e.g. permanent casing.  
*Type:* string  
*Example:* ``Double sleeve for pile test``

##### isInProgress
Construction status of pile. Set as true if in progress, i.e not yet complete.  
*Type:* boolean  
*Example:* ``false``

##### infoStatus
Status of information relating to pile/wall element  
*Type:* string (recommend using term from [vocabulary](./Codes_Vocab.md))  
*Example:* ``Draft``

##### group
Cap, group or wall reference, as applicable  
*Type:* string  
*Example:* ``PC20``

##### location
Site location subdivision (within project) reference  
*Type:* string  
*Example:* ``Bridge B1``

##### coordinates
As built plan coordinates of the element, as a None (x,y). Coordinate system defined in parent object.  
*Type:* array (None)  
*Example:* ``[565.25,421.09]``

##### chainage
Chainage. Reference alignment defined in parent [agspileConstructionSchedule](./Standard_Construction_agspileConstructionSchedule.md) object.  
*Type:* string  
*Example:* ``12+345.67``

##### offset
Offset from alignment used for chainage. Alignment used defined in parent object.  
*Type:* string  
*Example:* ``+10.7``

##### platformElevation
Elevation of working/piling platform (commencing surface)  
*Type:* number  
*Example:* ``16.15``

##### machineElevation
Elevation of working surface on which base machine stands, if different from elevationPlatform  
*Type:* number  
*Example:* ``16.6``

##### castElevation
Elevation of pile/wall element head level as constructed, before trimming, i.e casting level  
*Type:* number  
*Example:* ``16.05``

##### cutoffElevation
Elevation of pile/wall element cut-off level as constructed  
*Type:* number  
*Example:* ``15.5``

##### length
Length below cut-off level (measured along pile)  
*Type:* number  
*Example:* ``20.3``

##### bottomElevation
Elevation of the bottom/toe of the pile  
*Type:* number  
*Example:* ``-10.7``

##### casingTopElevation
Elevation of top of casing when installed to full depth  
*Type:* number  
*Example:* ``17.35``

##### casingLength
Length of casing (installed to full depth).  
*Type:* number  
*Example:* ``12``

##### verticalityRake
Actual verticality or gradient of raked pile (as H/V from vertical)  
*Type:* string  
*Example:* ``1/124``

##### verticalityBearing
Bearing of non vertical pile (degrees from project grid y/N axis)  
*Type:* number  
*Example:* ``135``

##### verticalityMethod
Method of verticality measurement  
*Type:* string  
*Example:* ``Rig mast survey``

##### prebore
Pre-bore summary details (or other relevant prior operation)  
*Type:* string  
*Example:* ``10m at 600 dia``

##### elementStart
Start date/time of element construction (excluding pre-boring)  
*Type:* string (Nonetime)  
*Example:* ``2016-11-23T10:20``

##### elementEnd
End date/time of element (excluding trimming/testing)  
*Type:* string (Nonetime)  
*Example:* ``2016-11-23T17:55``

##### elementEndAll
End date of element (all operations)  
*Type:* string (Nonetime)  
*Example:* ``2016-12-12T12:30``

##### agspileBoredProgress
Array of embedded [agspileBoredProgress](./Standard_Bored_agspileBoredProgress.md) objects. Used to provide time based summary of construction progress.  
*Type:* array (of [agspileBoredProgress](./Standard_Bored_agspileBoredProgress.md) objects)  


##### agspileBoredDepth
Array of embedded [agspileBoredDepth](./Standard_Bored_agspileBoredDepth.md) objects. Used to providedepth based description of the pile as constructed.  
*Type:* array (of [agspileBoredDepth](./Standard_Bored_agspileBoredDepth.md) objects)  


##### geologySummary
Summary of ground conditions encountered. Optional if full details provided in agspileCommonGeology  
*Type:* string  
*Example:* ``London Clay at 7.8m``

##### agspileGeology
Array of embedded [agspileGeology](./Standard_Common_agspileGeology.md) objects. May be used to record ground conditions encountered during element construction.  
*Type:* array (of [agspileGeology](./Standard_Common_agspileGeology.md) objects)  


##### baseHardnessGrade
Base hardness grade inaccordance with assessment criteria deifned in SPERWall3 Table C3.1. If other criteria used, decribe in baseHardnessRemarks  
*Type:* string  
*Example:* ``2``

##### baseHardnessMethod
Method of assesmsent of base hardness. If necessary, full description of test and observation arising may be inlcuded as an [agspileInspection](./Standard_Common_agspileInspection.md) record.  
*Type:* string  
*Example:* ``Weighted tape, after base cleaning``

##### baseHardnessRemarks
Further remarks on base hardness observed.  
*Type:* string  


##### concreteMix
Concrete mix designation  
*Type:* string  
*Example:* ``C40-1``

##### concreteStrength
Concrete strength class  
*Type:* string  
*Example:* ``C32/40``

##### concreteDC
Concrete DC class  
*Type:* string  
*Example:* ``DC-2``

##### volumeDelivered
Total volume of concrete delivered into the pile. Units: m3.  
*Type:* string  
*Example:* ``52``

##### volumeTheoretical
Theoretical volume of pile. Top of pile assumed should be same as that  used for volumeDelivered. Units: m3.  
*Type:* string  
*Example:* ``51.35``

##### overbreak
Pile overbreak: excess of volume delivered over theoretical as percentage. Units: %.  
*Type:* string  
*Example:* ``1.27``

##### agspileBoredPourRecord
Array of embedded [agspileBoredPourRecord](./Standard_Bored_agspileBoredPourRecord.md) objects used to provide detailed time based log of the concrete pour.  
*Type:* array (of [agspileBoredPourRecord](./Standard_Bored_agspileBoredPourRecord.md) objects)  


##### rftTopElevation
Elevation of top of main reinforcement  
*Type:* number  
*Example:* ``16.7``

##### rftProjection
Reinforcement projection above cut-off level. Units as for reinforcement length.  
*Type:* number  
*Example:* ``1.2``

##### rftCover
Nominal minimum cover to reinforcement. Units: mm  
*Type:* number  
*Example:* ``75``

##### rftCageReference
Reinforcement cage reference.  
*Type:* string  
*Example:* ``A``

##### rftMainDescription
Description of main (longitudinal) reinforcement. Not required if full details provided in [agspileRftMain](./Standard_Common_agspileRftMain.md).  
*Type:* string  
*Example:* ``12xB25, 10m length incl 1.2m projection above COL``

##### rftShearDescription
Description of shear reinforcement. Not required if full details provided in [agspileRftShear](./Standard_Common_agspileRftShear.md).  
*Type:* string  
*Example:* ``B12@200``

##### agspileRftMain
Array of embedded agspileMainRft objects that fully define the main (longitudinal) reinforcement as built  
*Type:* object (of [agspileRftMain](./Standard_Common_agspileRftMain.md) objects)  


##### agspileRftShear
Array of embedded [agspileRftShear](./Standard_Common_agspileRftShear.md) objects that fully define the shear reinforcement as built  
*Type:* object (of [agspileRftShear](./Standard_Common_agspileRftShear.md) objects)  


##### installationSummary
Instrumentation or other installation type/summary. Optional if full details provided in agspileCommonInstallation  
*Type:* string  
*Example:* ``Inclinometer duct``

##### agspileInstallation
Array of embedded [agspileInstallation](./Standard_Common_agspileInstallation.md) objects. May be used for instrumentation or any other types of installation.  
*Type:* array (of [agspileInstallation](./Standard_Common_agspileInstallation.md) objects)  


##### integritySummary
Integrity testing type/summary. Optional if full details provided in agspileCommonIntegrity  
*Type:* string  
*Example:* ``Transient dynamic``

##### agspileIntegrity
Array of embedded [agspileIntegrity](./Standard_Common_agspileIntegrity.md) objects. Allows for more than one integrity test per pile, e.g. where retest required.  
*Type:* array (of [agspileIntegrity](./Standard_Common_agspileIntegrity.md) objects)  


##### observationSummary
Summary of inspections or observations made during construction. Optional if full details provided in [agspileObservation](./Standard_Common_agspileObservation.md)  
*Type:* string  
*Example:* ``Seepage at 4.5m``

##### agspileObservation
Array of embedded [agspileObservation](./Standard_Common_agspileObservation.md) objects. May be used to record observations made during element construction.  
*Type:* array (of [agspileObservation](./Standard_Common_agspileObservation.md) objects)  


##### inspectionSummary
Summary of inspections or observations made during construction. Optional if full details provided in [agspileInspection](./Standard_Common_agspileInspection.md)  
*Type:* string  
*Example:* ``Pile base check: soild hard base``

##### agspileInspection
Array of embedded [agspileInspection](./Standard_Common_agspileInspection.md) objects. May be used to record inspections undertaken during element construction.  
*Type:* array (of [agspileInspection](./Standard_Common_agspileInspection.md) objects)  


##### nonConformanceSummary
Non-conformance report reference or summary description. Optional if full details provided in agspileCommonNonConformance  
*Type:* string  
*Example:* ``NCR/P/007``

##### agspileNonConformance
Array of embedded [agspileNonConformance](./Standard_Common_agspileNonConformance.md) objects  
*Type:* array (of [agspileNonConformance](./Standard_Common_agspileNonConformance.md) objects)  


##### agspileDataValue
Array of embedded [agspileDataValue](./Standard_Common_agspileDataValue.md) objects. May be used to provide additional project specific metadata for the pile, or user defined data.  
*Type:* array (of [agspileDataValue](./Standard_Common_agspileDataValue.md) objects)  


##### documentSetID
Reference to documentation relating to element (reference to [agsProjectDocumentSet](./Standard_Project_agsProjectDocumentSet.md) object), e.g. pile record sheets for this element.  
*Type:* string (reference to documentSetID of [agsProjectDocumentSet](./Standard_Project_agsProjectDocumentSet.md) object)  
*Example:* ``Ph1PileRecords_B1_P001``

##### remarks
Additional remarks, if required.  
*Type:* string  
*Example:* ``Some additional remarks``

