


# AGS Piling Standard / X. Construction

## X.X. agspileConstructionSchedule

### X.X.1. Object description

Container for as built pile construction data, i.e parent for the pile/element objects and materials. Includes metadata for the construction activity and the schedule itself, if applicable. Also used to define coordinate system and units for section size.

The parent object of [agspileConstructionSchedule](./Standard_Construction_agspileConstructionSchedule.md) is [root](./Standard_Root_Intro.md)

[agspileConstructionSchedule](./Standard_Construction_agspileConstructionSchedule.md) contains the following embedded child objects: 

- #REF!

[agspileConstructionSchedule](./Standard_Construction_agspileConstructionSchedule.md) has associations (reference links) with the following objects: 

- [agsProjectCoordinateSystem](./Standard_Project_agsProjectCoordinateSystem.md)
- [agsProjectDocumentSet](./Standard_Project_agsProjectDocumentSet.md)

[agspileConstructionSchedule](./Standard_Construction_agspileConstructionSchedule.md) has the following attributes:


- [constructionID](#constructionid)
- [constructionName](#constructionname)
- [description](#description)
- [coordSystemID](#coordsystemid)
- [alignmentDescription](#alignmentdescription)
- [sectionSizeUnits](#sectionsizeunits)
- [organisation](#organisation)
- [suppliers](#suppliers)
- [agspileBoredElement](#agspileboredelement)
- [agspileCFAElement](#agspilecfaelement)
- [agspileCFAPourRecord](#agspilecfapourrecord)
- [agspileCFACode](#agspilecfacode)
- [agspileTest](#agspiletest)
- [agspileConcrete](#agspileconcrete)
- [agspileFluid](#agspilefluid)
- [agspileDataValue](#agspiledatavalue)
- [documentSetID](#documentsetid)
- [remarks](#remarks)


### X.X.2. Attributes

##### constructionID
Reference or idenfitfer for the construction activity or schedule. This is optional and is not referenced anywhere else in the schema, but it is beneficial to include this to help with data control and integrity, and some software/applications may require it. If used, must be unique within the file.  
*Type:* string (identifier)  
*Condition:* Recommended  
*Example:* ``Ph1``

##### constructionName
Short name of construction activity or schedule.  
*Type:* string  
*Condition:* Recommended  
*Example:* ``Phase 1``

##### description
Further description, if required.  
*Type:* string  
*Example:* ``As built piles from Phase 1, November-December 2021``

##### coordSystemID
Reference to coordinate system applicable to this object including child objects (agsProjectCoordinateSystem object). Coordinate system defines datum for elevations and units for pile length and depth.  
*Type:* string (reference to systemID of [agsProjectCoordinateSystem](./Standard_Project_agsProjectCoordinateSystem.md) object)  
*Example:* ``MetroXYZ``

##### alignmentDescription
Description of alignment used to define chainage and offset in child pile element objects  
*Type:* string  


##### sectionSizeUnits
Units for section size used for piles  
*Type:* string (units)  
*Example:* ``mm``

##### organisation
Piling contractor  
*Type:* string  


##### suppliers
Suppliers and subcontractors. Input list as text string, not an array.  
*Type:* string  


##### agspileBoredElement
Array of embedded [agspileBoredElement](./Standard_Bored_agspileBoredElement.md) objects  
*Type:* array (agspileBoredElement objects)  


##### agspileCFAElement
Array of embedded [agspileCFAElement](./Standard_CFA_agspileCFAElement.md) objects  
*Type:* array (agspileCFAElement objects)  


##### agspileCFAPourRecord
Array of embedded [agspileCFAPourRecord](./Standard_CFA_agspileCFAPourRecord.md) objects  
*Type:* array (agspileCFAPourRecord objects)  


##### agspileCFACode
Array of embedded [agspileCFACode](./Standard_CFA_agspileCFACode.md) objects  
*Type:* array (agspileCFACode objects)  


##### agspileTest
Array of embedded [agspileTest](./Standard_PileTest_agspileTest.md) objects  
*Type:* array (agspileTest objects)  


##### agspileConcrete
Array of embedded [agspileConcrete](./Standard_Material_agspileConcrete.md) objects  
*Type:* array (agspileConcrete objects)  


##### agspileFluid
Array of embedded [agspileFluid](./Standard_Material_agspileFluid.md) objects  
*Type:* array (agspileFluid objects)  


##### agspileDataValue
Array of embedded [agspileDataValue](./Standard_Common_agspileDataValue.md) objects which may typically be used to provide project specific metadata for the activity or schedule.  
*Type:* array (agspileDataValue objects)  


##### documentSetID
Reference to documentation relating to schedule (reference to [agsProjectDocumentSet](./Standard_Project_agsProjectDocumentSet.md) object), e.g. pile record sheets.  
*Type:* string (reference to documentSetID of [agsProjectDocumentSet](./Standard_Project_agsProjectDocumentSet.md) object)  
*Example:* ``Ph1PileRecords``

##### remarks
Additional remarks, if required.  
*Type:* string  
*Example:* ``Some additional remarks``

