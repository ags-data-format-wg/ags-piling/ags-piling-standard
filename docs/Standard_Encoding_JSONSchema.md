# AGS Piling Standard / X. Encoding

*This is a placeholder page, copied from AGSi. Requires review and edit to make it suitable for AGS Piling.*

## X.3. JSON Schema

<a href="https://json-schema.org/" target="_blank">JSON Schema</a>
provides a method for describing a schema that may be used for validation. Validation tools using JSON Schema are available online.

A JSON Schema for this version of AGSi is provided in support this standard. It can be downloaded from <a href="../schema/AGSi_JSONSchema_v1-0-0.zip" target="_blank">../schema/AGSi_JSONSchema_v1-0-0.zip</a>

This JSON Schema is based on version
<a href="https://datatracker.ietf.org/doc/draft-handrews-json-schema/" target="_blank">Draft 2019-09 of the JSON Schema standard</a> (formerly known as draft-08).

!!! Note
    A later version of JSON Schema, Draft 2020-12, has been published. However, at the time of writing this was not well supported by the validation tools. Therefore at this stage only version 2019-09 is supported by AGSi.

In the event of any conflict between the JSON Schema file provided and this standard, the standard shall take precedence.

Validation against the JSON schema will normally include the following:

* JSON syntax
* Object names and their relationships
* Attribute names
* Attribute data types
* Required objects or attributes
* Additional format rules for strings, e.g. date/time format
* Specific conditional requirements of the schema, e.g. one or other of two fields must be populated

There are some aspects that are critical to the correct formation of an AGSi file that
will **not** generally be checked by general purpose JSON Schema validation software.
These include:

* Cross-check on identifiers used for reference links between objects
* Check that supporting files are accessible
