# AGS Piling Standard / 1. General

*This is a placeholder page, copied from AGSi. Requires review and edit to make it suitable for AGS Piling.*

## 1.8. Transfer of AGSi data

The following is based on file based transfer of AGSi data, which is expected to be the predominant method of sharing AGSi.

!!! Note
    It is recognised that systems for sharing data that are not file based are emerging. No particular provisions for this are given herein, but it is recommended that the general principles given below should be applied where relevant. Further guidance may be added at a later date.

### 1.8.1. AGSi file and package

When AGSi data is transferred or shared, it should comprise:

- One AGSi data file (the
  [**AGSi file**](./Standard_General_Definitions.md#agsi-file))
- One [index file](#182-index-file)
- [Supporting files](./Standard_General_Definitions.md#agsi-supporting-files) and
[documents](./Standard_General_Definitions.md#agsi-document),
as required ([included files](./Standard_General_Definitions.md#agsi-included-files))

The combination of the above is referred to in this standard as the
[**AGSi package**](./Standard_General_Definitions.md#agsi-package).

The [AGSi file](./Standard_General_Definitions.md#agsi-file)
shall have the file extension **.json**.

The [index file](#182-index-file) shall be named **index.json**.

For file based transfer, it is recommended that the files making up an
[AGSi package](./Standard_General_Definitions.md#agsi-package) are contained with a single archive file using either the *ZIP* or *7z* archive file formats.

The package (archive) file should have the file extension **.agsi**

!!! Note
    It is recommended that you update your file associations so that *.agsi* files can be opened with your prefered unzip application.

    If you are struggling to open an *.agsi* file as a zip file in Windows, a workaround is to add a further extension *.zip* onto the filename. This should allow it to be recognised as a zip file and the relevant extraction tools should now be available.  


### 1.8.2. Index file

Each AGSi package should have an index file which shall be named **index.json**. There shall be only one index file per [AGSi package](./Standard_General_Definitions.md#agsi-package).

The index file contains metadata including the name of the [AGSi file](./Standard_General_Definitions.md#agsi-file), the applicable version of AGSi used and, optionally, the title of the AGSi package.

!!! Note
    The main purpose of the index file is to provide single entry point for software or scripts that wish to access the AGSi file and package.
    Such software/scripts may use the information provided within it to carry out checks or other processes before going on to read the package data.

The index file shall comprise a single (root) object with an attribute named **agsiIndex**.
This attribute shall contain a single object with the following attributes (all text string type):

- **filePath**: file name and, if applicable, file path to the [AGSi file](./Standard_General_Definitions.md#agsi-file)
- **schemaVersion**: revision of the AGSi schema used by the [AGSi file](./Standard_General_Definitions.md#agsi-file) , e.g. `1.0.0`
- **title**: name or title of the [AGSi package](./Standard_General_Definitions.md#agsi-package); this is optional

Template for index file:


``` json
{
    "agsiIndex": {
        "filePath": "your_agsi_file_name.json",
        "schemaVersion": "1.0.0",
        "title": "Your package title"
    }
}
```

### 1.8.3. Supporting files

[Supporting files](./Standard_General_Definitions.md#agsi-supporting-files)
include the following:

- **geometry** files, as referenced by
  [agsiGeometryFromFile](./Standard_Geometry_agsiGeometryFromFile.md)
  objects
- **data** files, as referenced by
  [agsiDataPropertyFromFile](./Standard_Data_agsiDataPropertyFromFile.md)
    objects

Geometry and data supporting files shall be either
[included](./Standard_General_Definitions.md#agsi-included-files)
within the [AGSi package](./Standard_General_Definitions.md#agsi-package)
or hosted in an
[external](./Standard_General_Definitions.md#agsi-external-files)
resource with full [URI](./Standard_General_Definitions.md#uri) or
[URI-reference](./Standard_General_Definitions.md#uri-reference)
for that link provided in the relevant linking object attribute.

It is recommended that geometry and data supporting files are
[included](./Standard_General_Definitions.md#agsi-included-files)
within the [AGSi package](./Standard_General_Definitions.md#agsi-package).
The following shall be taken into account before considering the alternative of using an
[external](./Standard_General_Definitions.md#agsi-external-files)
resource:

- version control of the supported files, i.e. are the linked files live documents/data that may change over time
- ability of users, and potential future users, to access to the host resource
- barrier to long term legacy use when host resource may not be accessible

When geometry and data files are
[included](./Standard_General_Definitions.md#agsi-included-files)
within the [AGSi package](./Standard_General_Definitions.md#agsi-package),
the [URI-reference](./Standard_General_Definitions.md#uri-reference)
links provided shall be local relative links to the included files, not
[URI](./Standard_General_Definitions.md#uri)
to the external resource location.

### 1.8.4. Documents

[Documents](./Standard_General_Definitions.md#agsi-document)
are files that may include documents, data files, drawings, (BIM) models etc., identified and referenced as document objects ([agsProjectDocument](./Standard_Project_agsProjectDocument.md)) in the
[AGSi file](./Standard_General_Definitions.md#agsi-file).


It is recommended that
[documents](./Standard_General_Definitions.md#agsi-document) are
[included](./Standard_General_Definitions.md#agsi-included-files)
within the [AGSi package](./Standard_General_Definitions.md#agsi-package)
if there are any concerns about user access (current and future) to the alternative of using an
[external](./Standard_General_Definitions.md#agsi-external-files)
resource.

For [documents](./Standard_General_Definitions.md#agsi-document),
it is possible to provide both a local relative
([URI-reference](./Standard_General_Definitions.md#uri-reference))
link to
[included files ](./Standard_General_Definitions.md#agsi-included-files)
and a
[URI](./Standard_General_Definitions.md#uri) to the external resource location using the relevant object attributes.

### 1.8.5. Recommended folder structure

The [AGSi file](./Standard_General_Definitions.md#agsi-file)
should be in the root of the package folder with all included files in subfolders.
Recommended names and usage of the subfolders is shown below:

![Recommended folder structure](./images/General_Transfer_Folders.svg)

Further division into subfolders below the above may be determined by the user(s).
Additional folders may be added to the root if appropriate.
