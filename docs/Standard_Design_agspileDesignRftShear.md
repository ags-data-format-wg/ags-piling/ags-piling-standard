


# AGS Piling Standard / X. Design

## X.X. agspileDesignRftShear

### X.X.1. Object description

Design pile shear reinforcement. May be one object per pile covering the entire depth for a simple cage. For cages with shear links that vary with depth, use separate sections for each change. Adjacent sections should not normally overlap. Segmental precast piles should have one object per segment. Reference elevation that depth is measured from is defined in parent [agspileDesignRft](./Standard_Design_agspileDesignRft.md) object.

The parent object of [agspileDesignRftShear](./Standard_Design_agspileDesignRftShear.md) is agspileDesign

[agspileDesignRftShear](./Standard_Design_agspileDesignRftShear.md) has the following attributes:


- [topDepth](#topdepth)
- [bottomDepth](#bottomdepth)
- [shearBar](#shearbar)
- [shearPitch](#shearpitch)
- [shearType](#sheartype)
- [shearDetails](#sheardetails)
- [remarks](#remarks)


### X.X.2. Attributes

##### topDepth
Depth to top of section. Reference elevation for depth defined in parent object.  
*Type:* number  
*Example:* ``1.2``

##### bottomDepth
Depth to bottom of section. Reference elevation for depth defined in parent object.  
*Type:* number  
*Example:* ``10``

##### shearBar
Shear reinforcement bar type and size  
*Type:* string  
*Example:* ``B12``

##### shearPitch
Shear reinforcement pitch. Units: mm  
*Type:* number  
*Example:* ``200``

##### shearType
Shear reinforcement type (helical/link)  
*Type:* string (recommend using term from [vocabulary](./Codes_Vocab.md))  
*Example:* ``Link``

##### shearDetails
Shear reinforcement special additional details  
*Type:* string  


##### remarks
Additional remarks, if required.  
*Type:* string  
*Example:* ``Some additional remarks``

