


# AGS Piling Standard / X. PileTest

## X.X. agspileTestCode

### X.X.1. Object description

Heading code definitions used by DataBlock objects for load-movement and instrumentation data.  These may be linked to a specific instrument (agspileTestInstrument) if required. May also be used for derived values. For general purpose codes that are not specific to a pile, e.g. DateTime, [agsProjectCode](./Standard_Project_agsProjectCode.md) may be used instead. If there is conflict, codes defined here should take precedence over [agsProjectCode](./Standard_Project_agsProjectCode.md).

The parent object of [agspileTestCode](./Standard_PileTest_agspileTestCode.md) is [agspileTest](./Standard_PileTest_agspileTest.md)

[agspileTestCode](./Standard_PileTest_agspileTestCode.md) has associations (reference links) with the following objects: 

- DataBlock objects for Load-Movement and Instrument Data
- [agspileTestInstrument](./Standard_PileTest_agspileTestInstrument.md)

[agspileTestCode](./Standard_PileTest_agspileTestCode.md) has the following attributes:


- [codeID](#codeid)
- [description](#description)
- [instrumentID](#instrumentid)
- [dataType](#datatype)
- [dataRequirement](#datarequirement)
- [units](#units)
- [signConvention](#signconvention)
- [remarks](#remarks)


### X.X.2. Attributes

##### codeID
Code referenced by codeID attribute of DataBlock objects for load-movement and instrumentation data. See target DataBlock object descriptions for recommendations on codes.  
*Type:* string (identifier)  
*Condition:* Required  
*Example:* ``Gauge1``

##### description
Short description of what the code represents. Include location data, if applicable, if relevant instrument is not referenced (using instrumentID below).  
*Type:* string  
*Condition:* Required  
*Example:* ``Strain at Gauge 1 @ +9.55/A``

##### instrumentID
Instrument from which the reading is taken or derived, identifed by reference to instrumentID of relevant [agspileTestInstrument](./Standard_PileTest_agspileTestInstrument.md) object.  
*Type:* string (reference to instrumentID of [agspileTestInstrument](./Standard_PileTest_agspileTestInstrument.md) object)  
*Example:* ``PT1_gauge1``

##### dataType
Recommended data type for value to be provided  
*Type:* string  
*Example:* ``Number``

##### dataRequirement
Recommended format or other data requirement  
*Type:* string  


##### units
Units of measurement, if applicable.  
*Type:* string  
*Example:* ``u{strain}``

##### signConvention
Sign convention, if this requires clarification  
*Type:* string  
*Example:* ``Compression +ve``

##### remarks
Additional remarks, if required. If a derived value is being reported, this may be used to provide further details of the calculation.  
*Type:* string  
*Example:* ``Some additional remarks``

