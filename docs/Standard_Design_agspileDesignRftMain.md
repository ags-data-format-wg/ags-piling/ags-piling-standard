


# AGS Piling Standard / X. Design

## X.X. agspileDesignRftMain

### X.X.1. Object description

Design intent for main (longitudinal) reinforcement. This may leave details to be confirmed by contractor, e.g. joint/lap requirements. Separate sections should be used if the main reinforcement changes with depth. If lapped or jointed bars are required, these may be covered in one section object per pile or separate sections may be used for each bar length (sections may overlap to represent laps). Lap or joint requirements covered in parent object.  Segmental precast piles should have one section object per segment.  Reference elevation that depth is measured from is defined in parent [agspileDesignRft](./Standard_Design_agspileDesignRft.md) object.

The parent object of [agspileDesignRftMain](./Standard_Design_agspileDesignRftMain.md) is [agspileDesignRft](./Standard_Design_agspileDesignRft.md)

[agspileDesignRftMain](./Standard_Design_agspileDesignRftMain.md) has the following attributes:


- [topDepth](#topdepth)
- [bottomDepth](#bottomdepth)
- [mainNumber](#mainnumber)
- [mainBar](#mainbar)
- [mainDetails](#maindetails)
- [remarks](#remarks)


### X.X.2. Attributes

##### topDepth
Depth to top of section. Reference elevation for depth defined in parent object. Typically the top of the main bars for this section, but sections may sometimes be required to indicate a change in number of bars in the section, e.g. some bars curtailed. May lie above bottom of previous bar if lapped., or may be negative for topmost bars if depth mesaured from cut-off level.  
*Type:* number  
*Example:* ``-1.2``

##### bottomDepth
Depth to bottom of section. Reference elevation for depth defined in parent object. Typically the bottom of the main bars for this section, but sections may sometimes be required to indicate a change in number of bars in the section, e.g. some bars curtailed.  
*Type:* number  
*Example:* ``8.8``

##### mainNumber
Number of main (longitudinal) bars  
*Type:* number  
*Example:* ``12``

##### mainBar
Type and size of main (longitudinal) bars  
*Type:* string  
*Example:* ``B25``

##### mainDetails
Main (longitudinal) reinfocement additional details  
*Type:* string  


##### remarks
Additional remarks, if required.  
*Type:* string  
*Example:* ``Some additional remarks``

