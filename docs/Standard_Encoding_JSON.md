# AGS Piling Standard / X. Encoding

*This is a placeholder page, copied from AGSi. Requires review and edit to make it suitable for AGS Piling.*

## X.2. JSON encoding

### X.2.1. JSON standard and version

AGSi may be encoded as JSON (Javascript Object Notation) for
file based transfer.

JSON encoding of AGSi shall comply with the following version of the JSON standard:

* <a href="https://www.iso.org/standard/71616.html" target="_blank">ISO/IEC 21778:2017 Information technology — The JSON data interchange syntax</a>

!!! Note

    Alternative versions of the JSON standard can be found at:

    * <a href="https://tools.ietf.org/html/rfc8259" target="_blank">IETF RFC8259 (December 2017)</a>
    * <a href="http://www.ecma-international.org/publications/files/ECMA-ST/ECMA-404.pdf" target="_blank">ECMA-404 (December 2017)</a>

    It is commonly reported that the above standards are consistent with each other and the ISO/IEC version.

A general introduction to JSON is provided in the
<a href="https://ags-data-format-wg.gitlab.io/AGSi_Documentation/Guidance_Encoding_Json">Guidance</a>.
A comprehensive guide can be found at
<a href="https://www.json.org/json-en.html" target="_blank">www.json.org</a>


### X.2.2. General requirements for AGSi encoded as JSON

AGSi objects shall be represented as JSON objects, and AGSi arrays as JSON arrays.

The AGSi schema includes nested objects. JSON encoding of AGSi shall also follow a nested approach, i.e. objects within objects, where required by the schema.

The file created shall have the extension **.json**.

### X.2.3. AGSi Objects

AGSi objects shall be encoded as JSON objects with the attribute names and values mapping to JSON object names and values:

``` json
    { "attribute1" : "value1" , "attribute2" : "value2" }
```

The topmost level of the AGSi schema is the
[root schema](./Standard_Root_Intro.md)
which comprises a single object, the root object.
The entirety of an AGSi data set is contained within this single object, thus:

``` json
    {  ... everything goes in this single object ... }  
```

Where a **child object** is embedded within a **parent object**,
the child object becomes the value for the relevant parent object attribute.

!!! Note
    AGSi objects have the namespace prefix *ags* or *agsi* to make them easier to identify

The convention adopted in AGSi naming is such that the parent attribute name is often the child object class name:

``` json
    { "agsiParentObjectName" :
        { "parentAttribute1" : "a value" ,
          "parentAttribute2" : "a value" ,
          "agsiChildObjectName" :
              { "childAttribute1" : "a value" , "childAttribute2" : "a value"}
         }
     }
```

### X.2.4. AGSi arrays

All AGSi arrays, which may comprise values, objects or other arrays, shall be encoded as JSON arrays:

``` json
    { "listAttribute" : [ "this" , "is", "a", "list", "of", 7, "values" ] },

    { "agsChildObjectName" :
        [
              { "childAttributeID" : "1st child object" } ,
              { "childAttributeID" : "2nd child object" } ,
              { "childAttributeID" : "3rd child object" }
         ]
    }
```

Where the schema specifies an array type for an attribute,
the data must be encoded as an array, even if the data requires only one value (or object).
Replacing the array with a single value/object is not permitted.
For example:

``` json
    { "arrayAttribute" : [ "array with one item is correct" ] }

    { "arrayAttribute" : "replacing array with a string is incorrect" }
```

Conversely, in some cases the schema asks for data
that represents a list to be input as a single text
string, not an array.
In such cases, use of an array is not permitted

!!! Note
    The schema generally requires arrays where identification
    of the separate values may be critical to correct reading, parsing or understanding of the data.

    Lists as simple text string lists are used for attributes that carry
    metadata that does not generally need to be parsed to a list of items,
    e.g. *[producerSuppliers](./Standard_Project_agsProject.md#producersuppliers)* in
    [agsProject](./Standard_Project_agsProject.md).

    The object reference pages in this standard make it clear whether an array is required or not.


### 8.2.5. Data types and format

JSON data types shall be the same as the AGSi schema
data types, which replicate the
<a href="https://ags-data-format-wg.gitlab.io/AGSi_Documentation/Guidance_Encoding_Json#json-data-types">allowable JSON data types</a>.

JSON encoding for boolean and null values is as follows (quotemarks not required):

``` json
    { "this is boolean true" : true ,
      "this is boolean false" : false ,
      "and this is a null value" : null }
```

Numbers in JSON must comply with the JSON standard, whose requirements are similar to those used in most programming languages.

Numbers using scientific notation are acceptable in JSON encoding. The following formatting rules apply:

- `e` or `E` is used represent `x10^`
- leading zeroes in the exponent value are permitted
- positive exponents may be prefixed with `+`, but do not have to be
- spaces either side of the `e` or `E` are not permitted.

Allowable number formats include:

``` json
    [ 1, 0.345, -2.54, 999.99, 1.2e9, 1.2e+09, 1.2E-9]
        Not valid: [ .22, 007, +2.54, 1.2 e 9 ]
```

Where an array is specified in the standard, the data type for all array items shall be assumed to be string unless otherwise specifed.

Where additional formatting is specified in the standard, e.g. date,
this additional formatting shall be applied to the data
in accordance with the provisions in
[1.6. Data input formats](./Standard_General_Formats.md).
Validation of data against these additional formatting
rules is possible using the JSON Schema for AGSi 
([8.3. JSON Schema](./Standard_Encoding_JSONSchema.md))  

To comply with the JSON standard
certain characters are reserved and cannot be used in JSON string (text) data without being properly escaped. They include the following:

* Newline to be replaced with ` \n `
* Carriage return to be replaced with ` \r `
* Tab to be replaced with ` \t `
* Double quote to be replaced with ` \" `
* Backslash to be replaced with ` \\ `

Note that **a single quote/apostrophe ` ' ` should not be escaped**.

!!! Note
    Attention is drawn to line breaks (ASCII chr(10))
    which should be encoded as ` \n `.   

### 8.2.6. Required fields and null values

Where the AGSi schema specifies that an object and/or attribute is **required**, then this attribute must
be included in the encoded data set.
Generally this attribute will be required to have
a value (or object) defined, but in some cases  a null value or blank string value may be acceptable, as specified in the schema documentation.


For attributes that are not specified as required,
where the value of an attribute is **null or blank**, or there is no object to embed, then
the attribute name should be omitted from the data for the relevant object instance.

Use of the *null* data type for null or blank data is not permitted unless otherwise stated in the standard (object reference).

For string data, use of a blank string value, i.e: ``""``, is permitted as an alternative to omission, e.g.

``` json
    { "blankAttributeName" : "" }
```
