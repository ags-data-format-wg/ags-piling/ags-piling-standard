


# AGS Piling Standard / X. Design

## X.X. agspileDesignLoadAction

### X.X.1. Object description

Used to define the actions or components of load applied in the design. For Eurocode design it is recommended that both the characteristic values of individual actions and design values of the combined action (after application or partial factors) are included. Combinations used should be defined using agspileDesignCombination. This object can be used to carry the information provided in FPS e-pile schedule format. May be used with other design codes provided that the codes are fully defined using [agsProjectCodeSet](./Standard_Project_agsProjectCodeSet.md) and [agsProjectCode](./Standard_Project_agsProjectCode.md).

The parent object of [agspileDesignLoadAction](./Standard_Design_agspileDesignLoadAction.md) is [agspileDesignLoad](./Standard_Design_agspileDesignLoad.md)

[agspileDesignLoadAction](./Standard_Design_agspileDesignLoadAction.md) has the following attributes:


- [actionID](#actionid)
- [direction](#direction)
- [codeID](#codeid)
- [value](#value)
- [units](#units)
- [remarks](#remarks)


### X.X.2. Attributes

##### actionID
Identifier for this action or load component. May be local to this file or a UUID as required/specified. This is optional and not referenced anywhere else in the schema, but it may be beneficial to include this to help with data control and integrity, and some software/applications may require it.  
*Type:* string  


##### direction
Direction/type of load (vertical, horizontal, moment). Sign convention defined in relevant [agspileDesignSchedule](./Standard_Design_agspileDesignSchedule.md) object.  
*Type:* string (enum from list below)  
\- None  
*Example:* ``Vertical``

##### codeID
Code that identifies the action or load component. Codes should be defined in either an [agsProjectCode](./Standard_Project_agsProjectCode.md) object, or in the code dictionary defined in the relevant [agsProjectCodeSet](./Standard_Project_agsProjectCodeSet.md) object. A code dictionary is available covering codes typically used by Eurocde 7 and the FPS e-pile schedule.  
*Type:* string (Reference to codeID of relevant [agsProjectCode](./Standard_Project_agsProjectCode.md) object, or code defined in codelist specified in relevant [agsProjectCodeSet](./Standard_Project_agsProjectCodeSet.md) object)  
*Example:* ``SetB_DA1_Comb1_ULSSTR_Ed_max``

##### value
Value of action or load component.  
*Type:* number  
*Example:* ``1250``

##### units
Units for value of action or load component.  
*Type:* string (units)  
*Example:* ``kN``

##### remarks
Further remarks if required  
*Type:* string  


