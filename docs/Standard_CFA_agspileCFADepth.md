


# AGS Piling Standard / X. CFA

## X.X. agspileCFADepth

### X.X.1. Object description

Depth based description of the pile as finally constructed. Data provided in this object should faciliate generation of an accurate drawing or model of the pile. For simple CFA pile only one record may be required. Reinforcement is not included here. Use [agspileRftMain](./Standard_Common_agspileRftMain.md) and [agspileRftShear](./Standard_Common_agspileRftShear.md) instead.

The parent object of [agspileCFADepth](./Standard_CFA_agspileCFADepth.md) is [agspileCFAElement](./Standard_CFA_agspileCFAElement.md)

[agspileCFADepth](./Standard_CFA_agspileCFADepth.md) has the following attributes:


- [topDepth](#topdepth)
- [bottomDepth](#bottomdepth)
- [elevationReference](#elevationreference)
- [descriptionElevationReference](#descriptionelevationreference)
- [diameterConcrete](#diameterconcrete)
- [additionalDetail](#additionaldetail)
- [remarks](#remarks)


### X.X.2. Attributes

##### topDepth
Depth of top of reported section  
*Type:* number  
*Condition:* Required  
*Example:* ``0``

##### bottomDepth
Depth of base of reported section  
*Type:* number  
*Condition:* Required  
*Example:* ``25.2``

##### elevationReference
Elevation of reference datum from which depth measured for this record  
*Type:* number  
*Example:* ``16.23``

##### descriptionElevationReference
Description of reference datum from which depth measured for this record  
*Type:* string  
*Example:* ``Ground level``

##### diameterConcrete
Diameter of bore / concrete as constructed, including allowance for casing if applicable. Units defined in parent [agspileConstructionSchedule](./Standard_Construction_agspileConstructionSchedule.md) object.  
*Type:* number  
*Example:* ``900``

##### additionalDetail
Additional construction details  
*Type:* string  


##### remarks
Additional remarks, if required.  
*Type:* string  
*Example:* ``Some additional remarks``

