


# AGS Piling Standard / X. Material

## X.X. agspileConcreteStrengthTest

### X.X.1. Object description

Sampling information and results for concrete compressive strength tests, which may be on a cube or cylinder.

The parent object of [agspileConcreteStrengthTest](./Standard_Material_agspileConcreteStrengthTest.md) is [agspileConcrete](./Standard_Material_agspileConcrete.md)

[agspileConcreteStrengthTest](./Standard_Material_agspileConcreteStrengthTest.md) has the following attributes:


- [requestReference](#requestreference)
- [siteReference](#sitereference)
- [labReference](#labreference)
- [sampleLocation](#samplelocation)
- [testType](#testtype)
- [castDateTime](#castdatetime)
- [receivedDate](#receiveddate)
- [testDate](#testdate)
- [testAge](#testage)
- [storage](#storage)
- [dimensions](#dimensions)
- [weight](#weight)
- [density](#density)
- [failureLoad](#failureload)
- [compressiveStrength](#compressivestrength)
- [specifiedStrength](#specifiedstrength)
- [testOrganisation](#testorganisation)
- [testAccreditation](#testaccreditation)
- [remarks](#remarks)


### X.X.2. Attributes

##### requestReference
Request sheet reference or equivalent  
*Type:* string  
*Example:* ``S4065071``

##### siteReference
Reference of sample as allocated on site  
*Type:* string  
*Example:* ``M2``

##### labReference
Laboratory reference for sample/test  
*Type:* string  
*Example:* ``24517517``

##### sampleLocation
As stated on test certificate. Typically the ID of the relevant pile(s).  
*Type:* string  
*Example:* ``B1-P001``

##### testType
Type of test, i.e Cube or Cylinder  
*Type:* string (recommend using term from [vocabulary](./Codes_Vocab.md))  
*Example:* ``Cube``

##### castDateTime
Date and time that sample was made  
*Type:* string (None)  
*Example:* ``2017-06-02T13:41``

##### receivedDate
Date received by laboratory  
*Type:* string (ISO date)  
*Example:* ``2017-06-02``

##### testDate
Date of test  
*Type:* string (ISO date)  
*Example:* ``2016-11-30T11:40``

##### testAge
Age in days of specimen at time of test  
*Type:* string  
*Example:* ``28``

##### storage
Details of sample storage between time of sampling and testing  
*Type:* string  
*Example:* ``Cube tank``

##### dimensions
Dimensions of test specimen  
*Type:* string  
*Example:* ``100x100x100mm``

##### weight
Weight of test specimen. Units: kg.  
*Type:* number  
*Example:* ``2.369``

##### density
Density of specimen. Units: kg/m3.  
*Type:* number  
*Example:* ``2370``

##### failureLoad
Failure load. Units: kN.  
*Type:* number  
*Example:* ``512``

##### compressiveStrength
Compressive strength measured. Units: N/mm2  
*Type:* number  
*Example:* ``51.2``

##### specifiedStrength
Specificed nomimal compressive strength  
*Type:* number  
*Example:* ``40``

##### testOrganisation
Name of testing laboratory/organisation  
*Type:* string  
*Example:* ``ACME Laboratories plc``

##### testAccreditation
Accrediting body and reference number (when appropriate)  
*Type:* string  
*Example:* ``UKAS 0000``

##### remarks
Additional remarks, if required.  
*Type:* string  
*Example:* ``Some additional remarks``

