


# AGS Piling Standard / 3. Project 

## 3.8. agsProjectCodeSet

### 3.8.1. Object description

Sets of codes referenced by other parts of the schema such as the [Data group](./Standard_Data_Intro.md) objects (property and parameter codes) and Observation group objects (hole types, legend codes and geology codes). The codes may be project specific or from a standard list, e.g. AGSi standard code list or ABBR codes inherited from AGS factual data. An [agsProjectCodeSet](./Standard_Project_agsProjectCodeSet.md) object is required for each object attribute using a set of codes. The codes for each code set are defined using embedded [agsProjectCode](./Standard_Project_agsProjectCode.md) objects or found at the external source specified in this object. Refer to [3.2.4. Codes for Data objects](./Standard_Project_Rules.md#324-codes-for-data-objects) and [3.2.5. Codes where use of AGS ABBR recommended](./Standard_Project_Rules.md#325-codes-where-use-of-ags-abbr-recommended) for further details.

The parent object of [agsProjectCodeSet](./Standard_Project_agsProjectCodeSet.md) is [agsProject](./Standard_Project_agsProject.md)

[agsProjectCodeSet](./Standard_Project_agsProjectCodeSet.md) contains the following embedded child objects: 

- [agsProjectCode](./Standard_Project_agsProjectCode.md)

[agsProjectCodeSet](./Standard_Project_agsProjectCodeSet.md) has the following attributes:


- [codeSetID](#codesetid)
- [description](#description)
- [usedByObject](#usedbyobject)
- [usedByAttribute](#usedbyattribute)
- [sourceDescription](#sourcedescription)
- [sourceURI](#sourceuri)
- [concatenationAllow](#concatenationallow)
- [concatenationCharacter](#concatenationcharacter)
- [agsProjectCode](#agsprojectcode)
- [remarks](#remarks)


### 3.8.2. Attributes

##### codeSetID
[Identifier](./Standard_General_Rules.md#1510-identifiers) for this code set. May be local to this file or a [UUID](./Standard_General_Rules.md#1511-uuids) as required/specified.  This is optional and is not referenced anywhere else in the schema, but it may be beneficial to include this to help with data control and integrity.  If used, [identifiers](./Standard_General_Rules.md#1510-identifiers) for codeSetID should be unique within the [AGSi file](./Standard_General_Definitions.md#agsi-file).   
*Type:* string ([identifier](./Standard_General_Rules.md#1510-identifiers))  
*Example:* ``CodeSetParameter``

##### description
Name or short description of the code set.  
*Type:* string  
*Condition:* Recommended  
*Example:* ``Parameter codes``

##### usedByObject
Name of the AGSi object that references this code set.  
*Type:* string (name of relevant object in schema)  
*Condition:* Required  
*Example:* ``agsiDataParameterValue``

##### usedByAttribute
Name of the attribute of the AGSi object that references this code set.  
*Type:* string (name of relevant attribute in schema)  
*Condition:* Required  
*Example:* ``codeID``

##### sourceDescription
Description of the source of the list of codes to be used for this set, if applicable. This could be a published source, a project reference or a file provided within the [AGSi package](./Standard_General_Definitions.md#agsi-package). For properties or parameters, use of the  [AGSi code list](./Codes_Codelist.md) is recommended, but it can be changed to an alternate list, e.g. lists published by other agencies (UK or overseas) or major projects/clientsOptional if the codes are provided as [agsProjectCode](./Standard_Project_agsProjectCode.md) objects.  
*Type:* string  
*Condition:* Recommended  
*Example:* ``AGSi standard code list``

##### sourceURI
[URI-reference](./Standard_General_Definitions.md#uri-reference) link to source of list of codes to be used for this set, if applicable. This could be a published source, link to a project reference, or a file provided within the [AGSi package](./Standard_General_Definitions.md#agsi-package). For properties or parameters, use of the [AGSi code list](./Codes_Codelist.md) is recommended, but it can be changed to an alternate list, e.g. lists published by other agencies (UK or overseas) or major projects/clients. Optional if the codes are provided as [agsProjectCode](./Standard_Project_agsProjectCode.md) objects. Spaces are not permitted in [URI-reference](./Standard_General_Definitions.md#uri-reference) strings. Refer to [ 1.6.6. URI](./Standard_General_Formats.md#166-uri) for how to handle spaces in file paths or names.  
*Type:* string ([uri-reference](./Standard_General_Formats.md#167-uri-reference))  
*Condition:* Recommended  
*Example:* ``https://ags-data-format-wg.gitlab.io/agsi_standard/1.0.0/Codes_Codelist/``

##### concatenationAllow
`true` if concatenation of any combination of codes in the list is permitted, e.g. composite exploratory hole types when using AGS ABBR codes. Assume false (not permitted) if attribute omitted.  
*Type:* boolean  
*Example:* ``false``

##### concatenationCharacter
Linking character(s) used for concatenation, if permitted. Input blank string if none.   
*Type:* string  
*Example:* ``+``

##### agsProjectCode
Array of embedded [agsProjectCode](./Standard_Project_agsProjectCode.md) object(s).  
*Type:* array (of [agsProjectCode](./Standard_Project_agsProjectCode.md) object(s))  


##### remarks
Additional remarks, if required.  
*Type:* string  
*Example:* ``Same as AGS ABBR code used in GI Package A``

