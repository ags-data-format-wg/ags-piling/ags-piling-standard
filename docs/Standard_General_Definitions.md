# AGS Piling Standard / 1. General

*This is a placeholder page, copied from AGSi. Requires review and edit to make it suitable for AGS Piling.*

## 1.2. Definitions

This section presents definitions of selected important terms used in this standard.

Some of the terms are defined elsewhere in the standard but are repeated here for ease of reference.

Some of the definitions are specific to AGSi, or are commonly understood terms. Where definitions have been adopted based on other external sources, these are identified.

!!! Note
    Some of the terms have been adopted from the terminology task work carried out by the *Integrated Digital Built Environment (IDBE) Geotechnical Engineering standardization* group, a collaboration between buildingSMART and the Open Geospatial Consortium (OGC).

    The IDBE work has not been published but information from the work undertaken can be viewed at the <a href="https://github.com/opengeospatial/IDBE-Geotech" target="_blank">IDBE Github repository</a>.

The terms are presented in alphabetical order.

##### AGSi file

A data file produced in accordance with the [schema](#agsi-schema) and [encoding](#encoding) requirements defined in this Standard.

##### AGSi package

A set of files comprising the [AGSi file](#agsi-file) together with the
[supporting files](#agsi-supporting-files) and
[documents](#agsi-document) that are
[included](#agsi-included-files) within the package.
Files in an AGSi package are typically contained within a single zip file, as described in [1.8. Transfer of AGSi](./Standard_General_Transfer.md).

##### Analytical model
Model suitable for direct use in analysis or design for a specified purpose. Normally based on development and interpretation of an [observational model](#observational-model) taking into account uncertainty, requirements of codes and analysis/design methodology.
<br>*Source: IDBE, <a href="https://www.iaeg.info/commission-25-use-of-engineering-geological-models/" target="_blank">IAEG Commission 25</a>*

##### Category (of model)
A classification system for the type of model based recommendations by
<a href="https://www.iaeg.info/commission-25-use-of-engineering-geological-models/" target="_blank">IAEG Commission 25</a>. The category may be: [conceptual](#conceptual-model),
[observational](#observational-model) or
[analytical](#analytical-model), as defined herein.
<br>*Source: IDBE*

##### Conceptual model
Abstract description of a system, such as geology or hydrogeology,
based on understanding of local and regional processes and relationships.
<br>*Source: IDBE, <a href="https://www.iaeg.info/commission-25-use-of-engineering-geological-models/" target="_blank">IAEG Commission 25</a>*

##### (AGSi) document

Files that may include documents, data files, drawings, (BIM) models etc., identified and referenced as document objects ([agsProjectDocument](./Standard_Project_agsProjectDocument.md)) in the [AGSi file](#agsi-file).
Document files may be included within the [AGSi package](#agsi-package) or may be
[external files](#agsi-external-files).

##### Domain (of model)

A specified sphere of activity or knowledge. In the context of AGSi, the domains include ground (parent domain), geology, geotechnical, hydrogeology and geoenvironmental.
<br>*Source: IDBE*

##### Encoding

The process by which the data, arranged in accordance with the [schema](#agsi-schema), is converted to a specified format for transmittal. Encoding is also the term use for the deliverable produced.

##### Engineering geology observational model

Model showing  the anticipated location and extent of geological units and other geological features based on observations, eg. interpretation of borehole data.
<br>*Source: IDBE*

##### (AGSi) external files

A file that is not included as part of the [AGSi package](#agsi-package)
but is accessible to users, e.g. via a website or within a project document management system.

##### Factual data

The record of observations and measurements made during a ground investigation (GI) or other survey. For a GI it includes the results of field observations, in situ testing, laboratory testing and monitoring, as typically reported in a factual ground investigation report.

##### Interpreted data

Information, including but not limited to properties and/or parameters, collated and/or derived from factual data.
May incorporate some or all of presentation and evaluation of the factual information, technical insight, and parameters for analysis and design.

##### Geological model

Abbreviated term for an [engineering geology observational
model](#engineering-geology-observational-model).
<br>*Source: IDBE*

##### Geotechnical design model

A geotechnical [analytical model](#analytical-model) intended for use in design. This model will define, usually for a specified purpose, the location and extent of geological/geotechnical units and their [parameters](#parameter).
<br>*Source: IDBE*

##### Geotechnical model

Abbreviated form of [geotechnical design model](#geotechnical-design-model).
<br>*Source: IDBE*

##### Global coordinate system

A secondary coordinate system, normally an established regional or national system, defined via a transformation from the [model coordinate system](#model-coordinate-system).

##### Ground model

See [model](#model). For AGSi, ground model is used as a generic parent term for any [type of model](#type-of-model) in the ground [domain](#domain-of-model).

##### Ground investigation

A campaign of fieldwork and/or laboratory testing carried out and reported under a site/ground investigation contract.

##### Hydrogeological model
Abbreviated description for an hydrogeological observational
(or analytical) model.
This model will show the anticipated location and extent of hydrogeological
units, e.g. aquifers.
<br>*Source: IDBE*

##### (AGSi) included files

[Supporting files](#agsi-supporting-files) and [documents](#agsi-document) that are
part of the [AGSi package](#agsi-package).

##### Investigation

See [ground investigation](#ground-investigation).

##### Model

A digital geometric (1D, 2D or 3D) representation of the ground.

##### Model boundary

Definition of the valid extent of a [model](#model). Model geometry lying outside of a model boundary should be ignored.

##### Model coordinate system

The coordinate system used by a [model](#model), although this could be an established regional or national system.

##### Model element

Component part of a [model](#model) in AGSi,
i.e. a [model](#model) is a collection of model elements.

##### Observation

For the purposes of AGSi, observations comprise spatially referenced data or information provided as part of the [model](#model),
or in support of it. Observations include, but are not necessarily limited to, data and information used to derive the model. AGSi observations are most likely to comprise [factual data](#factual-data), but there may be situations where it is appropriate to include observations that are based on [interpretation](#interpreted-data). For further clarification, refer to documentation for the [Observation group](./Standard_Observation_Intro.md).


##### Observational model

Model predominantly based on observations and measurements of the ground. This may incorporate interpretation of anticipated conditions in areas between the observation and measurement points. Generally not suitable for
direct use in analysis or design without further interpretation. Observational models should be developed from a conceptual model.
<br>*Source: IDBE, <a href="https://www.iaeg.info/commission-25-use-of-engineering-geological-models/" target="_blank">IAEG Commission 25</a>*

##### Parameter

An interpreted single value, or a profile of values
related to an independent variable, that is to be used in design and/or analysis. Parameters are most likely to be (Eurocode 7) characteristic values.
<br>*Source: IDBE*

##### Project

The Project is the specific project/commission under which the [AGSi package](#agsi-package) has been delivered.

##### Parent project

The project that commissioned the [Project](#project) or, more generally, any project that commissions another project.

##### Producer

A person or organisation responsible for creating and delivering the [AGSi package](#agsi-package). A producer is a [user](#user).

##### Property

A value reported from an observation or test (directly measured, or based on interpreted measurement), as typically reported in a factual GI report. Properties are likely to be (Eurocode 7) measured or derived values.
<br>*Source: IDBE*

##### Root schema / root object

The **root schema** is the topmost (ultimate parent) level of the AGSi schema. It comprises a single object, known as the **root object**, which is the container for the remainder of the schema.

##### (AGSi) schema

The organisation/structure of AGSi data. Includes data hierarchy, object and attribute names and definitions, required data types and rules for use.

##### Specifier

The person or organisation responsible for specifying the [AGSi package](#agsi-package) to be delivered. A specifier is a [user](#user).

##### (AGSi) supporting files

Files that provide data or information in support of the [AGSi file](#agsi-file).
This includes geometry referenced by [agsiGeometryFromFile](./Standard_Geometry_agsiGeometryFromFile.md) and data files referenced by
[agsiDataPropertyFromFile](./Standard_Data_agsiDataPropertyFromFile.md).
[Documents](#agsi-document) are not considered to be supporting files.
It is recommended that supporting files should be
[included](#agsi-included-files) in the [AGSi package](#agsi-package)
but they can be [external](#agsi-external-files).


##### Type (of model)

Concise definition incorporating the
[category](#category-of-model) and [domain](#domain-of-model) of the model,
and any further definition that may be required.
Use of the formally defined category and domain names is preferred,
but some acceptable alternative terms are included herein.
<br>*Source: IDBE*

##### Ultimate project

The top level parent project of the [Project](#project), typically the works that are eventually to be constructed that will benefit from the [Project](#project) or a framework contract set up by an asset owner.

##### URI

A Uniform Resource Identifier (URI) is a string of characters that unambiguously identify a particular resource. Used in computing to identify the location of files, websites etc. A URI is formatted in accordance with
<a href="https://tools.ietf.org/html/rfc3986" target="_blank">RFC 3986</a>. A URL (Uniform Resource Locator) is a subset of URI.

##### URI-reference

A URI-reference is either a [URI](#uri) or a relative reference, in accordance with
<a href="https://tools.ietf.org/html/rfc3986" target="_blank">RFC 3986</a>. A relative reference can be used to specify the location of a file relative to the file containing the reference.

##### User

A person or organisation involved in specifying, creating or making use of an [AGSi package](#agsi-package).

##### UUID

A universally unique identifier (UUID) is a 128-bit number used to identify information in computer systems. The term globally unique identifier (GUID) is also often used.
