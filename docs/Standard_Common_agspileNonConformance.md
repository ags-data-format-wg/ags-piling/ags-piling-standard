


# AGS Piling Standard / X. Common

## X.X. agspileNonConformance

### X.X.1. Object description

Non conformance report (NCR) and/or record of remedial actions

[agspileNonConformance](./Standard_Common_agspileNonConformance.md) may be embedded in any of the following potential parent objects: 

- [agspileBoredElement](./Standard_Bored_agspileBoredElement.md)
-  [agspileCFAElement](./Standard_CFA_agspileCFAElement.md)

[agspileNonConformance](./Standard_Common_agspileNonConformance.md) has the following attributes:


- [ncrReference](#ncrreference)
- [ncrDate](#ncrdate)
- [defectDescription](#defectdescription)
- [actionDescription](#actiondescription)
- [actionDate](#actiondate)
- [actionOrganisation](#actionorganisation)
- [remarks](#remarks)


### X.X.2. Attributes

##### ncrReference
Non conformance / action reference  
*Type:* string (identifier)  
*Example:* ``NCR/001``

##### ncrDate
Date NCR raised / defect discovered  
*Type:* string (ISO date)  
*Example:* ``2017-02-15``

##### defectDescription
Description of defect   
*Type:* string  
*Example:* ``Leakage through wall 3.5-5.0m depth``

##### actionDescription
Description of remedial action  
*Type:* string  
*Example:* ``Grouted``

##### actionDate
Date of action  
*Type:* string (ISO date)  
*Example:* ``2017-04-01``

##### actionOrganisation
Organisation carrying out the action  
*Type:* None  
*Example:* ``ACME Contractor``

##### remarks
Additional remarks, if required.  
*Type:* string  
*Example:* ``Some additional remarks``

