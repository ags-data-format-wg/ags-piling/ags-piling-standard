


# AGS Piling Standard / X. Bored

## X.X. agspileBoredProgress

### X.X.1. Object description

Time based activity and progress record for bored piles. Typically one record per activity, but multiple records per activity may be used if required, e.g. if activity spans over different shifts. See agspileBoredRecord for detailed log of concreting activity. Depth fields only need to be completed where a relevant depth has changed during the activity. Unchanged depths may be left blank.

The parent object of [agspileBoredProgress](./Standard_Bored_agspileBoredProgress.md) is [agspileBoredElement](./Standard_Bored_agspileBoredElement.md)

[agspileBoredProgress](./Standard_Bored_agspileBoredProgress.md) has the following attributes:


- [activityStart](#activitystart)
- [activityEnd](#activityend)
- [activityType](#activitytype)
- [description](#description)
- [preboreDepth](#preboredepth)
- [excavationDepth](#excavationdepth)
- [concreteTopDepth](#concretetopdepth)
- [casingDepth ](#casingdepth )
- [fluidTopDepth](#fluidtopdepth)
- [rftTopDepth](#rfttopdepth)
- [elevationReference](#elevationreference)
- [descriptionElevationReference](#descriptionelevationreference)
- [fluidType](#fluidtype)
- [rig](#rig)
- [tool](#tool)
- [operator](#operator)
- [banksman](#banksman)
- [groundwaterObservation](#groundwaterobservation)
- [unexpectedEvent](#unexpectedevent)
- [remarks](#remarks)


### X.X.2. Attributes

##### activityStart
Date/time of start of activity  
*Type:* string (Nonetime)  
*Condition:* Required  
*Example:* ``2016-11-23T14:35``

##### activityEnd
Date/time of end of activity  
*Type:* string (Nonetime)  
*Example:* ``2016-11-23T16:05``

##### activityType
Activity type  
*Type:* string (recommend using term from [vocabulary](./Codes_Vocab.md))  
*Example:* ``Concrete``

##### description
Activity description/method  
*Type:* string  
*Example:* ``Concreting with tremie``

##### preboreDepth
Depth to bottom of prebore at end of activity  
*Type:* number  
*Example:* ``8.5``

##### excavationDepth
Depth to base of pile excavation at end of activity  
*Type:* number  
*Example:* ``5.5``

##### concreteTopDepth
Depth to top of concrete at end of activity  
*Type:* number  
*Example:* ``10.5``

##### casingDepth 
Depth to base of casing at end of activity  
*Type:* number  


##### fluidTopDepth
Depth to top of support fluid at end of activity  
*Type:* number  
*Example:* ``2.2``

##### rftTopDepth
Depth to top of reinforcement (main bars) at end of activity  
*Type:* number  


##### elevationReference
Elevation of reference datum from which depth measured for this record  
*Type:* number  
*Example:* ``16.23``

##### descriptionElevationReference
Description of reference datum from which depth measured for this record  
*Type:* string  
*Example:* ``Top of casing``

##### fluidType
Support fluid used during activity  
*Type:* string  
*Example:* ``Bentonite``

##### rig
Rig type and reference (if applicable)  
*Type:* string  
*Example:* ``BG40 (rig 2)``

##### tool
Boring/excavation tool  
*Type:* string  
*Example:* ``Auger``

##### operator
Rig operator / Operator responsible for activity  
*Type:* string  
*Example:* ``J. Smith``

##### banksman
Rig banksman  
*Type:* string  
*Example:* ``J. Jones``

##### groundwaterObservation
Groundwater observations. More detailed observations may be included using [agspileObservation](./Standard_Common_agspileObservation.md).  
*Type:* string  
*Example:* ``Seepage at 8.5m``

##### unexpectedEvent
Delay/obstruction/unexpected event remarks  
*Type:* string  
*Example:* ``Minor collapse at 8.7m``

##### remarks
Additional remarks, if required.  
*Type:* string  
*Example:* ``Some additional remarks``

