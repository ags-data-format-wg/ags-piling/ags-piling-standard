


# AGS Piling Standard / X. Common

## X.X. agspileGeology

### X.X.1. Object description

Description of excavated ground and/or interpretation of ground conditions encountered during pile construction. If interpretation is not based on pile arisings this should be flagged up in remarks.

[agspileGeology](./Standard_Common_agspileGeology.md) may be embedded in any of the following potential parent objects: 

- [agspileBoredElement](./Standard_Bored_agspileBoredElement.md)
-  [agspileCFAElement](./Standard_CFA_agspileCFAElement.md)

[agspileGeology](./Standard_Common_agspileGeology.md) has the following attributes:


- [topDepth](#topdepth)
- [bottomDepth](#bottomdepth)
- [elevationReference](#elevationreference)
- [descriptionElevationReference](#descriptionelevationreference)
- [geologyDescription](#geologydescription)
- [legendCode](#legendcode)
- [geologyCode](#geologycode)
- [geologyCode2](#geologycode2)
- [remarks](#remarks)


### X.X.2. Attributes

##### topDepth
Depth of top of reported section  
*Type:* number  
*Condition:* Required  
*Example:* ``0.2``

##### bottomDepth
Depth of base of reported section  
*Type:* number  
*Example:* ``5.5``

##### elevationReference
Elevation of reference datum from which depth measured for this record  
*Type:* number  
*Example:* ``16.23``

##### descriptionElevationReference
Description of reference datum from which depth measured for this record  
*Type:* string  
*Example:* ``Top of casing``

##### geologyDescription
General description of ground encountered.  
*Type:* string  
*Example:* ``Stiff grey clay``

##### legendCode
Legend code.  Optional. Typically only used if formal logging of bore/arisings being undertaken.   
*Type:* string (None)  
*Example:* ``102``

##### geologyCode
Geology code.  Optional. Typically only used if formal logging of bore/arisings being undertaken.   
*Type:* string (project None)  


##### geologyCode2
Second geology code.  Optional. Typically only used if formal logging of bore/arisings being undertaken.   
*Type:* string (project None)  


##### remarks
Additional remarks, if required.  
*Type:* string  
*Example:* ``Some additional remarks``

