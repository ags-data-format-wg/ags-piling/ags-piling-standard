


# AGS Piling Standard / X. Bored

## X.X. agspileBoredDepth

### X.X.1. Object description

Depth based description of the pile as finally constructed. Allows for recording of as built diameters in temporary cased sections. Data provided in this object should faciliate generation of an accurate drawing or model of the pile. Reinforcement is not included here. Use [agspileRftMain](./Standard_Common_agspileRftMain.md) and [agspileRftShear](./Standard_Common_agspileRftShear.md) instead.

The parent object of [agspileBoredDepth](./Standard_Bored_agspileBoredDepth.md) is [agspileBoredElement](./Standard_Bored_agspileBoredElement.md)

[agspileBoredDepth](./Standard_Bored_agspileBoredDepth.md) has the following attributes:


- [topDepth](#topdepth)
- [bottomDepth](#bottomdepth)
- [elevationReference](#elevationreference)
- [descriptionElevationReference](#descriptionelevationreference)
- [diameterConcrete](#diameterconcrete)
- [diameterBore](#diameterbore)
- [permanentCasingDetail](#permanentcasingdetail)
- [additionalDetail](#additionaldetail)
- [remarks](#remarks)


### X.X.2. Attributes

##### topDepth
Depth of top of reported section. Top of the topmost section will normally be at cut-off level.  
*Type:* number  
*Condition:* Required  
*Example:* ``0.2``

##### bottomDepth
Depth of base of reported section  
*Type:* number  
*Condition:* Required  
*Example:* ``5.5``

##### elevationReference
Elevation of reference datum from which depth measured for this record  
*Type:* number  
*Example:* ``16.23``

##### descriptionElevationReference
Description of reference datum from which depth measured for this record  
*Type:* string  
*Example:* ``Top of casing``

##### diameterConcrete
Diameter of concrete section. Generally same as diameter of bore unless there is a casing and/or annulus. Units defined in parent [agspileConstructionSchedule](./Standard_Construction_agspileConstructionSchedule.md) object.  
*Type:* number  
*Example:* ``900``

##### diameterBore
Diameter of bore / outside of casing, i.e outermost part of the element. Units defined in parent agspileScheduleAsBuilt object. Only required if different to concrete diameter, i.e. annulus or permanent casing present. Units defined in parent [agspileConstructionSchedule](./Standard_Construction_agspileConstructionSchedule.md) object.  
*Type:* number  
*Example:* ``1020``

##### permanentCasingDetail
Description of permanent casing / sleeve.  
*Type:* string  
*Example:* ``Double sleeve``

##### additionalDetail
Additional/other construction details  
*Type:* string  
*Example:* ``Grouted annulus``

##### remarks
Additional remarks, if required.  
*Type:* string  
*Example:* ``Some additional remarks``

