


# AGS Piling Standard / X. Design

## X.X. agspileDesignLoad

### X.X.1. Object description

Design loads to be applied to pile/wall elements. Can also be used for loads on pile caps as defined in [agspileDesignRequirement](./Standard_Design_agspileDesignRequirement.md). Referenced by [agspileDesignElement](./Standard_Design_agspileDesignElement.md) and/or [agspileDesignRequirement](./Standard_Design_agspileDesignRequirement.md) as required. Details of load components and load combinations are incorporated via embedded child objects. Loads may be specified for each element individually and referenced to the corresponding pile. Alternatively, generic load conditions may be defined that can be referenced by many different piles. Sign convention for loads in parent schedule object.

The parent object of [agspileDesignLoad](./Standard_Design_agspileDesignLoad.md) is [agspileDesignSchedule](./Standard_Design_agspileDesignSchedule.md)

[agspileDesignLoad](./Standard_Design_agspileDesignLoad.md) has associations (reference links) with the following objects: 

- [agspileDesignRequirement](./Standard_Design_agspileDesignRequirement.md)
- [agspileDesignElement](./Standard_Design_agspileDesignElement.md)

[agspileDesignLoad](./Standard_Design_agspileDesignLoad.md) has the following attributes:


- [loadID](#loadid)
- [description](#description)
- [designStandard](#designstandard)
- [agspileDesignLoadAction](#agspiledesignloadaction)
- [agsPileDesignLoadCombination](#agspiledesignloadcombination)
- [remarks](#remarks)


### X.X.2. Attributes

##### loadID
Design load reference. Must be unique within all agsPileDesignLoad objects within a file as this ID is referecend by other objects. Details of components of loading and load combinations required are incorporated within embedded child objects.  
*Type:* string  
*Condition:* Required  
*Example:* ``B1-A``

##### description
Description of loading  
*Type:* string  
*Example:* ``B1 south abutment piles``

##### designStandard
Applicable code(s) or standard(s) that this loading has been derived by and/or for use with  
*Type:* string  
*Example:* ``EC7``

##### agspileDesignLoadAction
Components of the load/actions as an array of embedded [agspileDesignLoadAction](./Standard_Design_agspileDesignLoadAction.md) objects  
*Type:* array (of [agspileDesignLoadAction](./Standard_Design_agspileDesignLoadAction.md) objects)  


##### agsPileDesignLoadCombination
Required load combinations as an array of embedded agsPileDesignLoadCombination objects  
*Type:* array (of agsPileDesignLoadCombination objects)  


##### remarks
Additional remarks, if required.  
*Type:* string  
*Example:* ``Some additional remarks``

