# AGS Piling Standard / 3. Project

*This is a placeholder page, copied from AGSi. Requires review and edit to make it suitable for AGS Piling.*

## 3.7. agsProjectDocument

### 3.7.1. Object description

Metadata for the individual documents or references contained within a document set. Refer to [3.2.3. Documents](./Standard_Project_Rules.md#323-documents) for further details.

The parent object of [agsProjectDocument](./Standard_Project_agsProjectDocument.md) is [agsProjectDocumentSet](./Standard_Project_agsProjectDocumentSet.md)

[agsProjectDocument](./Standard_Project_agsProjectDocument.md) has the following attributes:


- [documentID](#documentid)
- [title](#title)
- [description](#description)
- [author](#author)
- [client](#client)
- [originalReference](#originalreference)
- [systemReference](#systemreference)
- [revision](#revision)
- [date](#date)
- [status](#status)
- [statusCode](#statuscode)
- [documentURI](#documenturi)
- [documentSystemURI](#documentsystemuri)
- [remarks](#remarks)


### 3.7.2. Attributes

##### documentID
[Identifier](./Standard_General_Rules.md#1510-identifiers) for the document. May be local to this file or a [UUID](./Standard_General_Rules.md#1511-uuids) as required/specified. This is optional and is not referenced anywhere else in the schema, but it may be beneficial to include this to help with data control and integrity. If used, [identifiers](./Standard_General_Rules.md#1510-identifiers) for documentID should be unique at least within each document set ([agsProjectDocumentSet](./Standard_Project_agsProjectDocumentSet.md) object), and preferably unique within the [AGSi file](./Standard_General_Definitions.md#agsi-file).   
*Type:* string ([identifier](./Standard_General_Rules.md#1510-identifiers))  
*Example:* ``eac20ae4-25a1-4e68-96f8-cf43b9761b11``

##### title
Original title on the document.  
*Type:* string  
*Condition:* Recommended  
*Example:* ``Factual ground investigation report, Gotham City Metro Purple Line, C999 Package A, Volume 1 of 3``

##### description
Further description if required. Typically what the document is commonly known as, given that the formal title may be verbose. Alternatively use for name/title given in project documentation system if this differs from original title.  
*Type:* string  
*Example:* ``Package A factual report, Volume 1``

##### author
The original author of the document.  
*Type:* string  
*Condition:* Recommended  
*Example:* ``Boring Drilling Ltd``

##### client
The original commissioning client for the document.  
*Type:* string  
*Condition:* Recommended  
*Example:* ``XYZ D&B Contractor``

##### originalReference
Original reference shown on the document, if different from reference used by project document system (see [systemReference](#systemreference)).  
*Type:* string  
*Condition:* Recommended  
*Example:* ``12345/GI/2``

##### systemReference
Document reference used in the project document management system. May differ from original reference shown on document.  
*Type:* string  
*Condition:* Recommended  
*Example:* ``C999-BDL-AX-XX-RP-WG-0002``

##### revision
Revision reference (typically in accordance with ISO19650 or BS1192).  
*Type:* string  
*Condition:* Recommended  
*Example:* ``P2``

##### date
Date of the document (current revision).  
*Type:* string ([ISO date](./Standard_General_Formats.md#165-iso-date-andor-time))  
*Condition:* Recommended  
*Example:* ``2018-09-06``

##### status
Status as indicated on or within the document, typically following recommendations of BS8574 if applicable.  
*Type:* string  
*Condition:* Recommended  
*Example:* ``Final``

##### statusCode
Status code, typically in accordance with ISO19650, or BS1192 suitability code.  
*Type:* string  
*Condition:* Recommended  
*Example:* ``S2``

##### documentURI
[URI-reference](./Standard_General_Definitions.md#uri-reference) (link address) for the document. This will be a relative link if document included within the [AGSi package](./Standard_General_Definitions.md#agsi-package). For a public domain published reference, the link should be provided here. Spaces are not permitted in [URI-reference](./Standard_General_Definitions.md#uri-reference) strings. Refer to [ 1.6.6. URI](./Standard_General_Formats.md#166-uri) for how to handle spaces in file paths or names.  
*Type:* string ([uri-reference](./Standard_General_Formats.md#167-uri-reference))  
*Condition:* Recommended  
*Example:* ``docs/GI/C999-BDL-AX-XX-RP-WG-0002.pdf``

##### documentSystemURI
[URI](./Standard_General_Definitions.md#uri) (link address) for the location of the document within the project document management system. To be used if *[documentURI](#documenturi)* attribute used for relative link. Spaces are not permitted in [URI](./Standard_General_Definitions.md#uri) (link address) strings. Refer to [ 1.6.6. URI](./Standard_General_Formats.md#166-uri) for how to handle spaces in file paths or names.  
*Type:* string ([uri](./Standard_General_Formats.md#166-uri))  
*Condition:* Recommended  
*Example:* ``https://gothammetro.sharepoint.com/C999/docs/C999-BDL-AX-XX-RP-WG-0002``

##### remarks
Additional remarks, if required.  
*Type:* string  
*Example:* ``Some additional remarks``

