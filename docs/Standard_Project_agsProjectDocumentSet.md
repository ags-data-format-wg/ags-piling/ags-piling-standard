# AGS Piling Standard / 3. Project

*This is a placeholder page, copied from AGSi. Requires review and edit to make it suitable for AGS Piling.*

## 3.6. agsProjectDocumentSet

### 3.6.1. Object description

Container and metadata for a set of supporting documents or reference information, which may be referenced from other parts of the schema. This container must be used and referenced, even if there is only one document within it. Refer to [3.2.3. Documents](./Standard_Project_Rules.md#323-documents) for further details.

The parent object of [agsProjectDocumentSet](./Standard_Project_agsProjectDocumentSet.md) is [agsProject](./Standard_Project_agsProject.md)

[agsProjectDocumentSet](./Standard_Project_agsProjectDocumentSet.md) contains the following embedded child objects: 

- [agsProjectDocument](./Standard_Project_agsProjectDocument.md)

[agsProjectDocumentSet](./Standard_Project_agsProjectDocumentSet.md) has associations (reference links) with the following objects: 

- [agsProjectInvestigation](./Standard_Project_agsProjectInvestigation.md)
- [agsiModel](./Standard_Model_agsiModel.md)
- [agsiObservationSet](./Standard_Observation_agsiObservationSet.md)

[agsProjectDocumentSet](./Standard_Project_agsProjectDocumentSet.md) has the following attributes:


- [documentSetID](#documentsetid)
- [description](#description)
- [agsProjectDocument](#agsprojectdocument)
- [remarks](#remarks)


### 3.6.2. Attributes

##### documentSetID
[Identifier](./Standard_General_Rules.md#1510-identifiers) for this document set. May be local to this file or a [UUID](./Standard_General_Rules.md#1511-uuids) as required/specified.  [Identifiers](./Standard_General_Rules.md#1510-identifiers) for *[documentSetID](./Standard_Project_agsProjectDocumentSet.md#documentsetid)* shall be unique within an [AGSi file](./Standard_General_Definitions.md#agsi-file). Referenced by other parts of the schema.  
*Type:* string ([identifier](./Standard_General_Rules.md#1510-identifiers))  
*Condition:* Required  
*Example:* ``GIPackageAReport``

##### description
Brief description, i.e. what this set of documents is commonly known as.  
*Type:* string  
*Example:* ``Package A factual report``

##### agsProjectDocument
Array of embedded [agsProjectDocument](./Standard_Project_agsProjectDocument.md) object(s).  
*Type:* array (of [agsProjectDocument](./Standard_Project_agsProjectDocument.md) object(s))  


##### remarks
Additional remarks, if required  
*Type:* string  
*Example:* ``Some additional remarks``

