# AGS Piling Standard / 1. General

**AGS Piling is a draft under development. The API/schema is not yet stable. It should not be used or be specified for use in a production environment**

**If you have any queries, or wish to get involved in the development of AGS Piling, then please contact the AGS Data Management Working Group (see AGS website)**

*This is a placeholder page, copied from AGSi. Requires review and edit to make it suitable for AGS Piling.*

The title of this standard is:

**Association of Geotechnical & Geoenvironmental Specialists transfer format for ground model and interpreted data (AGSi): Standard**

## 1.1. Introduction

AGSi is the short name of a schema and transfer format for the exchange of ground model and interpreted data in the ground domain.

This standard defines the schema, rules, method of encoding and other requirements that constitute AGSi.

The online version of this standard, found by following the link provided on the AGS website:
<a href="https://www.ags.org.uk/data-format/agsi-ground-model" target="_blank">https://www.ags.org.uk/data-format/agsi-ground-model</a>,
is the official controlled version.
It is not available in printed form or as a PDF.
Any printed or PDF copy shall be considered as uncontrolled.

### 1.1.1. Contents of this standard

This standard is organised as follows:

* This **General** section which sets out general requirements and rules. This includes some common requirements that apply for all or many parts of the schema.
* Sections describing the **schema**. Each section covers a **group** of objects and typically comprises an **overview**, **rules and conventions**  and pages providing full details for each object in the group (**object reference**).
* The **Encoding** section which describes rules and requirements for encoding applicable to file based transfer.  
* The **Codes and vocabularies** section which provides lists of codes and vocabularies recommended for use in AGSi.

This standard is supported by the following supplementary documentation:

* [JSON Schema](./Standard_Encoding_JSONSchema.md) file(s) for AGSi
* <a href="https://ags-data-format-wg.gitlab.io/AGSi_Documentation/Guidance_Intro/">Guidance</a> on how to use AGSi, including guidance on best practice


!!! Note
    This standard concisely sets out requirements for AGSi and provides definitive reference information for the schema. Newcomers to AGSi will likely need to read this in conjunction with the <a href="https://ags-data-format-wg.gitlab.io/AGSi_Documentation/Guidance_Intro/">Guidance</a> which provides information on how to use AGSi, including examples.
    In particular, newcomers to AGSi may wish to start with our <a href="https://ags-data-format-wg.gitlab.io/AGSi_Documentation/Guidance_Brief_Scope/">Introduction to AGSi</a> guidance.

### 1.1.2. Navigation of the documentation

Use the navigation menus on all pages to move around the documentation. For this standard pages of content are generally only found on the third (lowest) level of the menu with the second level used for section headings only. The top level of the menu also includes links to the AGSi Home page and Guidance (separate sites).

The version number of the standard being viewed is given at the top of the page. Hovering over the version number activates a selector which may be used to view to a different version. A warning should appear at the top of the page if the version being viewed is not current.

Hyperlinks are used to identify related content on other pages, generally within this standard but there are a few links to the AGSi Guidance. Use the browser back button to return to the previous page. Links to external sites should open in a new browser window.

### 1.1.3. Interpretation of this standard

This standard adopts the common convention used in international standards whereby **shall** and **must** are used to indicate requirements to be strictly followed to conform to this standard, i.e.  mandatory requirements.

**Should** is used to indicate a recommendation, i.e. not mandatory.

The supplementary documentation is not part of the standard.
Links to such documentation provided in the standard are informative only.

!!! Note
    Throughout the documentation, *Notes* formatted as seen here, are used to provide additional background information or non-mandatory guidance.

In the event of conflict between the schema diagrams and the object reference pages, the object reference pages shall take precedence.

### 1.1.4. Governance

This standard has been authored by and is maintained by the *Data Management Working Group* of the
<a href="https://ags.org.uk" target="_blank">Association of Geotechnical and Geoenvironmental Specialists (AGS)</a>

!!! Note
    The AGS is a non-profit making trade association established to improve the profile and quality of geotechnical and geoenvironmental engineering. The membership comprises UK organisations and individuals having a common interest in the business of site investigation, geotechnics, geoenvironmental engineering, engineering geology, geochemistry, hydrogeology, and other related disciplines.

Version control is in effect for this standard. The standard defines the schema. Therefore the version number of the schema is always consistent with the version number of the standard.

The system used for version numbering broadly follows the principles of
<a href="https://semver.org/" target="_blank">Semantic Versioning</a>
which adopts the *Major.Minor.Patch* convention. For AGSi this has been interpreted thus:

* *Major* versions are reserved for major changes in scope or changes that break backwards compatibility
* *Minor* versions are used when functionality is added in a backwards compatible manner
* *Patch* versions are used for minor updates or correction of errors in documentation or the schema that do not change intent (includes new codes or vocabulary items)

The above applies to the standard only. Versioning of the supporting [AGSi JSON Schema](./Standard_Encoding_JSONSchema.md) files(s) is the same as that of the standard except that the version of the applicable <a href="http://json-schema.org/specification.html" target="_blank">JSON Schema Specification</a> is appended to the schema version number.
Versioning of the <a href="https://ags-data-format-wg.gitlab.io/AGSi_Documentation/Guidance_Intro/">Guidance</a> is separate from that of the standard.

!!! Note
    Further details of the system adopted for version numbering are given in
    <a href="https://ags-data-format-wg.gitlab.io/AGSi_Documentation/Guidance_General_Revision">Guidance > General > Governance - Version control</a>.
