


# AGS Piling Standard / X. CFA

## X.X. agspileCFAPourRecord

### X.X.1. Object description

Time based log of concrete pours for CFA. Timings relate to concrete delivered to agitator if not delivered direct to pile. Piles that the concrete is likely to have been subsequently delivered are idenitfied and linked by reference (this object is not a child of [agspileCFAElement](./Standard_CFA_agspileCFAElement.md)). Further details relating to the concrete, e.g. test results, are provided separately in [agspileConcrete](./Standard_Material_agspileConcrete.md) objects (linked by reference). Concrete strength and class details included in referenced [agspileCFAElement](./Standard_CFA_agspileCFAElement.md) object.

The parent object of [agspileCFAPourRecord](./Standard_CFA_agspileCFAPourRecord.md) is [agspileConstructionSchedule](./Standard_Construction_agspileConstructionSchedule.md)

[agspileCFAPourRecord](./Standard_CFA_agspileCFAPourRecord.md) has associations (reference links) with the following objects: 

- [agspileCFAElement](./Standard_CFA_agspileCFAElement.md)
- [agspileConcrete](./Standard_Material_agspileConcrete.md)

[agspileCFAPourRecord](./Standard_CFA_agspileCFAPourRecord.md) has the following attributes:


- [concreteID](#concreteid)
- [isAgitator](#isagitator)
- [elementIDs](#elementids)
- [batchDateTime](#batchdatetime)
- [arrivalDateTime](#arrivaldatetime)
- [consistenceTestType](#consistencetesttype)
- [consistenceResult](#consistenceresult)
- [consistenceRemarks](#consistenceremarks)
- [numberCubes](#numbercubes)
- [pourStart](#pourstart)
- [pourEnd](#pourend)
- [ambientTemperature](#ambienttemperature)
- [remarks](#remarks)


### X.X.2. Attributes

##### concreteID
Identifier for a load/batch of concrete used in pile or deliverd to agitator supplying concrete to the piles. Typically the concrete ticket reference. Must be unique within the file. Used to cross-reference concreteID in [agspileConcrete](./Standard_Material_agspileConcrete.md)  
*Type:* string (identifier)  
*Condition:* Required  
*Example:* ``21364285``

##### isAgitator
Flag for whether concrte delivered direct to pile (false) or first delivered to agitator or similar before final delivery to pile (true).  
*Type:* boolean  
*Example:* ``true``

##### elementIDs
Piles that concrete from this delivery is used in, or likely to be used in if delivered to agitator. List relevant elementID. Array must be used even if there is only one pile in the list.  
*Type:* array (reference to elementID of [agspileCFAElement](./Standard_CFA_agspileCFAElement.md) object)  


##### batchDateTime
Date and time of batching of concrete. May be at remote batching plant or on site.  
*Type:* string (Nonetime)  
*Example:* ``2023-03-02T10:32``

##### arrivalDateTime
Date and time of arrival of concrete to work site.  
*Type:* string (Nonetime)  
*Example:* ``2023-03-02T11:20``

##### consistenceTestType
Consistence test type  
*Type:* string (enum from list below)  
\- None  
*Example:* ``Slump``

##### consistenceResult
Consistence test result. Units mm for slump and flow table tests.  
*Type:* number  
*Example:* ``190``

##### consistenceRemarks
Remarks on consistence test. Use to report results and actions if initial test not compliant.  
*Type:* string  
*Example:* ``Initial result 90. Re-test after water added.``

##### numberCubes
Number of cubes or cyliders taken, for further testing.  
*Type:* number  
*Example:* ``4``

##### pourStart
Date/time of start of pour (for batch), which may be to agitator.  
*Type:* string (Nonetime)  
*Condition:* Required  
*Example:* ``2023-03-02T11:35``

##### pourEnd
Date/time of end of pour (for batch), which may be to agitator.  
*Type:* string (Nonetime)  
*Example:* ``2023-03-02T11:44``

##### ambientTemperature
Ambient air temperature. Units: degC  
*Type:* number  
*Example:* ``8``

##### remarks
Additional remarks, if required.  
*Type:* string  
*Example:* ``Some additional remarks``

