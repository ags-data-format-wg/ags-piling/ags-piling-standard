


# AGS Piling Standard / X. Material

## X.X. agspileFluid

### X.X.1. Object description

Support fluid sampling and compliance testing. Applicable to both bentonite and polymer.

The parent object of [agspileFluid](./Standard_Material_agspileFluid.md) is [agspileConstructionSchedule](./Standard_Construction_agspileConstructionSchedule.md)

[agspileFluid](./Standard_Material_agspileFluid.md) has the following attributes:


- [fluidID](#fluidid)
- [fluidType](#fluidtype)
- [fluidStage](#fluidstage)
- [elementID](#elementid)
- [description](#description)
- [depth](#depth)
- [elevationReference](#elevationreference)
- [descriptionElevationReference](#descriptionelevationreference)
- [depth](#depth)
- [dateTime](#datetime)
- [sampledBy](#sampledby)
- [sampleRemarks](#sampleremarks)
- [density](#density)
- [densityMethod](#densitymethod)
- [densityUnits](#densityunits)
- [fluidLoss](#fluidloss)
- [filterCake](#filtercake)
- [marshFunnel](#marshfunnel)
- [shearStrength](#shearstrength)
- [sandContent](#sandcontent)
- [pH](#ph)
- [testRemarks](#testremarks)


### X.X.2. Attributes

##### fluidID
Identifier, possibly a UUID. This is optional and is not referenced anywhere else in the schema, but it may be beneficial to include this to help with data control and integrity, and some software/applications may require it.  
*Type:* string (identifier)  
*Condition:* Recommended  
*Example:* ``1fb599ab-c040-408d-aba0-85b18bb506c2``

##### fluidType
Type of support fluid  
*Type:* string (recommend using term from [vocabulary](./Codes_Vocab.md))  
*Example:* ``Bentonite``

##### fluidStage
Location and stage of construction process that sample taken from.  
*Type:* string (recommend using term from [vocabulary](./Codes_Vocab.md))  
*Condition:* Recommended  
*Example:* ``Excavation during digging``

##### elementID
Reference to elementID of [agspileBoredElement](./Standard_Bored_agspileBoredElement.md) object for pile from which the fluid sample has been taken, if applicable. Not applicable for samples taken directly from batching plant or storage.  
*Type:* string (reference to elementID of agspileBored object)  
*Condition:* Recommended  
*Example:* ``B1-P001``

##### description
Further description, if required  
*Type:* string  


##### depth
Depth of sample taken from pile, if applicable, below reference datum.  
*Type:* number  
*Example:* ``1.0``

##### elevationReference
Elevation of reference datum from which depth measured for this record  
*Type:* number  


##### descriptionElevationReference
Description of reference datum from which depth measured for this record  
*Type:* string  


##### depth
Depth of sample taken from pile, below reference datum.  
*Type:* number  
*Example:* ``1.0``

##### dateTime
Date/time sample taken  
*Type:* string  
*Example:* ``2016-11-23T14:40``

##### sampledBy
Name of person taking sample  
*Type:* string  
*Example:* ``A N Other``

##### sampleRemarks
Sampling remarks  
*Type:* string  


##### density
Density. Units as given in densityUnits  
*Type:* number  
*Example:* ``1.03``

##### densityMethod
Density test method  
*Type:* number  
*Example:* ``Mud balance``

##### densityUnits
Units for density test result.  
*Type:* string  
*Example:* ``g/cm3``

##### fluidLoss
Fluid loss (30 minute test) from low-temperature fluid loss test. Units: ml  
*Type:* number  


##### filterCake
Filter cake thickness at 30 minustes  from low-temperature fluid loss test. Units: mm.  
*Type:* number  


##### marshFunnel
viscosity as Marsh funnel time. Units: s  
*Type:* number  


##### shearStrength
Shear strength (10 minute gel strength) from Fann viscometer test. Units: Pa  
*Type:* number  


##### sandContent
Sand content (as volume) from sand screen set. Units: %  
*Type:* number  


##### pH
pH using electrical pH meter to BS3445. Unitless.  
*Type:* number  


##### testRemarks
Testing remarks  
*Type:* string  


