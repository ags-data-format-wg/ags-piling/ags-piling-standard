


# AGS Piling Standard / X. Common

## X.X. agspileRftMain

### X.X.1. Object description

As built pile main (longitudinal) reinforcement. If main bars are lapped or jointed, use one object per bar length with overlapping section depths to represent laps if required. Segmental precast piles should have one object per segment. Separate section objects should be used for any other change in main (longitudinal) reinforcement. Reference elevation that depth is measured from is defined in design schedule.

[agspileRftMain](./Standard_Common_agspileRftMain.md) may be embedded in any of the following potential parent objects: 

- [agspileBoredElement](./Standard_Bored_agspileBoredElement.md)
-  [agspileCFAElement](./Standard_CFA_agspileCFAElement.md)

[agspileRftMain](./Standard_Common_agspileRftMain.md) has the following attributes:


- [topDepth](#topdepth)
- [bottomDepth](#bottomdepth)
- [mainNumber](#mainnumber)
- [mainBar](#mainbar)
- [mainJoint](#mainjoint)
- [mainLap](#mainlap)
- [mainDetails](#maindetails)
- [remarks](#remarks)


### X.X.2. Attributes

##### topDepth
Depth to top of section. Depth defined as depth below rftTopElevation in parent object. Typically the top of the main bars for this section, but sections may sometimes be used indicate a change in number of bars in the section, e.g. some bars curtailed. May lie above bottom of previous bar if lapped.   
*Type:* number  
*Example:* ``0``

##### bottomDepth
Depth to top of bottom of section. Typically the bottom of the main bars for this section, but sections may sometimes be used indicate a change in number of bars in the section, e.g. some bars curtailed.  
*Type:* number  
*Example:* ``10``

##### mainNumber
Number of main (longitudinal) bars  
*Type:* number  
*Example:* ``12``

##### mainBar
Type and size of main (longitudinal) bars  
*Type:* string  
*Example:* ``B25``

##### mainJoint
Type and nature of main reinforcement joint, if applicable.  
*Type:* string (recommend using term from [vocabulary](./Codes_Vocab.md))  
*Example:* ``Lap``

##### mainLap
Lap length, if applicable. Refers to bottom of section. Lap length should be included within topDepth and bottomDepth. Units: m.  
*Type:* number  
*Example:* ``1.2``

##### mainDetails
Main reinforcement additional details  
*Type:* string  


##### remarks
Additional remarks, if required.  
*Type:* string  
*Example:* ``Some additional remarks``

