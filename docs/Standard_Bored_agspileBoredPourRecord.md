


# AGS Piling Standard / X. Bored

## X.X. agspileBoredPourRecord

### X.X.1. Object description

Time based detailed log of the concrete pour. Provides information for graph of rise of top of concrete or grout level versus volume placed by batch, and associated data. May also inlcude casing extrction and support fluid levels. Further details relating to the concrete, e.g. test results, are provided separately in [agspileConcrete](./Standard_Material_agspileConcrete.md) objects (linked by reference). Concrete strengh and class details included in parent [agspileBoredElement](./Standard_Bored_agspileBoredElement.md) object.

The parent object of [agspileBoredPourRecord](./Standard_Bored_agspileBoredPourRecord.md) is [agspileBoredElement](./Standard_Bored_agspileBoredElement.md)

[agspileBoredPourRecord](./Standard_Bored_agspileBoredPourRecord.md) has associations (reference links) with the following objects: 

- [agspileConcrete](./Standard_Material_agspileConcrete.md)

[agspileBoredPourRecord](./Standard_Bored_agspileBoredPourRecord.md) has the following attributes:


- [concreteID](#concreteid)
- [isSplitLoad](#issplitload)
- [batchDateTime](#batchdatetime)
- [arrivalDateTime](#arrivaldatetime)
- [consistenceTestType](#consistencetesttype)
- [consistenceResult](#consistenceresult)
- [consistenceRemarks](#consistenceremarks)
- [numberCubes](#numbercubes)
- [pourStart](#pourstart)
- [pourEnd](#pourend)
- [volumePile](#volumepile)
- [volumeCumulativePile](#volumecumulativepile)
- [concreteTopDepthStart](#concretetopdepthstart)
- [concreteTopDepthEnd](#concretetopdepthend)
- [tremieDepthStart](#tremiedepthstart)
- [tremieDepthEnd](#tremiedepthend)
- [casingDepthStart](#casingdepthstart)
- [casingDepthEnd](#casingdepthend)
- [fluidTopDepthStart](#fluidtopdepthstart)
- [fluidTopDepthEnd](#fluidtopdepthend)
- [elevationReference](#elevationreference)
- [descriptionElevationReference](#descriptionelevationreference)
- [ambientTemperature](#ambienttemperature)
- [remarks](#remarks)


### X.X.2. Attributes

##### concreteID
Identifier for a load/batch of concrete used in this pile. Typically the concrete ticket reference. Must be unique within the file.  Used to cross-reference concreteID in [agspileConcrete](./Standard_Material_agspileConcrete.md)  
*Type:* string (identifier)  
*Condition:* Required  
*Example:* ``21364285``

##### isSplitLoad
Flag to indicate that concrete from this batch/load has been used in more than one pile, i.e true if this is the case. May be left blank if false.  
*Type:* boolean  
*Example:* ``false``

##### batchDateTime
Date and time of batching of concrete. May be at remote batching plant or on site.  
*Type:* string (Nonetime)  
*Example:* ``2023-03-02T10:32``

##### arrivalDateTime
Date and time of arrival of concrete to work site.  
*Type:* string (Nonetime)  
*Example:* ``2023-03-02T11:20``

##### consistenceTestType
Consistence test type  
*Type:* string (enum from list below)  
\- None  
*Example:* ``Slump``

##### consistenceResult
Consistence test result. Units mm for slump and flow table tests.  
*Type:* number  
*Example:* ``190``

##### consistenceRemarks
Remarks on consistence test. Use to report results and actions if initial test not compliant.  
*Type:* string  
*Example:* ``Initial result 90. Re-test after water added.``

##### numberCubes
Number of cubes or cyliders taken, for further testing.  
*Type:* number  
*Example:* ``4``

##### pourStart
Date/time of start of pour (for batch)  
*Type:* string (Nonetime)  
*Condition:* Required  
*Example:* ``2023-03-02T11:35``

##### pourEnd
Date/time of end of pour (for batch)  
*Type:* string (Nonetime)  
*Example:* ``2023-03-02T11:44``

##### volumePile
Volume of concrete delivered to this pile from this load/batch. Units: m3.  
*Type:* number  
*Example:* ``8``

##### volumeCumulativePile
Cumulative volume of concrete delivered to this pile at end of this load/batch. Units: m3  
*Type:* number  
*Example:* ``16``

##### concreteTopDepthStart
Depth to top of concrete at start of pour.  
*Type:* number  
*Example:* ``44.9``

##### concreteTopDepthEnd
Depth to top of concrete at end of pour of pour  
*Type:* number  
*Example:* ``37.5``

##### tremieDepthStart
Depth to base of tremie at start of pour  
*Type:* number  
*Example:* ``49``

##### tremieDepthEnd
Depth to base of tremie at end of pour  
*Type:* number  
*Example:* ``49``

##### casingDepthStart
Depth to base of casing at start of pour  
*Type:* number  
*Example:* ``12``

##### casingDepthEnd
Depth to base of casing at end of pour  
*Type:* number  
*Example:* ``12``

##### fluidTopDepthStart
Depth to top of support fluid at start of pour  
*Type:* number  
*Example:* ``6.8``

##### fluidTopDepthEnd
Depth to top of support fluid at end of pour  
*Type:* number  
*Example:* ``0.5``

##### elevationReference
Elevation of reference datum from which depth measured for this record  
*Type:* number  
*Example:* ``3.57``

##### descriptionElevationReference
Description of reference datum from which depth measured for this record  
*Type:* string  
*Example:* ``Top of casing``

##### ambientTemperature
Ambient air temperature. Units: degC  
*Type:* number  
*Example:* ``8``

##### remarks
Additional remarks, if required.  
*Type:* string  
*Example:* ``Some additional remarks``

