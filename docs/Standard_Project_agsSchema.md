


# AGS Piling Standard / 2. Project

## 2.2. agsSchema

### 2.2.1. Object description

Defines the schema used for the [AGSi file](./Standard_General_Definitions.md#agsi-file). It is recommended that, where possible, this object is output at the top of the file, for human readability.

The parent object of [agsSchema](./Standard_Root_agsSchema.md) is [root](./Standard_Root_Intro.md)

[agsSchema](./Standard_Root_agsSchema.md) has the following attributes:


- [name](#name)
- [version](#version)
- [link](#link)


### 2.2.2. Attributes

##### name
Name of the AGS schema used herein.  
*Type:* string  
*Condition:* Required  
*Example:* ``AGS Piling``

##### version
Version of the named AGS schema used herein.  
*Type:* string  
*Condition:* Required  
*Example:* ``0.4.0``

##### link
Web link ([uri](./Standard_General_Formats.md#166-uri)) to the AGS schema used herein.  
*Type:* string ([uri](./Standard_General_Formats.md#166-uri))  
*Example:* ``TBC``

