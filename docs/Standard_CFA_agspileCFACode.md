


# AGS Piling Standard / X. CFA

## X.X. agspileCFACode

### X.X.1. Object description

Heading code definitions used by DataBlock object for CFA rig data.

The parent object of [agspileCFACode](./Standard_CFA_agspileCFACode.md) is [agspileConstructionSchedule](./Standard_Construction_agspileConstructionSchedule.md)

[agspileCFACode](./Standard_CFA_agspileCFACode.md) has the following attributes:


- [codeID](#codeid)
- [description](#description)
- [dataType](#datatype)
- [dataRequirement](#datarequirement)
- [units](#units)
- [signConvention](#signconvention)
- [remarks](#remarks)


### X.X.2. Attributes

##### codeID
Code referenced by relevant DataBlock object  
*Type:* string (identifier)  
*Condition:* Required  
*Example:* ``AugerPenetrationRate``

##### description
Short description of what the code represents.  
*Type:* string  
*Condition:* Required  
*Example:* ``Auger penetration rate/speed during boring``

##### dataType
Recommended data type for value to be provided  
*Type:* string  
*Example:* ``Number``

##### dataRequirement
Recommended format or other data requirement  
*Type:* string  
*Example:* ``Nonetime``

##### units
Units of measurement, if applicable.  
*Type:* string  
*Example:* ``m/min``

##### signConvention
Sign convention, if this requires clarification  
*Type:* string  


##### remarks
Additional remarks, if required.  
*Type:* string  
*Example:* ``Some additional remarks``

