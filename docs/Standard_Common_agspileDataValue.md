


# AGS Piling Standard / X. Common

## X.X. agspileDataValue

### X.X.1. Object description

Each agsiDataValue object provides the data for a single defined property. The property value conveyed may be numeric or text.

[agspileDataValue](./Standard_Common_agspileDataValue.md) may be embedded in any of the following potential parent objects: 

- [agspileBoredElement](./Standard_Bored_agspileBoredElement.md)
-  [agspileCFAElement](./Standard_CFA_agspileCFAElement.md)

[agspileDataValue](./Standard_Common_agspileDataValue.md) has associations (reference links) with the following objects: 

- [agsProjectCode](./Standard_Project_agsProjectCode.md)

[agspileDataValue](./Standard_Common_agspileDataValue.md) has the following attributes:


- [codeID](#codeid)
- [valueNumeric](#valuenumeric)
- [valueText](#valuetext)
- [remarks](#remarks)
- [remarks](#remarks)


### X.X.2. Attributes

##### codeID
Code that identifies the property. Codes should be defined in either an [agsProjectCode](./Standard_Project_agsProjectCode.md) object, or in the code dictionary defined in the relevant [agsProjectCodeSet](./Standard_Project_agsProjectCodeSet.md) object. The codes used by the instances of this object contained within a single parent object instance shall be unique.  
*Type:* string (Reference to codeID of relevant [agsProjectCode](./Standard_Project_agsProjectCode.md) object, or code defined in codelist specified in relevant [agsProjectCodeSet](./Standard_Project_agsProjectCodeSet.md) object)  
*Condition:* Required  
*Example:* ``subAssetID``

##### valueNumeric
Numeric value of a single property.  
*Type:* number  
*Example:* ``100``

##### valueText
Text value for property, if applicable.  
*Type:* string  
*Example:* ``B1_Abut1``

##### remarks
Additional remarks, if required.  
*Type:* string  
*Example:* ``Some remarks if required``

##### remarks
Additional remarks, if required.  
*Type:* string  
*Example:* ``Some additional remarks``

