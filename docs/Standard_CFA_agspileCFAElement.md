


# AGS Piling Standard / X. CFA

## X.X. agspileCFAElement

### X.X.1. Object description

Summary construction data for CFA piles. The embedded child objects can be used to provide full construction details.

The parent object of [agspileCFAElement](./Standard_CFA_agspileCFAElement.md) is [agspileConstructionSchedule](./Standard_Construction_agspileConstructionSchedule.md)

[agspileCFAElement](./Standard_CFA_agspileCFAElement.md) contains the following embedded child objects: 

- [agspileCFAProgress](./Standard_CFA_agspileCFAProgress.md)
- [agspileCFADepth](./Standard_CFA_agspileCFADepth.md)
- DataBlock for rig data
- [agspileRftMain](./Standard_Common_agspileRftMain.md)
- [agspileRftShear](./Standard_Common_agspileRftShear.md)
- [agspileIntegrity](./Standard_Common_agspileIntegrity.md)
- [agspileInstallation](./Standard_Common_agspileInstallation.md)
- [agspileNonConformance](./Standard_Common_agspileNonConformance.md)
- [agspileObservation](./Standard_Common_agspileObservation.md)
- [agspileInspection](./Standard_Common_agspileInspection.md)
- [agspileGeology](./Standard_Common_agspileGeology.md)
- [agspileDataValue](./Standard_Common_agspileDataValue.md)

[agspileCFAElement](./Standard_CFA_agspileCFAElement.md) has associations (reference links) with the following objects: 

- [agspileDesignElement](./Standard_Design_agspileDesignElement.md)
- [agspileCFAPourRecord](./Standard_CFA_agspileCFAPourRecord.md)
- [agspileTest](./Standard_PileTest_agspileTest.md)
- [agsProjectDocumentSet](./Standard_Project_agsProjectDocumentSet.md)

[agspileCFAElement](./Standard_CFA_agspileCFAElement.md) has the following attributes:


- [elementID](#elementid)
- [elementUUID](#elementuuid)
- [elementAssetID](#elementassetid)
- [designID](#designid)
- [elementCategory](#elementcategory)
- [elementType](#elementtype)
- [elementSize](#elementsize)
- [elementDetail](#elementdetail)
- [isInProgress](#isinprogress)
- [infoStatus](#infostatus)
- [group](#group)
- [location](#location)
- [coordinates](#coordinates)
- [chainage](#chainage)
- [offset](#offset)
- [platformElevation](#platformelevation)
- [machineElevation](#machineelevation)
- [castElevation](#castelevation)
- [cutoffElevation](#cutoffelevation)
- [length](#length)
- [bottomElevation](#bottomelevation)
- [casingTopElevation](#casingtopelevation)
- [casingLength](#casinglength)
- [verticalityRake](#verticalityrake)
- [verticalityBearing](#verticalitybearing)
- [verticalityMethod](#verticalitymethod)
- [prebore](#prebore)
- [elementStart](#elementstart)
- [elementEnd](#elementend)
- [elementEndAll](#elementendall)
- [agspileCFAProgress](#agspilecfaprogress)
- [agspileCFADepth](#agspilecfadepth)
- [geologySummary](#geologysummary)
- [agspileGeology](#agspilegeology)
- [augerRevTotal](#augerrevtotal)
- [augerRevAverage](#augerrevaverage)
- [augerRevAverageTargetMin](#augerrevaveragetargetmin)
- [augerRevAverageTargetMax](#augerrevaveragetargetmax)
- [concreteMix](#concretemix)
- [concreteStrength](#concretestrength)
- [concreteDC](#concretedc)
- [volumeDelivered](#volumedelivered)
- [volumeTheoretical](#volumetheoretical)
- [oversupply](#oversupply)


### X.X.2. Attributes

##### elementID
Pile/wall element reference. Must be unique within a file. May be referenced by other objects, e.g. [agspileCFAPourRecord](./Standard_CFA_agspileCFAPourRecord.md), [agspileTest](./Standard_PileTest_agspileTest.md).  
*Type:* string  
*Condition:* Required  
*Example:* ``W1-P001``

##### elementUUID
Universal/global unique identifier (UUID) for the pile. This is optional and is not referenced anywhere else in the schema, but it may be beneficial to include this to help with data control and integrity.  
*Type:* string  
*Example:* ``1fb599ab-c040-408d-aba0-85b18bb506c2``

##### elementAssetID
Unique asset ID for the element  
*Type:* string  


##### designID
Reference to designID of agspileDesign object for corresponding pile in design schedule, if applicable  
*Type:* string (reference to designID of agspileDesign object)  
*Example:* ``W027``

##### elementCategory
Category of element, i.e what it is in general term (not to be confused with type of element).  
*Type:* string (recommend using term from [vocabulary](./Codes_Vocab.md))  
*Example:* ``Secant pile male``

##### elementType
Element (pile/wall) type input as relevant code from list provided.  
*Type:* string (enum from list below)  
\- None  
*Example:* ``CFA``

##### elementSize
Nominal diameter or section size/reference, as applicable. Units defined in parent [agspileConstructionSchedule](./Standard_Construction_agspileConstructionSchedule.md) object.  
*Type:* string  
*Example:* ``880/760``

##### elementDetail
Summary of additional important construction details  
*Type:* string  


##### isInProgress
Construction status of pile. Set as True if in progress, i.e not yet complete.  
*Type:* boolean  
*Example:* ``false``

##### infoStatus
Status of information relating to pile/wall element  
*Type:* string (recommend using term from [vocabulary](./Codes_Vocab.md))  
*Example:* ``DRAFT``

##### group
Cap, group or wall reference, as applicable  
*Type:* string  
*Example:* ``W/A``

##### location
Site location subdivision (within project) reference  
*Type:* string  
*Example:* ``Wall W1``

##### coordinates
As built plan coordinates of the element, as a None (x,y). Coordinate system defined in parent object.  
*Type:* array (None)  
*Example:* ``[565.25,421.09]``

##### chainage
Chainage. Reference alignment defined in parent [agspileConstructionSchedule](./Standard_Construction_agspileConstructionSchedule.md) object.  
*Type:* string  
*Example:* ``12+345.67``

##### offset
Offset from alignment used for chainage. Alignment used defined in parent object.  
*Type:* string  
*Example:* ``-12.2``

##### platformElevation
Elevation of working/piling platform (commencing surface)  
*Type:* number  
*Example:* ``16.15``

##### machineElevation
Elevation of working surface on which base machine stands, if different from elevationPlatform  
*Type:* number  
*Example:* ``16.6``

##### castElevation
Elevation of pile/wall element head level as constructed, before trimming, i.e casting level.  
*Type:* number  
*Example:* ``16.05``

##### cutoffElevation
Elevation of pile/wall element cut-off level as constructed  
*Type:* number  
*Example:* ``15.5``

##### length
Length below cut-off level (measured along pile)  
*Type:* number  
*Example:* ``20.3``

##### bottomElevation
Elevation of the bottom/toe of the pile  
*Type:* number  
*Example:* ``-10.7``

##### casingTopElevation
Elevation of top of casing when installed to full depth. Only applicable for cased CFA piles.  
*Type:* number  
*Example:* ``11.2``

##### casingLength
Length of casing (installed to full depth).  
*Type:* number  
*Example:* ``15``

##### verticalityRake
Actual verticality or gradient of raked pile (as H/V from vertical)  
*Type:* string  
*Example:* ``1/124``

##### verticalityBearing
Bearing of non vertical pile (degrees from project grid y/N axis)  
*Type:* number  
*Example:* ``135``

##### verticalityMethod
Method of verticality measurement  
*Type:* string  
*Example:* ``Rig mast survey``

##### prebore
Pre-bore summary details (or other relevant prior operation)  
*Type:* string  
*Example:* ``10m at 600 dia``

##### elementStart
Start date/time of element construction (excluding pre-boring)  
*Type:* string (Nonetime)  
*Example:* ``2016-11-23T10:20``

##### elementEnd
End date/time of element (excluding trimming/testing)  
*Type:* string (Nonetime)  
*Example:* ``2016-11-23T17:55``

##### elementEndAll
End date of element (all operations)  
*Type:* string (Nonetime)  
*Example:* ``2016-12-12T12:30``

##### agspileCFAProgress
Array of embedded elementEnd objects. Used to provide time based summary of construction progress.  
*Type:* array (of [agspileCFAProgress](./Standard_CFA_agspileCFAProgress.md) objects)  


##### agspileCFADepth
Array of embedded elementEndAll objects. Used to providedepth based description of the pile as constructed.  
*Type:* array (of [agspileCFADepth](./Standard_CFA_agspileCFADepth.md) objects)  


##### geologySummary
Summary of ground conditions encountered. Optional if full details provided in agspileCommonGeology  
*Type:* string  


##### agspileGeology
Array of embedded [agspileGeology](./Standard_Common_agspileGeology.md) objects. May be used to record ground conditions encountered during element construction.  
*Type:* array (of [agspileGeology](./Standard_Common_agspileGeology.md) objects)  


##### augerRevTotal
Total number of auger revolutions during boring  
*Type:* number  
*Example:* ``228``

##### augerRevAverage
Average number of auger revolutions per metre during boring  
*Type:* number  
*Example:* ``11.2``

##### augerRevAverageTargetMin
Target minimum number of auger revolutions per metre during boring  
*Type:* number  
*Example:* ``5``

##### augerRevAverageTargetMax
Target maximum number of auger revolutions per metre during boring  
*Type:* number  
*Example:* ``15``

##### concreteMix
Concrete mix designation  
*Type:* string  
*Example:* ``C40-1``

##### concreteStrength
Concrete strength class  
*Type:* string  
*Example:* ``C32/40``

##### concreteDC
Concrete DC class  
*Type:* string  
*Example:* ``DC-2``

##### volumeDelivered
Total volume of concrete delivered into the pile. Units: m3.  
*Type:* string  
*Example:* ``57.1``

##### volumeTheoretical
Theoretical volume of pile. Top of pile assumed should be same as that  used for volumeDelivered. Units: m3.  
*Type:* string  
*Example:* ``51.35``

##### oversupply
Pile oversupply: excess of volume delivered over theoretical as percentage. Units: %.  
*Type:* number  
*Example:* ``11.2``

##### oversupplyTargetMin
Target minimum oversupply, if applicable. Units: %.  
*Type:* number  
*Example:* ``10``

##### oversupplyTargetMax
Target maximum oversupply, if applicable. Units: %.  
*Type:* number  
*Example:* ``15``

##### concretePressureLocation
Location of concrete pressure measurement  
*Type:* string  
*Example:* ``Swan neck``

##### isPullDownForce
Was any pull down force used during pile construction? Yes = true. Further details may be provided in remarksCFAConstruction.  
*Type:* boolean  
*Example:* ``false``

##### isDelayStoppage
Were there any delays or stoppages during pile construction? Yes = true. Further details to be provided in remarksCFAConstruction if false.  
*Type:* boolean  
*Example:* ``true``

##### isConcreteTip
Was cocnrete observed to be flowing under pressue out of the top of the bore prior to the tip of the boring tool emerging? Yes = true. Further details to be provded in remarksCFAConstruction if false.  
*Type:* boolean  
*Example:* ``true``

##### remarksCFAConstruction
Remarks specific to CFA pile construction process, e.g. delays or stoppages or other issues to be reported.  
*Type:* string  
*Example:* ``20 min delay during concreting due to rejection of non compliant concrete``

##### rigDataDataBlock
Single DataBlock object used to provide detailed pile construction data from rig monitoring system.  
*Type:* object (single DataBlock object)  


##### rftTopElevation
Elevation of top of main reinforcement  
*Type:* number  
*Example:* ``16.7``

##### rftProjection
Reinforcement projection above cut-off level. Units as for reinforcement length.  
*Type:* number  
*Example:* ``1.2``

##### rftCover
Nominal minimum cover to reinforcement. Units: mm  
*Type:* number  
*Example:* ``75``

##### rftCageReference
Reinforcement cage reference.  
*Type:* string  
*Example:* ``A``

##### rftMainDescription
Description of main (longitudinal) reinforcement. Not required if full details provided in [agspileRftMain](./Standard_Common_agspileRftMain.md).  
*Type:* string  
*Example:* ``12xB25, 10m length incl 1.2m projection above COL``

##### rftShearDescription
Description of shear reinforcement. Not required if full details provided in [agspileRftShear](./Standard_Common_agspileRftShear.md).  
*Type:* string  
*Example:* ``B12@200``

##### agspileRftMain
Array of embedded agspileMainRft objects that fully define the main (longitudinal) reinforcement as built  
*Type:* object (of [agspileRftMain](./Standard_Common_agspileRftMain.md) objects)  


##### agspileRftShear
Array of embedded [agspileRftShear](./Standard_Common_agspileRftShear.md) objects that fully define the shear reinforcement as built  
*Type:* object (of [agspileRftShear](./Standard_Common_agspileRftShear.md) objects)  


##### installationSummary
Instrumentation or other installation type/summary. Optional if full details provided in agspileCommonInstallation  
*Type:* string  
*Example:* ``Inclinometer duct``

##### agspileInstallation
Array of embedded [agspileInstallation](./Standard_Common_agspileInstallation.md) objects. May be used for instrumentation or any other types of installation.  
*Type:* array (of [agspileInstallation](./Standard_Common_agspileInstallation.md) objects)  


##### integritySummary
Integrity testing type/summary. Optional if full details provided in agspileCommonIntegrity  
*Type:* string  
*Example:* ``Transient dynamic``

##### agspileIntegrity
Array of embedded [agspileIntegrity](./Standard_Common_agspileIntegrity.md) objects. Allows for more than one integrity test per pile, e.g. where retest required.  
*Type:* array (of [agspileIntegrity](./Standard_Common_agspileIntegrity.md) objects)  


##### observationSummary
Summary of inspections or observations made during construction. Optional if full details provided in [agspileObservation](./Standard_Common_agspileObservation.md)  
*Type:* string  
*Example:* ``Seepage at 4.5m``

##### agspileObservation
Array of embedded [agspileDataValue](./Standard_Common_agspileDataValue.md) objects. May be used to record observations made during element construction.  
*Type:* array (of [agspileObservation](./Standard_Common_agspileObservation.md) objects)  


##### inspectionSummary
Summary of inspections or observations made during construction. Optional if full details provided in [agspileInspection](./Standard_Common_agspileInspection.md)  
*Type:* string  


##### agspileInspection
Array of embedded [agspileInspection](./Standard_Common_agspileInspection.md) objects. May be used to record inspections undertaken during element construction.  
*Type:* array (of [agspileInspection](./Standard_Common_agspileInspection.md) objects)  


##### nonConformanceSummary
Non-conformance report reference or summary description. Optional if full details provided in agspileCommonNonConformance  
*Type:* string  
*Example:* ``NCR/W/003``

##### agspileNonConformance
Array of embedded [agspileNonConformance](./Standard_Common_agspileNonConformance.md) objects  
*Type:* array (of [agspileNonConformance](./Standard_Common_agspileNonConformance.md) objects)  


##### agspileDataValue
Array of embedded [agspileDataValue](./Standard_Common_agspileDataValue.md) objects. May be used to provide additional project specific metadata for the pile, or user defined data.  
*Type:* array (of [agspileDataValue](./Standard_Common_agspileDataValue.md) objects)  


##### documentSetID
Reference to documentation relating to element (reference to [agsProjectDocumentSet](./Standard_Project_agsProjectDocumentSet.md) object), e.g. pile record sheets for this element.  
*Type:* string (of documentSetID objects)  
*Example:* ``Ph1PileRecords_W1_P001``

##### remarks
Additional remarks, if required.  
*Type:* string  
*Example:* ``Some additional remarks``

