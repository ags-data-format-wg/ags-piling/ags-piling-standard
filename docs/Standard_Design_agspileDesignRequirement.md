


# AGS Piling Standard / X. Design

## X.X. agspileDesignRequirement

### X.X.1. Object description

Employer's requirements for piles, pile groups or wall elements. Typically used to identify requirement for individual pile/wall elements. May alternatively be used to define requirements for pile groups, or generic requirements for a series of piles. Relevant to both Employer and Contractor design of pile/wall elements. Typically completed by Employer's designer.

The parent object of [agspileDesignRequirement](./Standard_Design_agspileDesignRequirement.md) is [agspileDesignSchedule](./Standard_Design_agspileDesignSchedule.md)

[agspileDesignRequirement](./Standard_Design_agspileDesignRequirement.md) has associations (reference links) with the following objects: 

- [agspileDesignLoad](./Standard_Design_agspileDesignLoad.md)
- [agspileDesignElement](./Standard_Design_agspileDesignElement.md)

[agspileDesignRequirement](./Standard_Design_agspileDesignRequirement.md) has the following attributes:


- [elementID](#elementid)
- [elementCategory](#elementcategory)
- [elementType](#elementtype)
- [elementSizeContraint](#elementsizecontraint)
- [infoStatus](#infostatus)
- [group](#group)
- [location](#location)
- [coordinates](#coordinates)
- [chainage](#chainage)
- [offset](#offset)
- [platformElevation](#platformelevation)
- [cutoffElevation](#cutoffelevation)
- [lengthConstraint](#lengthconstraint)
- [verticalityRake](#verticalityrake)
- [verticalityBearing](#verticalitybearing)
- [loadID](#loadid)
- [elementOtherRequirement](#elementotherrequirement)
- [remarks](#remarks)


### X.X.2. Attributes

##### elementID
Element reference. Must be unique within all agsPileDesignRequirement objects within a file, but the ID used here may be re-used in [agspileDesignElement](./Standard_Design_agspileDesignElement.md) and in constructed pile objects (agspileBoredElement etc.). May be referenced by [agspileDesignElement](./Standard_Design_agspileDesignElement.md).  
*Type:* string  
*Condition:* Required  
*Example:* ``B1-P001``

##### elementCategory
Category of element, i.e what it is in general term (not to be confused with type of element).  
*Type:* string (recommend using term from [vocabulary](./Codes_Vocab.md))  
*Example:* ``Bearing pile``

##### elementType
Permitted element (pile/wall) type(s) input as relevant code from list provided. If multiple types are permitted these shall be separated with defined separatorType in parent agspileDesign object  
*Type:* string (enum from list below)  
\- None  
*Example:* ``BORED+CFA``

##### elementSizeContraint
Permitted section size(s). Include shape information if applicable.  
*Type:* string  
*Example:* ``750 to 900mm ``

##### infoStatus
Status of information relating to pile/wall element  
*Type:* string (recommend using term from [vocabulary](./Codes_Vocab.md))  
*Example:* ``DRAFT``

##### group
Cap, group or wall reference, as applicable  
*Type:* string  
*Example:* ``PC20``

##### location
Site location subdivision (within project) reference  
*Type:* string  
*Example:* ``Area B``

##### coordinates
As built plan coordinates of the element, as a coordinate tuple (x,y). Coordinate system defined in parent object.  
*Type:* array (coordinate tuple)  
*Example:* ``[565.25,421.09]``

##### chainage
Chainage. Reference alignment defined in parent [agspileDesignSchedule](./Standard_Design_agspileDesignSchedule.md) object.  
*Type:* string  
*Example:* ``12+345.67``

##### offset
Offset from alignment used for chainage. Alignment used defined in parent object.  
*Type:* string  
*Example:* ``+10.7``

##### platformElevation
Assumed elevation of working/piling platform  
*Type:* number  
*Example:* ``16.1``

##### cutoffElevation
Elevation of pile/wall element cut-off level as specified  
*Type:* number  
*Example:* ``15.5``

##### lengthConstraint
Constraints on toe elevation or pile length  
*Type:* string  
*Example:* ``Toe -10.5 to -12.5mOD``

##### verticalityRake
Verticality or gradient of raked pile (as H/V from vertical)  
*Type:* string  
*Example:* ``1/15``

##### verticalityBearing
Bearing of non vertical pile (degrees from project grid y/N axis)  
*Type:* number  
*Example:* ``135``

##### loadID
Reference to loadID of agsDesignLoad object that defines the loads and load combinations applied to this design element  
*Type:* string (reference to loadID of agsDesignLoad object)  


##### elementOtherRequirement
Other constraints and requirements, .e.g. instrumentation, permanent casing.  
*Type:* string  
*Example:* ``Double sleeve for pile test and inclinometer. See Drawing XXX.``

##### remarks
Additional remarks, if required.  
*Type:* string  
*Example:* ``Some additional remarks``

