


# AGS Piling Standard / X. Common

## X.X. agspileInspection

### X.X.1. Object description

Record of inspections.

[agspileInspection](./Standard_Common_agspileInspection.md) may be embedded in any of the following potential parent objects: 

- [agspileBoredElement](./Standard_Bored_agspileBoredElement.md)
-  [agspileCFAElement](./Standard_CFA_agspileCFAElement.md)

[agspileInspection](./Standard_Common_agspileInspection.md) has the following attributes:


- [inspectionID](#inspectionid)
- [inspectionDateTime](#inspectiondatetime)
- [inspectionType](#inspectiontype)
- [inspectionMethod](#inspectionmethod)
- [inspectionResult](#inspectionresult)
- [madeBy](#madeby)
- [remarks](#remarks)


### X.X.2. Attributes

##### inspectionID
Identifier for this observation or inspection record. May be local to this file or a UUID as required/specified. This is optional and not referenced anywhere else in the schema, but it may be beneficial to include this to help with data control and integrity, and some software/applications may require it.  
*Type:* string (identifier)  


##### inspectionDateTime
Date/time of inspection.  
*Type:* string (None)  
*Example:* ``2016-11-23T14:35``

##### inspectionType
Type of inspection.  
*Type:* string  
*Example:* ``Base inspection``

##### inspectionMethod
Method of inspection, if applicable, e.g. tools used to assist.  
*Type:* string  
*Example:* ``Light lowered to base. Weighted tape.``

##### inspectionResult
Descripton of findings from inspection.  
*Type:* string  


##### madeBy
Name of peron(s) carrying out inspection  
*Type:* string  
*Example:* ``A N Other``

##### remarks
Additional remarks, if required.  
*Type:* string  
*Example:* ``Some additional remarks``

