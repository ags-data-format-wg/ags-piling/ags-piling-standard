


# AGS Piling Standard / X. Material

## X.X. agspileConcrete

### X.X.1. Object description

Data relating to for concrete delivered to site or batched on site before transport to work location. Can also be used for batches of concrete made at work location. For piles precast off-site the work area is the precast yard. Piles/elements that this concrete is used for shall be identified in the relevant pour record objects, e.g. [agspileBoredPourRecord](./Standard_Bored_agspileBoredPourRecord.md), which should reference back to this object for the delivery data and test results. Details of strength testing are covered by child objects.

The parent object of [agspileConcrete](./Standard_Material_agspileConcrete.md) is [agspileConstructionSchedule](./Standard_Construction_agspileConstructionSchedule.md)

[agspileConcrete](./Standard_Material_agspileConcrete.md) contains the following embedded child objects: 

- [agspileConcreteStrengthTest](./Standard_Material_agspileConcreteStrengthTest.md)

[agspileConcrete](./Standard_Material_agspileConcrete.md) has associations (reference links) with the following objects: 

- [agspileBoredPourRecord](./Standard_Bored_agspileBoredPourRecord.md)
- [agspileCFAPourRecord](./Standard_CFA_agspileCFAPourRecord.md)

[agspileConcrete](./Standard_Material_agspileConcrete.md) has the following attributes:


- [concreteID](#concreteid)
- [concreteUUID](#concreteuuid)
- [supplier](#supplier)
- [supplierLocation](#supplierlocation)
- [batchDateTime](#batchdatetime)
- [concreteMix](#concretemix)
- [concreteStrength](#concretestrength)
- [concreteDC](#concretedc)
- [agspileConcreteStrengthTest](#agspileconcretestrengthtest)
- [remarks](#remarks)


### X.X.2. Attributes

##### concreteID
Identifier for a batch of concrete delivered ready for use. Batch/ticket reference may be used. Must be unique within the file.  Used to cross-reference concreteID in relevant pour record objects.  
*Type:* string (identifier)  
*Condition:* Required  
*Example:* ``12345678``

##### concreteUUID
Universal/global unique identifier (UUID) for this delivery of concrete. This is optional and is not referenced anywhere else in the schema, but it may be beneficial to include this to help with data control and integrity.  
*Type:* string (UUID)  
*Example:* ``1fb599ab-c040-408d-aba0-85b18bb506c2``

##### supplier
Name of supplier organisation  
*Type:* string  
*Example:* ``ABC Concrete Ltd``

##### supplierLocation
Location of supplier, or 'site' if batched on project site.  
*Type:* string  
*Example:* ``Wokingham``

##### batchDateTime
Date and time of batching of concrete. May be at remote batching plant or on site.  
*Type:* string (None)  
*Example:* ``2023-03-02T10:32``

##### concreteMix
Concrete mix designation, as given on delivery documentation  
*Type:* string  
*Example:* ``C40-1``

##### concreteStrength
Concrete strength class, as given on delivery documentation  
*Type:* string  
*Example:* ``C32/40``

##### concreteDC
Concrete DC class  
*Type:* string  
*Example:* ``DC-2``

##### agspileConcreteStrengthTest
Array of embedded [agspileConcreteStrengthTest](./Standard_Material_agspileConcreteStrengthTest.md) objects providing cube/cylidner strength test data for samples taken from this delivery.  
*Type:* array (of [agspileConcreteStrengthTest](./Standard_Material_agspileConcreteStrengthTest.md) objects)  


##### remarks
Additional remarks, if required.  
*Type:* string  
*Example:* ``Some additional remarks``

