# AGS Piling Standard / 3. Project

*This is a placeholder page, copied from AGSi. Requires review and edit to make it suitable for AGS Piling.*

## 3.9. agsProjectCode

### 3.9.1. Object description

Codes referenced by other parts of the schema such as the [Data group](./Standard_Data_Intro.md) objects (property and parameter codes) and Observation group objects (hole types, legend codes and geology codes). The codes may be project specific or from a standard list, e.g. AGSi standard code list or ABBR codes inherited from AGS factual data. Inclusion of standard [AGSi code list](./Codes_Codelist.md) or AGS ABBR codes used is optional (unless required by specification) provided that the code list used is identifed using [agsProjectCodeSet](./Standard_Project_agsProjectCodeSet.md). Refer to [3.2.4. Codes for Data objects](./Standard_Project_Rules.md#324-codes-for-data-objects) and [3.2.5. Codes where use of AGS ABBR recommended](./Standard_Project_Rules.md#325-codes-where-use-of-ags-abbr-recommended) for further details.

The parent object of [agsProjectCode](./Standard_Project_agsProjectCode.md) is [agsProjectCodeSet](./Standard_Project_agsProjectCodeSet.md)

[agsProjectCode](./Standard_Project_agsProjectCode.md) has associations (reference links) with the following objects: 

- [agsiDataPropertyValue](./Standard_Data_agsiDataPropertyValue.md)
- [agsiDataPropertySummary](./Standard_Data_agsiDataPropertySummary.md)
- [agsiDataParameterValue](./Standard_Data_agsiDataParameterValue.md)

[agsProjectCode](./Standard_Project_agsProjectCode.md) has the following attributes:


- [codeID](#codeid)
- [description](#description)
- [units](#units)
- [isStandard](#isstandard)
- [remarks](#remarks)


### 3.9.2. Attributes

##### codeID
Codes, including project specific codes, used in this [AGSi file](./Standard_General_Definitions.md#agsi-file). All codes within each code set shall be unique.   
*Type:* string ([identifier](./Standard_General_Rules.md#1510-identifiers))  
*Condition:* Required  
*Example:* ``UndrainedShearStrength``

##### description
Short description of what the code represents.  
*Type:* string  
*Condition:* Required  
*Example:* ``Undrained shear strength``

##### units
Units of measurement if code represents a property or parameter.   
*Type:* string ([units](./Standard_General_Formats.md#1610-units))  
*Example:* ``kPa``

##### isStandard
`true` if code is from standard dictionary such as the [AGSi code list](./Codes_Codelist.md). If omitted, should be assumed to be false, i.e. project specific or other non-standard code.  
*Type:* boolean  
*Condition:* Recommended  
*Example:* ``true``

##### remarks
Additional remarks, if required.  
*Type:* string  
*Example:* ``Some additional remarks``

