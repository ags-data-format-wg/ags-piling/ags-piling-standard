


# AGS Piling Standard / X. CFA

## X.X. agspileCFAProgress

### X.X.1. Object description

Time based activity and progress record for CFA piles. Typically one record per activity. See [agspileCFAPourRecord](./Standard_CFA_agspileCFAPourRecord.md) for detailed log of concrete delivery and activity. See agspileCFARigData for detailed installation data from rig measurements. Depth fields only need to be completed where a relevant depth has changed during the activity. Unchanged depths may be left blank.

The parent object of [agspileCFAProgress](./Standard_CFA_agspileCFAProgress.md) is [agspileCFAElement](./Standard_CFA_agspileCFAElement.md)

[agspileCFAProgress](./Standard_CFA_agspileCFAProgress.md) has the following attributes:


- [activityStart](#activitystart)
- [activityEnd](#activityend)
- [activityType](#activitytype)
- [description](#description)
- [preboreDepth](#preboredepth)
- [excavationDepth](#excavationdepth)
- [concreteTopDepth](#concretetopdepth)
- [casingDepth](#casingdepth)
- [rftTopDepth](#rfttopdepth)
- [elevationReference](#elevationreference)
- [descriptionElevationReference](#descriptionelevationreference)
- [preboreDetail](#preboredetail)
- [rig](#rig)
- [tool](#tool)
- [operator](#operator)
- [banksman](#banksman)
- [groundwaterObservation](#groundwaterobservation)
- [unexpectedEvent](#unexpectedevent)
- [remarks](#remarks)


### X.X.2. Attributes

##### activityStart
Date/time of start of activity  
*Type:* string (Nonetime)  
*Condition:* Required  
*Example:* ``2016-11-23T14:35``

##### activityEnd
Date/time of end of activity  
*Type:* string (Nonetime)  
*Example:* ``2016-11-23T16:05``

##### activityType
Activity type  
*Type:* string (recommend using term from [vocabulary](./Codes_Vocab.md))  
*Example:* ``Concrete``

##### description
Activity description/method  
*Type:* string  
*Example:* ``CFA concreting``

##### preboreDepth
Depth to base of prebore at end of activity  
*Type:* number  


##### excavationDepth
Depth to base of pile excavation at end of activity  
*Type:* number  
*Example:* ``25``

##### concreteTopDepth
Depth to top of concrete at end of activity  
*Type:* number  
*Example:* ``0``

##### casingDepth
Depth to base of casing at end of activity, for cased CFA piles  
*Type:* number  


##### rftTopDepth
Depth to top of reinforcement (main bars) at end of activity  
*Type:* number  


##### elevationReference
Elevation of reference datum from which depth measured for this record  
*Type:* number  
*Example:* ``16.23``

##### descriptionElevationReference
Description of reference datum from which depth measured for this record  
*Type:* string  
*Example:* ``Ground level``

##### preboreDetail
Prebore diameter and details  
*Type:* string  
*Example:* ``600 auger, backspun out``

##### rig
Rig type and reference (if applicable)  
*Type:* None  
*Example:* ``BG40 (rig 2)``

##### tool
Boring/excavation tool  
*Type:* None  
*Example:* ``Auger``

##### operator
Rig operator / Operator responsible for activity  
*Type:* None  
*Example:* ``J. Smith``

##### banksman
Rig banksman  
*Type:* None  
*Example:* ``J. Jones``

##### groundwaterObservation
Groundwater observations  
*Type:* None  
*Example:* ``Seepage at 8.5m``

##### unexpectedEvent
Delay/obstruction/unexpected event remarks  
*Type:* None  
*Example:* ``Minor collapse at 8.7m``

##### remarks
Additional remarks, if required.  
*Type:* string  
*Example:* ``Some additional remarks``

