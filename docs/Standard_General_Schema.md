# AGS Piling Standard / 1. General

*This is a placeholder page, copied from AGSi. Requires review and edit to make it suitable for AGS Piling.*

## 1.4. Schema

### 1.4.1. Schema requirements

AGSi data shall only be valid if it is arranged in accordance with the schema defined in this standard.

A general high level description of the schema is given below. Full details of schema are given in the following sections:

* [2. Root](./Standard_Root_Intro.md)
* [3. Project](./Standard_Project_Intro.md)
* [4. Model](./Standard_Model_Intro.md)
* [5. Observation](./Standard_Observation_Intro.md)
* [6. Geometry](./Standard_Geometry_Intro.md)
* [7. Data](./Standard_Data_Intro.md)

A diagram showing the full AGSi schema is given below in [1.4.3. Full AGSi schema UML diagram](#143-full-agsi-schema-uml-diagram).

### 1.4.2. Description of schema

The AGSi schema is based on an object model, with **objects** generally representing physical or virtual entities.

Objects have a number of **attributes** (alternatively: properties) that describe the object in more detail.

The proposed schema is hierarchical, i.e. it includes objects embedded (nested) within other objects.

In other cases, relationships between objects are defined by cross-reference links, i.e. links formed via object attributes.

The data that makes up each AGSi file is contained within a single
[root object](./Standard_Root_Intro.md).
Within this [root object](./Standard_Root_Intro.md)
are found two objects that contain general metadata:
[agsSchema](./Standard_Root_agsSchema.md) and
[agsFile](./Standard_Root_agsFile.md).

The remaining schema objects are organised into the following groups according to function:

* [Project](./Standard_Project_Intro.md)
* [Model](./Standard_Model_Intro.md)
* [Observation](./Standard_Observation_Intro.md)
* [Geometry](./Standard_Geometry_Intro.md)
* [Data](./Standard_Data_Intro.md)

The root object contains the top level objects for the
[Project](./Standard_Project_Intro.md) and
[Model](./Standard_Model_Intro.md) groups.

The remaining groups
([Observation](./Standard_Observation_Intro.md),
[Geometry](./Standard_Geometry_Intro.md) and
[Data](./Standard_Data_Intro.md))
comprise 'resource' objects that may be embedded within other objects in the schema, where specified in the object reference documentation.

In addition, some objects are linked to objects in the same or other groups by cross-reference.

The general concept is illustrated in the following diagram.

![Schema overall summary UML diagram](./images/ALL_overview_UML.svg)


### 1.4.3. Full AGSi schema UML diagram

A diagram showing the full AGSi schema is given below.

<a href="../images/ALL_full_UML.svg" target="_blank">Click here to a open full size version of this diagram in a new browser window</a>.

!!! Note
    Guidance on how to interpret the UML diagrams shown in this standard can be found in
    <a href="https://ags-data-format-wg.gitlab.io/AGSi_Documentation/Guidance_General_UML">Guidance > General > How to read AGSi schema diagrams</a>.

![Schema all objects summary UML diagram](./images/ALL_full_UML.svg)
