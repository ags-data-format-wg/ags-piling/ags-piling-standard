


# AGS Piling Standard / X. Common

## X.X. agspileRftShear

### X.X.1. Object description

As built pile shear reinforcement. May be one object per pile covering the entire depth for a simple cage. For cages with shear links that vary with depth, use separate sections for each change. Adjacent sections should not normally overlap. Segmental precast piles should have one object per segment. Top elevation of reinforcement is defined in the parent object.

[agspileRftShear](./Standard_Common_agspileRftShear.md) may be embedded in any of the following potential parent objects: 

- [agspileBoredElement](./Standard_Bored_agspileBoredElement.md)
-  [agspileCFAElement](./Standard_CFA_agspileCFAElement.md)

[agspileRftShear](./Standard_Common_agspileRftShear.md) has the following attributes:


- [topDepth](#topdepth)
- [bottomDepth](#bottomdepth)
- [shearBar](#shearbar)
- [shearPitch](#shearpitch)
- [shearType](#sheartype)
- [shearDetails](#sheardetails)
- [remarks](#remarks)


### X.X.2. Attributes

##### topDepth
Depth to top of section from reinforcement top elevation defined in rftTopElevation in parent object. For top section (of links) this will not normally be zero as links usually start below projection (above COL).  
*Type:* number  
*Example:* ``1.2``

##### bottomDepth
Depth to bottom of section from reinforcement top elevation defined in rftTopElevation in parent object.   
*Type:* number  
*Example:* ``10``

##### shearBar
Shear reinforcement bar type and size  
*Type:* string  
*Example:* ``B12``

##### shearPitch
Shear reinforcement pitch. Units: mm  
*Type:* number  
*Example:* ``200``

##### shearType
Shear reinforcement type (helical/link)  
*Type:* string (recommend using term from [vocabulary](./Codes_Vocab.md))  
*Example:* ``Link``

##### shearDetails
Shear reinforcement special additional details  
*Type:* string  


##### remarks
Additional remarks, if required.  
*Type:* string  
*Example:* ``Some additional remarks``

