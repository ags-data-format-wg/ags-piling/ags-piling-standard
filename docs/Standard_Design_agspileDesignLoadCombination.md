


# AGS Piling Standard / X. Design

## X.X. agspileDesignLoadCombination

### X.X.1. Object description

Used to define the combinations of actions/loads applied in the design.  This object can be used to carry the information provided in FPS e-pile schedule format. Intended for use with Eurocode, but can be used for designs to other codes by providing particular  requirements in remarks.

The parent object of [agspileDesignLoadCombination](./Standard_Design_agspileDesignLoadCombination.md) is [agspileDesignLoad](./Standard_Design_agspileDesignLoad.md)

[agspileDesignLoadCombination](./Standard_Design_agspileDesignLoadCombination.md) has the following attributes:


- [combinationID](#combinationid)
- [description](#description)
- [codeID](#codeid)
- [leadingVariableAction](#leadingvariableaction)
- [accompanyingVariableAction](#accompanyingvariableaction)
- [combinationFactor](#combinationfactor)
- [combination](#combination)
- [remarks](#remarks)


### X.X.2. Attributes

##### combinationID
Reference for this combination. This is optional and not referenced anywhere else in the schema, but it may be beneficial to include it, especially if referenced in the calculations.  
*Type:* string  
*Example:* ``C1a``

##### description
Decscription of combination  
*Type:* string  
*Example:* ``Maximum building load``

##### codeID
Code that identifies the derived combined action or load component. Corresponds to Type in e-pile schedule. Codes should be defined in either an [agsProjectCode](./Standard_Project_agsProjectCode.md) object, or in the code dictionary defined in the relevant [agsProjectCodeSet](./Standard_Project_agsProjectCodeSet.md) object. A code dictionary is available covering codes typically used by Eurocode and the FPS e-pile schedule.  
*Type:* string (Reference to codeID of relevant [agsProjectCode](./Standard_Project_agsProjectCode.md) object, or code defined in codelist specified in relevant [agsProjectCodeSet](./Standard_Project_agsProjectCodeSet.md) object)  
*Example:* ``SetB_DA1_Comb1_ULSSTR_Ed_max``

##### leadingVariableAction
Leading variable action (Eurocode design)  
*Type:* string  
*Example:* ``Qk, imposed``

##### accompanyingVariableAction
Accompanying variable action (Eurocode design)  
*Type:* string  
*Example:* ``Qk, wind``

##### combinationFactor
Combination factor (psi) for accompanying variable action (Eurocode design)  
*Type:* number  
*Example:* ``0.5``

##### combination
Combination, typically as a formula showing actions and partial/combination factors.   
*Type:* string  
*Example:* ``1.35Gk + 1.5Qk,imposed + 0.75Qk,wind``

##### remarks
Further remarks if required  
*Type:* string  
*Example:* ``e.g. source of ground heave``

