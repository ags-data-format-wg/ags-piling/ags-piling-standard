# AGS Piling Standard / X. Codes and vocabulary

*This is a placeholder page, copied from AGSi. Requires rewrite to make it suitable for AGS Piling.*

## 9.1. Code list

This page provides lists of recommended codes for use by [Data group objects](./Standard_Data_Intro.md).

!!! Note
    AGS welcomes feedback including suggestions for new codes for items not yet covered.


### 9.1.1. Parameters

Recommended codes for use by [agsiDataParameterValue](./Standard_Data_agsiDataParameterValue.md).

##### Geotechnical

The following are proposed geotechnical parameter codes,
ordered by category.
This is by no means a complete set of parameters, but it establishes a proposed style for such codes.


Code | Description | Units | Category
---- | ----------- | ----- | --------
`Depth` | Depth | m | General
`Elevation` | Elevation | m | General
`UnitWeightBulk` | Bulk unit weight | kN/m3 | Density
`AngleFriction` | Effective angle of shearing resistance | deg | Strength
`AngleFrictionPeak` | Peak effective angle of shearing resistance | deg | Strength
`AngleFrictionCritical` | Critical state effective angle of shearing resistance | deg | Strength
`AngleFrictionResidual` | Residual effective angle of shearing resistance | deg | Strength
`Cohesion` | Effective cohesion | kPa | Strength
`UndrainedShearStrength` | Undrained shear strength | kPa | Strength
`UndrainedShearStrengthTriaxial` | Undrained shear strength from triaxial tests | kPa | Strength
`UniaxialCompressiveStrength` | Uniaxial Compressive Strength | MPa | Stiffness
`YoungsModulusDrained` | Drained Young's Modulus | MPa | Stiffness
`YoungsModulusUndrained` | Undrained Young's Modulus | MPa | Stiffness
`YoungsModulusDrainedVertical` | Vertical drained Young's Modulus | MPa | Stiffness
`YoungsModulusUndrainedVertical` | Vertical undrained Young's Modulus | MPa | Stiffness
`YoungsModulusDrainedHorizontal` | Horizontal drained Young's Modulus | MPa | Stiffness
`YoungsModulusUndrainedHorizontal` | Horizontal undrained Young's Modulus | MPa | Stiffness
`BulkModulus` | Bulk modulus | MPa | Stiffness
`ShearModulusDrained` | Drained shear Modulus | MPa | Stiffness
`ShearModulusUndrained` | Undrained shear Modulus | MPa | Stiffness
`CBR` | California bearing ratio (CBR) | % | Pavement
`SubgradeSurfaceModulus` | Subgrade surface modulus | MPa | Pavement
`ACECClass` | ACEC Aggressive chemical environment class |  | ACEC
`ACECDSClass` | ACEC Design sulphate class |  | ACEC
`ACECCDCClass` | ACEC Design chemical class |  | ACEC

##### Other domains

!!! Note
    Codes for other domains may be developed in the future.

### 9.1.2. Properties

Recommended codes for use by
[agsiDataPropertyValue](./Standard_Data_agsiDataPropertyValue.md) and
[agsiDataPropertyValueSummary](./Standard_Data_agsiDataPropertySummary.md).

##### Geotechnical / Geoenvironmental

Properties generally relate to factual GI data. Therefore the following is recommended.

Subject to the exceptions and additions described below, the property code shall be the **heading name** for the applicable property defined in the <a href="https://www.ags.org.uk/data-format/" target="_blank">AGS (factual) data format</a>.

Examples:

* `LLPL_PL` for plastic limit
* `ISPT_NVAL` for SPT N value

For geochemical data from the AGS (factual) GCHM group, use the ABBR code used for the determinand (GCHM_CODE).

For geoenvironmental data from the AGS (factual) ELRG, ERES or CNMT group (as applicable, depending on version of AGS format in use), use the ABBR code used for the chemical code (ELRG_CODE, ERES_CODE or CNMT_TYPE as applicable).

Where the AGS (factual) data format heading is insufficient, e.g. reporting of derived values, or summary of values from different types of test, use the codes recommended for parameters.

For depth and elevation where required, e.g. profiles, the following are recommended:

* `Depth` for depth
* `Elevation` for elevation

If additional codes are required, it is recommended that the style adopted for parameter codes is followed.
