


# AGS Piling Standard / X. Common

## X.X. agspileInstallation

### X.X.1. Object description

Installation within pile, including instrumentation installed for pile testing and other monitoring. Further technical details of pile test instruments, e.g. callibration, provided in [agspileTestInstrument](./Standard_PileTest_agspileTestInstrument.md).

[agspileInstallation](./Standard_Common_agspileInstallation.md) may be embedded in any of the following potential parent objects: 

- [agspileBoredElement](./Standard_Bored_agspileBoredElement.md)
-  [agspileCFAElement](./Standard_CFA_agspileCFAElement.md)

[agspileInstallation](./Standard_Common_agspileInstallation.md) has associations (reference links) with the following objects: 

- [agspileTestInstrument](./Standard_PileTest_agspileTestInstrument.md)

[agspileInstallation](./Standard_Common_agspileInstallation.md) has the following attributes:


- [installationID](#installationid)
- [installationReference](#installationreference)
- [installationType](#installationtype)
- [installationSerial](#installationserial)
- [description](#description)
- [topDepth](#topdepth)
- [bottomDepth](#bottomdepth)
- [elevationReference](#elevationreference)
- [descriptionElevationReference](#descriptionelevationreference)
- [installationPosition](#installationposition)
- [installationPositionReference](#installationpositionreference)
- [remarks](#remarks)


### X.X.2. Attributes

##### installationID
Identifer for instrument. Must be unique within file. Pile test instruments may be referenced by [agspileTestInstrument](./Standard_PileTest_agspileTestInstrument.md).  
*Type:* string (identifier)  
*Example:* ``PT1_gauge1``

##### installationReference
Instrument/installation reference, if different froon installationID, i.e. site reference that may not be unique across the project/ file  
*Type:* string  
*Example:* ``g1``

##### installationType
Instrument/installation general type  
*Type:* string (recommend using term from [vocabulary](./Codes_Vocab.md))  
*Example:* ``Vibrating wire strain guage``

##### installationSerial
Serial number/reference  
*Type:* string  
*Example:* ``805471``

##### description
Further details of instrument/installation  
*Type:* string  


##### topDepth
Depth of top of instrument/installation  
*Type:* number  
*Example:* ``6.15``

##### bottomDepth
Depth of base of instrument/installation. Not required if installation is at a discrete depth given in depthTop.  
*Type:* number  
*Example:* ``25.95``

##### elevationReference
Elevation of reference datum from which depth measured for this record  
*Type:* number  
*Example:* ``16.77``

##### descriptionElevationReference
Description of reference datum from which depth measured for this record  
*Type:* string  
*Example:* ``Top of rft``

##### installationPosition
Position of instrument within pile section  
*Type:* string  
*Example:* ``A``

##### installationPositionReference
Description of reference system for position within pile section  
*Type:* string  
*Example:* ``Inside shear link. A aligned NE, BCD evenly spaced clockwise. See sketch in pile test report.``

##### remarks
Additional remarks, if required.  
*Type:* string  
*Example:* ``Some additional remarks``

