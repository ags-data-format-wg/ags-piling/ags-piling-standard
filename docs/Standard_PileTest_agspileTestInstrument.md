


# AGS Piling Standard / X. PileTest

## X.X. agspileTestInstrument

### X.X.1. Object description

Details of pile test instrumentation. May be referenced by [agspileTestCode](./Standard_PileTest_agspileTestCode.md) objects to provide link to readings. 

The parent object of [agspileTestInstrument](./Standard_PileTest_agspileTestInstrument.md) is [agspileTest](./Standard_PileTest_agspileTest.md)

[agspileTestInstrument](./Standard_PileTest_agspileTestInstrument.md) has associations (reference links) with the following objects: 

- [agspileTestCode](./Standard_PileTest_agspileTestCode.md)

[agspileTestInstrument](./Standard_PileTest_agspileTestInstrument.md) has the following attributes:


- [instrumentID](#instrumentid)
- [installationID](#installationid)
- [instrumentType](#instrumenttype)
- [instrumentDetail](#instrumentdetail)
- [serialNumber](#serialnumber)
- [topElevation](#topelevation)
- [bottomElevation](#bottomelevation)
- [position](#position)
- [positionReference](#positionreference)
- [callibrationDate](#callibrationdate)
- [callibrationFactor](#callibrationfactor)
- [callibrationDetails](#callibrationdetails)
- [documentSetID](#documentsetid)
- [remarks](#remarks)


### X.X.2. Attributes

##### instrumentID
Identifier for instrument. Must be unique within file. May be referenced by [agspileTestCode](./Standard_PileTest_agspileTestCode.md).  
*Type:* string (identifier)  
*Condition:* Required  
*Example:* ``PT1_gauge1``

##### installationID
Identifier of related agspileInstallationobject   
*Type:* string (reference to installationID of relevant agspileInstallationobject )  
*Example:* ``P001_Gauge1``

##### instrumentType
Instrument general type  
*Type:* string (recommend using term from [vocabulary](./Codes_Vocab.md))  
*Example:* ``Strain gauge``

##### instrumentDetail
Further details of instrument, if required.  
*Type:* string  
*Example:* ``Vibrating wire strain gauge on sister bar. Model 4911-4.``

##### serialNumber
Instrument serial number  
*Type:* string  
*Example:* ``2001575``

##### topElevation
Elevation of top of instrument,  
*Type:* number  
*Example:* ``9.55``

##### bottomElevation
Elevation of bottom of instrument. Not required if instrument located at discrete elevation.  
*Type:* number  


##### position
Position of instrument within pile section  
*Type:* string  
*Example:* ``A``

##### positionReference
Description of reference system for position within pile section  
*Type:* string  
*Example:* ``Inside shear link. A aligned NE, BCD evenly spaced clockwise. See sketch in pile test report.``

##### callibrationDate
Callibration date  
*Type:* string (ISO date)  
*Example:* ``2020-03-03``

##### callibrationFactor
Callibration factor  
*Type:* number  
*Example:* ``349``

##### callibrationDetails
Further details of callibration  
*Type:* string  


##### documentSetID
Reference to documentation relating to test (reference to [agsProjectDocumentSet](./Standard_Project_agsProjectDocumentSet.md) object).  
*Type:* string (reference to documentSetID of [agsProjectDocumentSet](./Standard_Project_agsProjectDocumentSet.md) object)  
*Example:* ``PT1_Certificates``

##### remarks
Additional remarks if required  
*Type:* string  
*Example:* ``Some additonal remarks``

