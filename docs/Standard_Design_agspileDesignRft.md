


# AGS Piling Standard / X. Design

## X.X. agspileDesignRft

### X.X.1. Object description

Design reinforcement cage for pile/wall element. This is a parent record with general metadata and requirements. Full details of the reinforcement are provided via the embedded child [agspileDesignRftMain](./Standard_Design_agspileDesignRftMain.md) and agspileDesignShear objects. Reinforcement may be specified for each pile individually and referenced to the corresponding pile. Alternatively, generic cages may be defined that can be referenced by many different piles.

The parent object of [agspileDesignRft](./Standard_Design_agspileDesignRft.md) is [agspileDesignSchedule](./Standard_Design_agspileDesignSchedule.md)

[agspileDesignRft](./Standard_Design_agspileDesignRft.md) contains the following embedded child objects: 

- [agspileDesignRftMain](./Standard_Design_agspileDesignRftMain.md)
- [agspileDesignRftShear](./Standard_Design_agspileDesignRftShear.md)

[agspileDesignRft](./Standard_Design_agspileDesignRft.md) has associations (reference links) with the following objects: 

- [agspileDesignElement](./Standard_Design_agspileDesignElement.md)

[agspileDesignRft](./Standard_Design_agspileDesignRft.md) has the following attributes:


- [rftID](#rftid)
- [rftCageReference](#rftcagereference)
- [description](#description)
- [rftCover](#rftcover)
- [rftJoint](#rftjoint)
- [rftReferenceElevation](#rftreferenceelevation)
- [rftDetail](#rftdetail)
- [agspileDesignRftMain](#agspiledesignrftmain)
- [agspileDesignRftShear](#agspiledesignrftshear)
- [remarks](#remarks)


### X.X.2. Attributes

##### rftID
Identifier for this reinforcement record. Must be unique within the project/file. Referenced by [agspileDesignElement](./Standard_Design_agspileDesignElement.md).  
*Type:* string (identifier)  
*Condition:* Required  
*Example:* ``B1-A``

##### rftCageReference
Reinforcement cage reference, if different from rftID, e.g. if cage reference used is not unique within project/file.  
*Type:* string  
*Example:* ``A``

##### description
Further description, if required.  
*Type:* string  
*Example:* ``Cage A - 900mm bearing piles general``

##### rftCover
Nominal minimum cover to reinforcement. Units: mm  
*Type:* number  
*Example:* ``75``

##### rftJoint
Joint or lap requirements for main reinforcement.  
*Type:* string  
*Example:* ``Laps shall be calculated by the contractor based on poor bond conditions``

##### rftReferenceElevation
Reference datum from which depth is measured for child reinforcement records (agspileDesignRftMain amd [agspileDesignRftShear](./Standard_Design_agspileDesignRftShear.md)). Use name of applicable [agspileDesignElement](./Standard_Design_agspileDesignElement.md) attribute.  
*Type:* string (enum from list below)  
\- None  
*Example:* ``cutoffElevation``

##### rftDetail
Other requirements for the reinforcement generally where not covered by other attributes or the embedded objects.  
*Type:* string  


##### agspileDesignRftMain
Details of main (longitudinal) reinforcement. Multiple objects may be defined if reinforcement varies with depth. Array of embedded [agspileDesignRftMain](./Standard_Design_agspileDesignRftMain.md) objects  
*Type:* array (agspileDesignRftMain objects)  


##### agspileDesignRftShear
Details of shear reinforcement. Multiple objects may be defined if reinforcement varies with depth. Array of embedded [agspileDesignRftShear](./Standard_Design_agspileDesignRftShear.md) objects  
*Type:* array (agspileDesignRftShear objects)  


##### remarks
Additional remarks, if required.  
*Type:* string  
*Example:* ``Some additional remarks``

