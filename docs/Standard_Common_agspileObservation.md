


# AGS Piling Standard / X. Common

## X.X. agspileObservation

### X.X.1. Object description

Record of observations made during piling.

[agspileObservation](./Standard_Common_agspileObservation.md) may be embedded in any of the following potential parent objects: 

- [agspileBoredElement](./Standard_Bored_agspileBoredElement.md)
-  [agspileCFAElement](./Standard_CFA_agspileCFAElement.md)

[agspileObservation](./Standard_Common_agspileObservation.md) has the following attributes:


- [observationID](#observationid)
- [observationDateTime](#observationdatetime)
- [topDepth](#topdepth)
- [bottomDepth](#bottomdepth)
- [elevationReference](#elevationreference)
- [descriptionElevationReference](#descriptionelevationreference)
- [observationType](#observationtype)
- [observationDescription](#observationdescription)
- [madeBy](#madeby)
- [remarks](#remarks)


### X.X.2. Attributes

##### observationID
Identifier for this observation or inspection record. May be local to this file or a UUID as required/specified. This is optional and not referenced anywhere else in the schema, but it may be beneficial to include this to help with data control and integrity, and some software/applications may require it.  
*Type:* string (identifier)  


##### observationDateTime
Date/time ofobservation  
*Type:* string (None)  
*Example:* ``2016-11-23T14:35``

##### topDepth
Depth of observation, or depth of top of reported section, as applicable  
*Type:* number  
*Example:* ``7.2``

##### bottomDepth
Depth of base of reported section  
*Type:* number  
*Example:* ``8.5``

##### elevationReference
Elevation of reference datum from which depth measured for this record  
*Type:* number  
*Example:* ``Cut-off level``

##### descriptionElevationReference
Description of reference datum from which depth measured for this record  
*Type:* string  


##### observationType
Type of observation  
*Type:* string  
*Example:* ``Groundwater``

##### observationDescription
Details of observation  
*Type:* string  
*Example:* ``Groundwater inflow at 18m from suspected sand lens. Slowed to seepage after 10 mins. ``

##### madeBy
Name of peron(s) making the observation.  
*Type:* string  
*Example:* ``A N Other``

##### remarks
Additional remarks, if required.  
*Type:* string  
*Example:* ``Some additional remarks``

