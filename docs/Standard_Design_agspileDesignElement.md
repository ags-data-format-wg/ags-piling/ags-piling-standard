


# AGS Piling Standard / X. Design

## X.X. agspileDesignElement

### X.X.1. Object description

Design output for pile/wall elements. May be completed by Employer's designer or Contractor's designer depending on design responsibility assigned (see parent agspileDesign). Not to be used for as built data.

The parent object of [agspileDesignElement](./Standard_Design_agspileDesignElement.md) is [agspileDesignSchedule](./Standard_Design_agspileDesignSchedule.md)

[agspileDesignElement](./Standard_Design_agspileDesignElement.md) has associations (reference links) with the following objects: 

- [agspileDesignRequirement](./Standard_Design_agspileDesignRequirement.md)
- [agspileDesignLoad](./Standard_Design_agspileDesignLoad.md)
- [agspileDesignRft](./Standard_Design_agspileDesignRft.md)
- [agspileBoredElement](./Standard_Bored_agspileBoredElement.md)
- [agspileCFAElement](./Standard_CFA_agspileCFAElement.md)

[agspileDesignElement](./Standard_Design_agspileDesignElement.md) has the following attributes:


- [elementID](#elementid)
- [requirementElementID](#requirementelementid)
- [elementCategory](#elementcategory)
- [elementType](#elementtype)
- [elementSize](#elementsize)
- [infoStatus](#infostatus)
- [group](#group)
- [location](#location)
- [coordinates](#coordinates)
- [chainage](#chainage)
- [offset](#offset)
- [platformElevation](#platformelevation)
- [cutoffElevation](#cutoffelevation)
- [length](#length)
- [bottomElevation](#bottomelevation)
- [verticalityRake](#verticalityrake)
- [verticalityBearing](#verticalitybearing)
- [loadID](#loadid)
- [rftProjection](#rftprojection)
- [rftTopElevation](#rfttopelevation)
- [rftMainDescription](#rftmaindescription)
- [rftShearDescription](#rftsheardescription)
- [rftID](#rftid)
- [installation](#installation)
- [otherRequirement](#otherrequirement)
- [remarks](#remarks)


### X.X.2. Attributes

##### elementID
Pile/wall element reference. Must be unique within all agsPileDesignElement objects within a file, but the ID used here may be re-used in [agspileDesignRequirement](./Standard_Design_agspileDesignRequirement.md) and in constructed pile objects (agspileBored etc.). Referenced by other objects.  
*Type:* string  
*Condition:* Required  
*Example:* ``B1-P001``

##### requirementElementID
Reference to elementID of agspileRequirement object for corresponding element in design requirements schedule, if applicable  
*Type:* string (reference to elementID of [agspileDesignRequirement](./Standard_Design_agspileDesignRequirement.md) object)  
*Example:* ``B1-P001``

##### elementCategory
Category of element, i.e what it is in general term (not to be confused with type of element).  
*Type:* string (recommend using term from [vocabulary](./Codes_Vocab.md))  
*Example:* ``Bearing pile``

##### elementType
Specified element (pile/wall) type(s) input as relevant code from list provided.  
*Type:* string (enum from list below)  
\- None  
*Example:* ``BORED``

##### elementSize
Section size(s). Include shape information if applicable. Units for section size defined in parent agspileDesign object.  
*Type:* string  
*Example:* ``900``

##### infoStatus
Status of information relating to pile/wall element  
*Type:* string (recommend using term from [vocabulary](./Codes_Vocab.md))  
*Example:* ``DRAFT``

##### group
Cap, group or wall reference, as applicable  
*Type:* string  
*Example:* ``PC20``

##### location
Site location subdivision (within project) reference  
*Type:* string  
*Example:* ``Area B``

##### coordinates
As built plan coordinates of the element, as a coordinate tuple (x,y). Coordinate system defined in parent object.  
*Type:* array (coordinate tuple)  
*Example:* ``[565.25,421.09]``

##### chainage
Chainage. Reference alignment defined in parent [agspileDesignSchedule](./Standard_Design_agspileDesignSchedule.md) object.  
*Type:* string  
*Example:* ``12+345.67``

##### offset
Offset from alignment used for chainage. Alignment used defined in parent object.  
*Type:* string  
*Example:* ``+10.7``

##### platformElevation
Assumed elevation of working/piling platform  
*Type:* number  
*Example:* ``16.1``

##### cutoffElevation
Elevation of pile/wall element cut-off level  
*Type:* number  
*Example:* ``15.5``

##### length
Length below cut-off level (measured along pile). Not required if bottomElevation defined. Must be consistent with bottomElevation if both defined.  
*Type:* number  
*Example:* ``25``

##### bottomElevation
Elevation of the bottom/toe of the pile. Not required if length defined. Must be consistent with length if both defined.  
*Type:* number  
*Example:* ``-10.5``

##### verticalityRake
Verticality or gradient of raked pile (as H/V from vertical)  
*Type:* string  
*Example:* ``1/15``

##### verticalityBearing
Bearing of non vertical pile (degrees from project grid y/N axis)  
*Type:* number  
*Example:* ``135``

##### loadID
Reference to loadID of [agspileDesignLoad](./Standard_Design_agspileDesignLoad.md) object that defines the loads and load combinations applicable to this design element. Not required if requirementElementID used (i.e. load defined via [agspileDesignRequirement](./Standard_Design_agspileDesignRequirement.md))  
*Type:* string (reference to loadID of [agspileDesignLoad](./Standard_Design_agspileDesignLoad.md) object)  
*Example:* ``B1-A``

##### rftProjection
Minimum reinforcement projection above cut-off level. Optional if full cage details provided in [agspileDesignRft](./Standard_Design_agspileDesignRft.md). Units: m  
*Type:* number  
*Example:* ``1.2``

##### rftTopElevation
Elevation of top of main reinforcement. Optional if full cage details provided in [agspileDesignRft](./Standard_Design_agspileDesignRft.md).  
*Type:* number  
*Example:* ``16.7``

##### rftMainDescription
Description of main (longitudinal) reinforcement. Optional if full details provided in [agspileDesignRft](./Standard_Design_agspileDesignRft.md).  
*Type:* string  
*Example:* ``12xB25, 10m length incl projection``

##### rftShearDescription
Description of shear reinforcement. Include link type. Optional if full details provided in [agspileDesignRft](./Standard_Design_agspileDesignRft.md).  
*Type:* string  
*Example:* ``B12@200 links``

##### rftID
Reference to rftID of [agspileDesignRft](./Standard_Design_agspileDesignRft.md) object describing the specified reinforcement cage.  
*Type:* string (reference to rftID of [agspileDesignRft](./Standard_Design_agspileDesignRft.md))  
*Example:* ``cageA``

##### installation
Requirements for installations including instrumentation.  
*Type:* string  
*Example:* ``Inclinometer tube. See spec and drawings for details``

##### otherRequirement
Other requirements.  
*Type:* string  


##### remarks
Additional remarks, if required.  
*Type:* string  
*Example:* ``Some additional remarks``

