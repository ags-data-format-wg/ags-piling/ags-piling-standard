# AGS Piling Standard / 3. Project

*This is a placeholder page, copied from AGSi. Requires review and edit to make it suitable for AGS Piling.*

## 3.1. Project group overview

### 3.1.1. Usage

The Project part of the schema contains general projectwide data.
This includes:

* Project metadata
* Investigation metadata
* Coordinate systems used (by the models)
* Documents, that may be referenced by other parts of the schema
* Codes, used by other parts of the schema

!!! Note
    The namespace prefix **ags** is used for the Project group,
    instead of **agsi**, i.e. *agsProject*, not *agsiProject*.
    This is because the agsProject group may eventually be shared with other AGS schema.

#### 3.1.1.1. Projects and investigations

For the purposes of AGSi, the [Project](./Standard_General_Definitions.md#project)
is the specific project/commission under which this [AGSi package](./Standard_General_Definitions.md#agsi-package) has been delivered.

Only one [Project](./Standard_General_Definitions.md#project) may be defined
for each [AGSi package](./Standard_General_Definitions.md#agsi-package).

An [investigation](./Standard_General_Definitions.md#ground-investigation)
is a campaign of fieldwork and/or laboratory testing carried out and reported under a site/ground investigation contract.

Refer to
[3.2.1. Projects and investigations](./Standard_Project_Rules.md#321-projects-and-investigations)
for further details of how projects and investigations should be handled in AGSi.


#### 3.1.1.2. Coordinate systems

This part of the schema is used to define the spatial coordinate system(s) used by the models.

The coordinate system used by a model is considered to be the
[model coordinate system](./Standard_General_Definitions.md#model-coordinate-system),
although this could be an established regional or national system.

A secondary
[global coordinate system](./Standard_General_Definitions.md#global-coordinate-system),
which will normally be an established regional or national system, may also be defined.
This will exist only via transformation from the
[model coordinate system](./Standard_General_Definitions.md#model-coordinate-system).

Refer to
[3.2.2. Coordinate systems](./Standard_Project_Rules.md#322-coordinate-systems)
for further details.

#### 3.1.1.3. Project documents

This part of the schema is used to identify relevant documents including a link to each document. This may be  a relative link to a file included as part of the [AGSi package](./Standard_General_Definitions.md#agsi-package) or a link to an
[external file](./Standard_General_Definitions.md#agsi-external-files),
typically linking to a project document management system that users have will access to.

The documents are organised into sets. These sets can be referenced from other parts of the schema.

AGS (factual) data files are considered to be documents and should be included here.

Refer to [3.2.3. Documents](./Standard_Project_Rules.md#323-documents)
for further details.   

#### 3.1.1.4. Code sets

Some parts of the schema require the use of project specific codes, including:

- objects from the [Data group](./Standard_Data_Intro.md)
- [agsiObservationExpHole](./Standard_Observation_agsiObservationExpHole.md)
- [agsiObservationColumn](./Standard_Observation_agsiObservationColumn.md)

The codes used by any such objects shall be defined here, using
[agsProjectCodeSet](./Standard_Project_agsProjectCodeSet.md) and
[agsProjectCode](./Standard_Project_agsProjectCode.md).
The codes are grouped into sets corresponding to the object/attribute that uses them. The codes used in a set may be individually identified, or reference made to an
[external](./Standard_General_Definitions.md#agsi-external-files)
list of codes.

Refer to
[3.2.4. Codes for Data objects](./Standard_Project_Rules.md#324-codes-for-data-objects) and
[3.2.5. Codes where use of AGS ABBR recommended](./Standard_Project_Rules.md#325-codes-where-use-of-ags-abbr-recommended)
for further details.

### 3.1.2. Summary of schema

This diagram below shows the objects in the Project group and their relationships.

![Project summary UML diagram](./images/agsProject_summary_UML.svg)

[agsProject](./Standard_Project_agsProject.md) includes general metadata for the [Project](./Standard_General_Definitions.md#project) and is a container for all of the other objects described below.

Coordinate systems used by models are defined using [agsProjectCoordinateSystem](./Standard_Project_agsProjectCoordinateSystem.md) objects.
If all models are based on the same coordinate system then only one system (one object) is required.

The [investigations](./Standard_General_Definitions.md#ground-investigation) that the data is based on are defined using
[agsProjectInvestigation](./Standard_Project_agsProjectInvestigation.md)
objects.
Detailed attributed data for the investigation may be included by embedding [agsiDataPropertyValue](./Standard_Data_agsiDataPropertyValue.md) objects.
[agsiDataPropertyFromFile](./Standard_Data_agsiDataPropertyFromFile.md) objects, which reference
[supporting](./Standard_General_Definitions.md#agsi-supporting-files)
data files, can be similarly embedded.

Sets of key project [documents](./Standard_General_Definitions.md#agsi-document) may be identified using
[agsProjectDocumentSet](./Standard_Project_agsProjectDocumentSet.md)
objects. These document sets can be referenced from several different parts of the AGSi schema by referencing the
[*documentSetID*](./Standard_Project_agsProjectDocumentSet.md#documentsetid)
identifier assigned.

The individual [documents](./Standard_General_Definitions.md#agsi-document) making up each set are defined and referenced using
[agsProjectDocument](./Standard_Project_agsProjectDocument.md)
objects. These document objects are collected together (embedded within) the relevant parent [agsProjectDocumentSet](./Standard_Project_agsProjectDocumentSet.md) object.

Sets of codes used by other parts of the schema may be identified using
[agsProjectCodeSet](./Standard_Project_agsProjectCodeSet.md) objects.
The schema object and attribute that uses each code list is identified within this object.

If required, the individual codes making up each set are defined using  [agsProjectCode](./Standard_Project_agsProjectCode.md) objects.
These codes are collected together (embedded within) the relevant parent [agsProjectCodeSet](./Standard_Project_agsProjectCodeSet.md) object.

Refer to [3.2. Project rules and conventions](./Standard_Project_Rules.md)
for further details of how the above should be used in practice.

### 3.1.3. Schema UML diagram

The diagram below shows the full schema of the Project group including all attributes.
It also includes the direct parent and child objects of the Project group.
Reference links to other parts of the AGSi schema are not shown in this diagram.

<a href="../images/agsProject_full_UML.svg" target="_blank">Click here to a open full size version of this diagram in a new browser window</a>.

!!! Note
    Guidance on how to interpret the UML diagrams shown in this standard can be found in
    <a href="https://ags-data-format-wg.gitlab.io/AGSi_Documentation/Guidance_General_UML">Guidance > General > How to read AGSi schema diagrams</a>.


![Project full UML diagram](images/agsProject_full_UML.svg)
