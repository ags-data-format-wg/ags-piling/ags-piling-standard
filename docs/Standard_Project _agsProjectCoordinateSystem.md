


# AGS Piling Standard / 3. Project 

## 3.4. agsProjectCoordinateSystem

### 3.4.1. Object description

Defines the spatial coordinate system(s) used by the models. The coordinate system(s) used by the model is considered to be the [model coordinate system](./Standard_General_Definitions.md#model-coordinate-system), although this could be an established regional or national system. A secondary [global coordinate system](./Standard_General_Definitions.md#global-coordinate-system), which will normally be an established regional or national system, may also be defined (for each model system) but this will only exist via transformation from the [model coordinate system](./Standard_General_Definitions.md#model-coordinate-system). Refer to [3.2.2. Coordinate systems](./Standard_Project_Rules.md#322-coordinate-systems) for further details.

The parent object of [agsProjectCoordinateSystem](./Standard_Project_agsProjectCoordinateSystem.md) is [agsProject](./Standard_Project_agsProject.md)

[agsProjectCoordinateSystem](./Standard_Project_agsProjectCoordinateSystem.md) has associations (reference links) with the following objects: 

- agsiModel

[agsProjectCoordinateSystem](./Standard_Project_agsProjectCoordinateSystem.md) has the following attributes:


- [systemID](#systemid)
- [description](#description)
- [systemType](#systemtype)
- [systemNameXY](#systemnamexy)
- [systemNameZ](#systemnamez)
- [axisNameX](#axisnamex)
- [axisNameY](#axisnamey)
- [axisNameZ](#axisnamez)
- [axisUnitsXY](#axisunitsxy)
- [axisUnitsZ](#axisunitsz)
- [globalXYSystem](#globalxysystem)
- [globalZSystem](#globalzsystem)
- [transformShiftX](#transformshiftx)
- [transformShiftY](#transformshifty)
- [transformXYRotation](#transformxyrotation)
- [transformXYScaleFactor](#transformxyscalefactor)
- [transformShiftZ](#transformshiftz)
- [remarks](#remarks)


### 3.4.2. Attributes

##### systemID
[Identifier](./Standard_General_Rules.md#1510-identifiers) for this coordinate system.  May be local to this file or a [UUID](./Standard_General_Rules.md#1511-uuids) as required/specified. May be referenced by agsiModel. [Identifiers](./Standard_General_Rules.md#1510-identifiers) for systemID shall be unique within an [AGSi file](./Standard_General_Definitions.md#agsi-file). Optional if only one system is being used.   
*Type:* string ([identifier](./Standard_General_Rules.md#1510-identifiers))  
*Example:* ``MetroXYZ``

##### description
Name or short description for this coordinate system.  
*Type:* string  
*Example:* ``3D [model coordinate system](./Standard_General_Definitions.md#model-coordinate-system): Gotham Metro Grid + OS elevations``

##### systemType
Type of system. Only cartesian systems fully supported at present: XYZ (3D), XZ (2D vertical section), XY (2D map), Z (elevation only, i.e. simple layer profiles). For other types of system input `other` and describe in *[description](#description)*.  
*Type:* string (enum from list below)  
\- XYZ  
\- XZ  
\- XY  
\- Z  
\- other  
*Example:* ``XYZ``

##### systemNameXY
Name/description of horizontal coordinate (XY) reference system used for  [model coordinate system](./Standard_General_Definitions.md#model-coordinate-system).  
*Type:* string  
*Condition:* Recommended  
*Example:* ``Gotham Metro Grid``

##### systemNameZ
Name/description of vertical coordinate (Z) reference system used for  [model coordinate system](./Standard_General_Definitions.md#model-coordinate-system).  
*Type:* string  
*Condition:* Recommended  
*Example:* ``Ordnance Datum Newlyn``

##### axisNameX
Axis name for X axis of [model coordinate system](./Standard_General_Definitions.md#model-coordinate-system).  
*Type:* string  
*Condition:* Recommended  
*Example:* ``Easting``

##### axisNameY
Axis name for Y axis of [model coordinate system](./Standard_General_Definitions.md#model-coordinate-system).  
*Type:* string  
*Condition:* Recommended  
*Example:* ``Northing``

##### axisNameZ
Axis name for Z axis of [model coordinate system](./Standard_General_Definitions.md#model-coordinate-system).  
*Type:* string  
*Condition:* Recommended  
*Example:* ``Elevation``

##### axisUnitsXY
Units for X and Y axis (or X axis only if no Y axis). Units are considered to be case sensitive.  
*Type:* string ([units](./Standard_General_Formats.md#1610-units))  
*Condition:* Required  
*Example:* ``m``

##### axisUnitsZ
Units for Z axis (elevation). May include optional prefix and/or suffix as commonly used to identify the datum used. Considered to be case sensitive.  
*Type:* string ([units](./Standard_General_Formats.md#1610-units))  
*Condition:* Required  
*Example:* ``mOD``

##### globalXYSystem
Recognised national or regional horizontal coordinate system that the [model coordinate system](./Standard_General_Definitions.md#model-coordinate-system) can be mapped to. This is intended to facilitate co-ordination with data sets in alternative systems and, in particular, encourage legacy use from archive. Transformation information provided in relevant attributes.  
*Type:* string  
*Example:* ``British National Grid``

##### globalZSystem
Recognised national or regional vertical coordinate system that the [model coordinate system](./Standard_General_Definitions.md#model-coordinate-system) can be mapped to. This is intended to facilitate co-ordination with data sets in alternative systems and, in particular, encourage legacy use from archive. Transformation information provided in relevant attributes.  
*Type:* string  
*Example:* ``Ordnance Datum Newlyn``

##### transformShiftX
Shift in X (or Easting) direction of origin of [model coordinate system](./Standard_General_Definitions.md#model-coordinate-system) relative to [global coordinate system](./Standard_General_Definitions.md#global-coordinate-system), i.e. X value of the model origin in the global system.  
*Type:* number  
*Example:* ``450000``

##### transformShiftY
Shift in Y (or Northing) direction of origin of [model coordinate system](./Standard_General_Definitions.md#model-coordinate-system) relative to [global coordinate system](./Standard_General_Definitions.md#global-coordinate-system), i.e. Y value of the model origin in the global system.  
*Type:* number  
*Example:* ``125000``

##### transformXYRotation
Rotation in anticlockwise direction of [model coordinate system](./Standard_General_Definitions.md#model-coordinate-system) XY axes relative to [global coordinate system](./Standard_General_Definitions.md#global-coordinate-system) XY axes. Units of rotation are decimal degrees.   
*Type:* number (anticlockwise in decimal degrees)  
*Example:* ``1.44450116``

##### transformXYScaleFactor
Scale factor as ratio of distance in [global coordinate system](./Standard_General_Definitions.md#global-coordinate-system) to [model coordinate system](./Standard_General_Definitions.md#model-coordinate-system), i.e. global distance divided by model distance.  
*Type:* number  
*Example:* ``0.9999745653``

##### transformShiftZ
Shift in Z (or Elevation) direction of origin of [model coordinate system](./Standard_General_Definitions.md#model-coordinate-system) relative to [global coordinate system](./Standard_General_Definitions.md#global-coordinate-system), i.e. Z value of the model origin in the global system.  
*Type:* number  
*Example:* ``-100``

##### remarks
Additional remarks if required.  
*Type:* string  
*Example:* ``Some remarks if required``

