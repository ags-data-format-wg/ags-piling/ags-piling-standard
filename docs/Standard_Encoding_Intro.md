# AGS Piling Standard / X. Encoding

*This is a placeholder page, copied from AGSi. Requires review and edit to make it suitable for AGS Piling.*

## X.1. Encoding general

**Encoding** is the process by which the data, arranged in accordance with the schema, is converted to a specified format for transmittal.

The term *encoding* is also used for the end product of this process.

### X.1.1. Permitted methods of encoding

The AGSi schema and this standard have been drafted to facilitate different methods of encoding.

!!! Note
    At present only one method of encoding, JSON, is defined.
    However, data transfer technology and practice is a fast developing field.
    Alternative encodings of AGSi may be supported in the future if there is demand and a clear benefit to users.
    Some data transfer formats, such as
    <a href="https://www.buildingsmart.org/standards/bsi-standards/industry-foundation-classes/" target="_blank">IFC (buildingSMART)</a>
    and some
    <a href="https://www.ogc.org/docs/is" target="_blank">OGC standards</a>
    allow alternative encodings of their schema.

Unless otherwise agreed in advance between the specifier and supplier,
an AGSi file shall only be considered valid if it has been encoded
by one of the methods defined in this standard.

At this time, there is only one defined method of encoding:

  * [JSON encoding](./Standard_Encoding_JSON.md)

### X.1.2. Validation

Some methods of encoding are supported by tools or methodologies
for validation of the encoding in accordance with the encoding standard.

In addition, some methods of encoding facilitate a machine readable
description of the schema which in some cases can be used for validation
of the data set against the schema.

For JSON encoding,
<a href="https://json-schema.org/" target="_blank">JSON Schema</a>
provides a method for describing the schema that can also be used for validation. Validation tools that use JSON Schema are available online.

A [JSON Schema for AGSi](./Standard_Encoding_JSONSchema.md) is available
in support of this standard.
