


# AGS Piling Standard / X. PileTest

## X.X. agspileTest

### X.X.1. Object description

Metadata and results for a pile load test. This object includes a summary of results. Full results are incorporated via embedded child objects.

The parent object of [agspileTest](./Standard_PileTest_agspileTest.md) is agspileConstruction

[agspileTest](./Standard_PileTest_agspileTest.md) contains the following embedded child objects: 

- [agspileTestCode](./Standard_PileTest_agspileTestCode.md)
- [agspileTestInstrument](./Standard_PileTest_agspileTestInstrument.md) & DataBlock objects for load-movement and instrumentation readings

[agspileTest](./Standard_PileTest_agspileTest.md) has associations (reference links) with the following objects: 

- asgspileBoredElement
- [agspileCFAElement](./Standard_CFA_agspileCFAElement.md)
- [agspileDesignElement](./Standard_Design_agspileDesignElement.md)
- [agsProjectDocumentSet](./Standard_Project_agsProjectDocumentSet.md)

[agspileTest](./Standard_PileTest_agspileTest.md) has the following attributes:


- [testID](#testid)
- [testUUID](#testuuid)
- [elementID](#elementid)
- [designID](#designid)
- [description](#description)
- [testStart](#teststart)
- [testEnd](#testend)
- [testType](#testtype)
- [loadDirection](#loaddirection)
- [loadRegime](#loadregime)
- [testProcedure](#testprocedure)
- [reactionSystem](#reactionsystem)
- [unitsLoad](#unitsload)
- [unitsMovement](#unitsmovement)
- [loadDVL](#loaddvl)
- [loadRA](#loadra)
- [loadMaxRequired](#loadmaxrequired)
- [movementDVL](#movementdvl)
- [residualDVL](#residualdvl)
- [movementDVLRA](#movementdvlra)
- [residualDVLRA](#residualdvlra)
- [loadMax](#loadmax)
- [movementMax](#movementmax)
- [residualEnd](#residualend)
- [organisation](#organisation)
- [person](#person)
- [weather](#weather)
- [remarks](#remarks)
- [documentSetID](#documentsetid)
- [loadMovementDataBlock](#loadmovementdatablock)
- [instrumentReadingDataBlock](#instrumentreadingdatablock)
- [agspileTestCode](#agspiletestcode)
- [agspileTestInstrument](#agspiletestinstrument)
- [remarks](#remarks)


### X.X.2. Attributes

##### testID
Test reference. Must be unique within a file if used.  
*Type:* string  
*Condition:* Required  
*Example:* ``Test1``

##### testUUID
Universal/global unique identifier (UUID) for the test. This is optional and is not referenced anywhere else in the schema, but it may be beneficial to include this to help with data control and integrity.  
*Type:* string  
*Example:* ``1fb599ab-c040-408d-aba0-85b18bb506c2``

##### elementID
Identifier (elementID) of the pile/wall element under test.  
*Type:* string (reference to elementID of relevant object (from one of: asgspileBoredElement, [agspileCFAElement](./Standard_CFA_agspileCFAElement.md), agspileDrivenElement))  
*Condition:* Required  
*Example:* ``PT1``

##### designID
Identifier (desigID) of the corresponding pile/wall element in design schedule, if applicable  
*Type:* string (reference to designID of agspileDesign object)  
*Example:* ``PT1``

##### description
Brief description of the test  
*Type:* string  
*Example:* ``Preliminary pile test, pile PT1, 900 x 25m, DVL 6MN, single cycle ML``

##### testStart
Date/time of start of test  
*Type:* string (ISO datetime)  
*Example:* ``2016-11-23T14:35``

##### testEnd
Date/time of end of test  
*Type:* string (ISO datetime)  
*Example:* ``2016-11-23T16:05``

##### testType
Test type: preliminary or working  
*Type:* string (recommend using term from [vocabulary](./Codes_Vocab.md))  
*Example:* ``Preliminary``

##### loadDirection
Test type: load orientation and direction  
*Type:* string (recommend using term from [vocabulary](./Codes_Vocab.md))  
*Example:* ``Vertical compression``

##### loadRegime
Test type: loading regime  
*Type:* string (recommend using term from [vocabulary](./Codes_Vocab.md))  
*Example:* ``Static ML``

##### testProcedure
Test type: procedure  
*Type:* string (recommend using term from [vocabulary](./Codes_Vocab.md))  
*Example:* ``SPERWall3 multi-cyclic``

##### reactionSystem
Short description of reaction system.  
*Type:* string  
*Example:* ``Tension piles (PT1A, PT1B, PT1C and PT1D).``

##### unitsLoad
Units for load for this summary of results.  
*Type:* string (units)  
*Example:* ``kN``

##### unitsMovement
Units for movement for this summary of results.  
*Type:* string (units)  
*Example:* ``mm``

##### loadDVL
Design verification load. Units as defined in unitsLoad.  
*Type:* number  
*Example:* ``6000``

##### loadRA
Representative action (or SWL). Units as defined in unitsLoad.  
*Type:* number  
*Example:* ``2500``

##### loadMaxRequired
Maximum load required. Units as defined in unitsLoad.  
*Type:* number  
*Example:* ``10000``

##### movementDVL
Maximum movement at DVL. Units as defined in unitsMovement.  
*Type:* number  
*Example:* ``4.67``

##### residualDVL
Residual movement after DVL. Units as defined in unitsMovement.  
*Type:* number  
*Example:* ``2.12``

##### movementDVLRA
Maximum movement at DVL + representative load. Units as defined in unitsMovement.  
*Type:* number  
*Example:* ``8.54``

##### residualDVLRA
Residual movement after DVL + representative load. Units as defined in unitsMovement.  
*Type:* number  
*Example:* ``4.69``

##### loadMax
Maximum/final load reached. Units as defined in unitsLoad.  
*Type:* number  
*Example:* ``10250``

##### movementMax
Maximum movement at maximum/final load. Units as defined in unitsMovement.  
*Type:* number  
*Example:* ``16.85``

##### residualEnd
Residual movement at end of test. Units as defined in unitsMovement.  
*Type:* number  
*Example:* ``8.37``

##### organisation
Organisation undertaking the test  
*Type:* string  
*Example:* ``ACME testing``

##### person
Person undertaking the test  
*Type:* string  
*Example:* ``A N Other``

##### weather
Summary of weather conditions during test  
*Type:* string  
*Example:* ``Dry on first day, then rain showers``

##### remarks
General test remarks  
*Type:* string  
*Example:* ``Further remarks as required``

##### documentSetID
Reference to documentation relating to test (reference to [agsProjectDocumentSet](./Standard_Project_agsProjectDocumentSet.md) object).  
*Type:* string (reference to documentSetID of [agsProjectDocumentSet](./Standard_Project_agsProjectDocumentSet.md) object)  
*Example:* ``PT1_TestReport``

##### loadMovementDataBlock
Single DataBlock object used to provide the full load-movement data for the test.  
*Type:* object (single DataBlock object)  


##### instrumentReadingDataBlock
Array of embedded DataBlock objects used to provide instrumentation readings and/or other data from the test.  
*Type:* array (of DataBlock objects)  


##### agspileTestCode
Array of [agspileTestCode](./Standard_PileTest_agspileTestCode.md) providing heading code definitions used by DataBlock objects for load-movement and instrumentation data.  These may be linked to a specific instrument (agspileTest Instrument) if required.  
*Type:* array (of [agspileTestCode](./Standard_PileTest_agspileTestCode.md) objects)  


##### agspileTestInstrument
Array of [agspileTestInstrument](./Standard_PileTest_agspileTestInstrument.md) objects providing details of pile test intrumentation. May be referenced by [agspileTestCode](./Standard_PileTest_agspileTestCode.md) objects to provide link to readings.   
*Type:* array (of [agspileTestInstrument](./Standard_PileTest_agspileTestInstrument.md) objects)  


##### remarks
Additional remarks if required  
*Type:* string  
*Example:* ``Some additonal remarks``

