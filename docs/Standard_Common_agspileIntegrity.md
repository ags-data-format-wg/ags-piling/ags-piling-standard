


# AGS Piling Standard / X. Common

## X.X. agspileIntegrity

### X.X.1. Object description

Details and summary of results from integrity testing

[agspileIntegrity](./Standard_Common_agspileIntegrity.md) may be embedded in any of the following potential parent objects: 

- [agspileBoredElement](./Standard_Bored_agspileBoredElement.md)
-  [agspileCFAElement](./Standard_CFA_agspileCFAElement.md)

[agspileIntegrity](./Standard_Common_agspileIntegrity.md) has the following attributes:


- [integrityReference](#integrityreference)
- [dateTime](#datetime)
- [isRetest](#isretest)
- [testType](#testtype)
- [method](#method)
- [result](#result)
- [resultRemarks](#resultremarks)
- [testOrganisation](#testorganisation)
- [testBy](#testby)


### X.X.2. Attributes

##### integrityReference
Integrity test reference  
*Type:* string  


##### dateTime
Date/time of test  
*Type:* string (None)  
*Example:* ``2016-11-23T14:35``

##### isRetest
Is this a re-test? true if yes.  
*Type:* boolean  
*Example:* ``false``

##### testType
Test type  
*Type:* string (recommend using term from [vocabulary](./Codes_Vocab.md))  
*Example:* ``TDR``

##### method
Test method/equipment details  
*Type:* string  


##### result
Test result, e.g. pass/fail  
*Type:* string  
*Example:* ``Pass``

##### resultRemarks
Test result further details, as required  
*Type:* string  
*Example:* ``Anomaly at 4.7m depth``

##### testOrganisation
Organisation undertaking the test  
*Type:* string  
*Example:* ``ACME testing``

##### testBy
Person undertaking the test  
*Type:* string  
*Example:* ``A N Other``

