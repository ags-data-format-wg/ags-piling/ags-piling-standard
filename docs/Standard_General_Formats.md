# AGS Piling Standard / 1. General

*This is a placeholder page, copied from AGSi. Requires review and edit to make it suitable for AGS Piling.*

## 1.6. Data input formats


This section provides the definitions for specified input data formats applicable across the entire schema.

Where required in the standard (object reference pages), the specified input data format for an attribute is given in brackets after the required data type, e.g.

*Type:* string (required format specification)

General rules on data types, including use of arrays and embedded objects, are given in
[1.5. General rules and conventions](./Standard_General_Rules.md)
(this should be read first).

!!! Note
    Use the menu top right of the page to quickly navigate to the item of interest.

### 1.6.1. Identifier

Any string is permitted, but refer to the requirements relating to identifiers given in
[1.5.10. Identifiers](./Standard_General_Rules.md#1510-identifiers).

Requirements for UUIDs, if used, can be found in
[1.5.11 UUIDs](./Standard_General_Rules.md#1511-uuids).

### 1.6.2. Reference to object ID

The input value shall be the [identifier](./Standard_General_Rules.md#1510-identifiers) (string value) assigned to the specified attribute of the target object instance.

### 1.6.3. Standard term from list

Input value shall be one of the terms given in the list provided.

Whilst AGSi strings are generally considered to be case insensitive, it is recommended that the term be input exactly as given in the list.

!!! Note
    Unfortunately, validation using
    <a href="https://json-schema.org/" target="_blank">JSON Schema</a> assumes case sensitivity. A valid value input  in a different case may be incorrectly rejected during validation. Hence the recommendation.  

### 1.6.4. Recommended term

Use of terms defined in the relevant section of [9.2. Vocabularies](./Codes_Vocab.md) or in the standard or reference identified is recommended.

### 1.6.5. ISO date and/or time

Date, time or date-time (as specified) formatted in accordance with
<a href="https://www.iso.org/iso-8601-date-and-time-format.html" target="_blank">ISO 8601-1:2019 Date and time — Representations for information interchange — Part 1: Basic rules</a>.

Valid formats include:

* `YYYY-MM-DD` for dates
* `hh:mm` for time
* `YYYY-MM-DDThh:mm` for date-time

IS0 8601 allows other variants that may be used.

ISO 8601 requires date and time values to be padded with leading zeros where applicable, e.g. `2020-02-07` and `09:30` are correct whereas `2020-2-7` and `9:30` are incorrect.

AGSi does not require time zone offsets to be used, but users may include them if they wish and in some cases specifiers may require their use.

Specifiers should not impose additional restrictions on date/time formats, i.e. producers should be allowed to use any variant permitted by ISO 8601.

### 1.6.6. URI

Uniform Resource Identifier (URI) formatted in accordance with
<a href="https://tools.ietf.org/html/rfc3986" target="_blank">RFC 3986</a>.

Valid examples include:

* `https://www.somewebsite.com/somefolder/somepage`, a page on a website
* `https://www.somewebsite.com/somefolder/somefile.extn`, a file with a web address

URI should be treated as case sensitive. Not all target addresses are case sensitive, but many are, therefore case sensitivity should be assumed.

A URI **must not contain whitespace**. This is a requirement of RFC 3986. If the target name, file path or address includes one or more spaces then these must be alternatively encoded within the URI. The most common way to do this is to use `%20` to replace each space as described in RFC 3986.  Example:

`https://myaddress with spaces` becomes `https://myaddress%20with%20spaces`

A URI is not allowed to be a relative reference address. [URI-reference](#167-uri-reference) is specified where relative references are permitted.

!!! Note
    A URL (Uniform Resource Locator) is a subset of a URI. In practice, AGSi data is likely to use URLs.

### 1.6.7. URI-reference

Either a [URI](#166-uri) as defined above, or a relative reference, in accordance with
<a href="https://tools.ietf.org/html/rfc3986" target="_blank">RFC 3986</a>.

Similar to URI, a URI-reference should be treated as case sensitive.

Similar to URI, a URI-reference may not contain whitespace. Use `%20` to replace spaces as described in the example for URI above.

Valid examples include:

* the examples given above for [URI](#166-uri)
* `/somefolder/somefile.extn`, a relative reference to a file (address relative to the AGSi file).
* `/some%20folder/somefile%20with%20spaces.extn`, similar but showing how spaces in a file path and/or name must be treated.

### 1.6.8. Coordinate tuple

Where coordinate tuple input is specified, the coordinate values shall be entered as an array of numbers as follows:

    :::JSON
    [x coordinate , y coordinate]

or, for 3D co-ordinates:

    :::JSON
    [x coordinate , y coordinate, z coordinate]



Coordinate values are numeric. Therefore quotemarks are not required for JSON encoding, e.g.

    :::JSON
    [100, 200, 300]

Do not include a leading ` + ` for (positive) elevations, i.e. `42.5` is correct whereas `+42.5` is incorrect.

For spatial coordinates, x, y and z are as defined for the
[model coordinate system](./Standard_General_Definitions.md#model-coordinate-system)
e.g. x = Easting or model grid X, y = Northing or Y and z = Elevation or Z.

In a few cases, x and y have different meanings,
as defined in the attribute description.

Where used, chainages must be input as numeric values, e.g. what may be commonly written as *42+150* (km+m) should be entered as `42150` (if chainage units set as *m*) or `42.150` (if chainage units set as *km*).

### 1.6.9. Profiles or arrays of coordinate tuples

Some attributes require an array of coordinates to define a line or profile, e.g. design lines, section lines. In such cases the coordinate tuples are themselves the items in a parent array. For example:

    :::JSON
    [[x1,y1], [x2,y2], [x3,y3]]

Where applicable, a line will be formed by joining the points in the order that they are listed.

!!! Note
    This format is similar to that used for linestrings in
    <a href="https://geojson.org/" target="_blank">GeoJSON</a> and
    <a href="https://www.ogc.org/standards/sfa" target="_blank">OGC Simple Features</a>.


### 1.6.10. Units

Where required, units should comply with
<a href="https://ucum.org/ucum.html" target="_blank">The Unified Code for Units of Measure (UCUM)</a> wherever possible. UCUM incorporates SI units.

Unless otherwise specified, the ***c/s*** (case sensitive) version defined in UCUM shall be used. The *c/s* version sometimes differs from the *print* version in UCUM.

For properties (used by
[agsiDataPropertyValue](./Standard_Data_agsiDataPropertyValue.md) and
[agsiDataPropertySummary](./Standard_Data_agsiDataPropertySummary.md)
objects) use of the units specified in the
<a href="https://www.ags.org.uk/data-format/" target="_blank">AGS (factual) data format</a>
is recommended, even where these do not fully comply with UCUM. These reflect the reporting units specified in the relevant testing standards currently used in the UK.

For elevation, it is acceptable to use a unit of measure prefixed or suffixed by an abbreviation identifying the elevation datum, e.g. `mOD`.

Units should be considered to be case sensitive. This is an exception to the general rule for string values, as described in
[1.5.2. Case sensitivity](./Standard_General_Rules.md#152-case-sensitivity).


### 1.6.11. AGS ABBR codes / project codes

Some attributes require values that are codes. These codes should be defined using [agsProjectCodeSet](./Standard_Project_agsProjectCodeSet.md) and
[agsProjectCode](./Standard_Project_agsProjectCode.md),
as described in
[3.2.4. Codes for Data objects](./Standard_Project_Rules.md#324-codes-for-data-objects).

Use of the
<a href="https://www.ags.org.uk/data-format/" target="_blank">AGS (factual) data format ABBR codes</a>
for such codes is recommended, especially if these have been used in the corresponding factual data.

If AGS ABBR codes are not used, it is recommended that the codes adopted should differ significantly from the AGS ABBR codes to avoid potential confusion. For example use of `TrialPit` would be preferable to `TP`.

Project requirements for codes should be described in the
[specification](./Standard_General_Specification.md).


### 1.6.12. Other requirements as described

Some attributes may require one-off formats that are fully described in the attribute description/definition.
