# -*- coding: utf-8 -*-
"""
Created on Fri Sep 20 21:16:33 2019

@author: neil.chadwick


"""

############### WHEN DOING UPDATE - EDIT VERSION IN LINE 90 #################

# This script creates Json Schema from spreadsheet.
# It can deal with most clever stuff that we have defined on the spreadsheet
# But if we want to do more exstic things in Json Schema then will need to add them
# (or manually edit the created JsonSchema)

import openpyxl
#import os
from tkinter import Tk, filedialog
import json
import re

# Function for parsing out example when it is an array:
def examplearray(addreq,extxt):
    if 'tuples' in addreq or 'profile' in addreq:
        outerarray = extxt.split('],')
        exarray2 = []
        for innerarray in outerarray:
            innerarray = innerarray.replace('[','')
            innerarray = innerarray.replace(']','')
            exarray = innerarray.split(',')
            exarray = [float(i) for i in exarray]  # string to number
            exarray2.append(exarray)
        return exarray2
    else: #assumes simple array
        extxt = extxt.replace('[','')
        extxt = extxt.replace(']','')
        exarray = extxt.split(',')  # string to number
        if 'number' in addreq or 'tuple' in addreq:
            exarray = [float(i) for i in exarray]
        return exarray
    
    
# Function to extract and clean up object names from requirements
# Also adds the prefix to make the subschema $def reference required
# Note - will fail if no match! TODO - catch this error (albeit should never happen)
    
def getobjectnames(addreq):
    # use regex to find object name which will begin with ags
    objlist = re.findall(r"ags\S*", addreq)
    # Trim trailing commas that may be caught, then put '#/$defs/' at front of each

    deflist = []
    for obj in objlist:
        defobj = '#/$defs/' + obj.replace(',','')
        deflist.append(defobj)
    return deflist
    


########### START OF MAIN SCRIPT #######################
#Get input file
root = Tk()
inputfiles=filedialog.askopenfilenames(initialdir = '',
                                    title = 'Select input files',
                                    filetypes = (('Excel files','*.xls'),
                                                 ('Excel files','*.xlsx')))
#Correct file name hard way until fix found
for f in inputfiles:
    f = f.replace('/','\\')
#inputfolder = os.path.dirname(inputfile)

# Select file to save as (will prompt if already exists)
outputfile=filedialog.asksaveasfilename(initialdir = '',
                                        title = 'Output filename (json)',
                                        defaultextension = '*.json')
# End of input
root.destroy()  # Gets rid of annoying Tk window

# Initialise dictionary we are making, and add top level 'fixed' data
root = {}
# TODO - do we put our schema revision anywhere?
# NOTE currently using version 2019-09 of Json Schema (draft) standard
# This has been superseded by 2020-12, curent as of Oct 2021.
# However, 2020-12 does not seem to be well supported by validators at time of writing
# Therefore we are sticking with the older vesion for now
# Note that use of 'items' chanes in 2020-12 so change will have impact, and
# validateon of our schema to 2020-20 will likley fail
################# CHECK VERSIONS OK BEFORE RUNNING #########################
version = '1.0.0' # Do not include leading v
root['$schema'] = 'https://json-schema.org/draft/2019-09/json-schema-core.html'
root['$id'] = 'https://ags-data-format-wg.gitlab.io/agsi_standard/' + version + '/'
root['title'] = 'AGSi v' + version
root['description'] = 'Association of Geotechnical & Geoenvironmental Specialists transfer format for ground model and interpreted data (AGSi)'
root['type'] = 'object'
root['properties'] = {}
root['additionalProperties'] = False
root['required'] = []
root['$defs']= {}  

# Hard code the overall schema (for now)
root['properties']['agsSchema'] = {}
root['properties']['agsSchema']['$ref'] = '#/$defs/agsSchema'
root['properties']['agsFile'] = {}
root['properties']['agsFile']['$ref'] = '#/$defs/agsFile'
root['properties']['agsProject'] = {}
root['properties']['agsProject']['$ref'] = '#/$defs/agsProject'
root['properties']['agsiModel'] = {}
root['properties']['agsiModel']['type'] = 'array'
root['properties']['agsiModel']['items'] = {}
root['properties']['agsiModel']['items']['$ref'] = '#/$defs/agsiModel'
tmp = {}  # we will use this as a temp dict for root['$defs'] to shorten lines of code
tmpreq = {} # a temporary dict used in special requirements loop

# Create coord tuple etc.
DoThis = True  #switch on/off here!
if DoThis:
    tmp['coordinateTuple'] = {}
    tmp['coordinateTuple']['description'] = 'Sub-schema for a coordinate tuple, referenced from other objects'
    tmp['coordinateTuple']['type'] = 'array'
    tmp['coordinateTuple']['items'] = {}
    tmp['coordinateTuple']['items']['type'] = 'number'
    tmp['coordinateTuple']['items']['type'] = 'number'
    tmp['coordinateTuple']['minItems'] = 2
    tmp['coordinateTuple']['maxItems'] = 3
    tmp['valueProfile'] = {}
    tmp['valueProfile']['description'] = 'Sub-schema for a list of coordinate tuples, referenced from other objects'
    tmp['valueProfile']['type'] = 'array'
    tmp['valueProfile']['items'] = {}
    tmp['valueProfile']['items']['type'] = 'array'
    tmp['valueProfile']['items']['items'] = {}
    tmp['valueProfile']['items']['items']['type'] = 'number'
    tmp['valueProfile']['items']['minItems'] = 2
    tmp['valueProfile']['items']['maxItems'] = 2

# Now for main process, steeping through each input file, and then each sheet in each file
for inputfile in inputfiles:
    #Read workbook
    wb = openpyxl.load_workbook(inputfile, data_only=True)
    sheetnames = wb.sheetnames
    # Read each sheet
    for sheetname in sheetnames:
        ws = wb[sheetname]
        # To filter out junk sheets - restrict to the following...
        if sheetname.startswith('ags') or sheetname in ['schema', 'file']:
            agsiobj = sheetname
            tmp[agsiobj] = {}
             # In the follwing, check col 1 is as expected - ignore if not
            if str(ws.cell(row=2, column=1).value).lower().startswith('description'):
                objdesc = str(ws.cell(row=2, column=2).value)
                tmp[agsiobj]['description'] = objdesc
            # TODO - indent here?
            tmp[agsiobj]['type'] = 'object'
            tmp[agsiobj]['properties'] = {}
            tmp[agsiobj]['additionalProperties'] = False
            tmp[agsiobj]['required'] = []
            if str(ws.cell(row=6, column=1).value).lower().startswith('attribute'):
                # This is the header row for the table, triggers table loop
                # For now we assume correct order of columns!
                #Get first attribute (r set to starting row)
                r = 7
                attribute = str(ws.cell(row=r, column=1).value)
                while attribute != 'None':  #Do until blank attribute name reached!
                    # Start to populate dictionary - create object first
                    tmp[agsiobj]['properties'][attribute]= {}
                    desc = str(ws.cell(row=r, column=5).value)
                    tmp[agsiobj]['properties'][attribute]['description'] = desc
                    atype = str(ws.cell(row=r, column=2).value).lower()
                    tmp[agsiobj]['properties'][attribute]['type'] = atype
                    example = ws.cell(row=r, column=6).value
                    addreq = str(ws.cell(row=r, column=3).value)
                    isreq = str(ws.cell(row=r, column=4).value)
                    # Process additional requirements
                    # Note the enum list must be a CSV list in column 8 (H)
                    if 'enum' in addreq.lower():
                        if 'list' in addreq.lower():
                            print('list')
                            liststring = str(ws.cell(row=r, column=8).value)
                            listitems = liststring.split(',')
                            tmp[agsiobj]['properties'][attribute]['enum'] = []
                            tmp[agsiobj]['properties'][attribute]['enum'] = listitems
                        elif 'reference' in addreq.lower():
                            # TODO - do we use this anywhere. What is it!
                            reference = str(ws.cell(row=r, column=8).value)
                            if reference != 'None':
                                tmp[agsiobj]['properties'][attribute]['$ref'] = reference
                    # Process varies by attribute type
                    if atype == 'string':
                        if addreq.lower() in ['iso date', 'iso time', 'iso date-time', 'uri', 'uri-reference']:
                            addreqtemp = addreq.lower().replace('iso ','')
                            tmp[agsiobj]['properties'][attribute]['format'] = addreqtemp
                        elif 'required' in isreq.lower():
                            if 'empty' not in isreq.lower():
                                tmp[agsiobj]['properties'][attribute]['minLength'] = 1
                    elif atype == 'object':
                        aobjectlist = getobjectnames(addreq)
                        if len(aobjectlist) > 1:  # If more than one, need to use anyOf
                            #target = ' + '.join(aobjectlist)
                            tmp[agsiobj]['properties'][attribute]['anyOf'] = []
                            for aobjectname in aobjectlist:
                                tmp[agsiobj]['properties'][attribute]['anyOf'].append({'$ref' : aobjectname}) 
                        else:
                            tmp[agsiobj]['properties'][attribute]['$ref'] = aobjectlist[0]
                    elif atype == 'array': # For an array, we have a number of possibilities!
                        if addreq.lower() in ['string','number','boolean']:
                           tmp[agsiobj]['properties'][attribute]['items'] = {}
                           tmp[agsiobj]['properties'][attribute]['items']['type'] = addreq.lower()
                        elif 'reference to' in addreq.lower():
                            tmp[agsiobj]['properties'][attribute]['items'] = {}
                            tmp[agsiobj]['properties'][attribute]['items']['type'] = 'string'
                        elif 'object' in addreq.lower():
                            aobjectlist = getobjectnames(addreq)
                            if len(aobjectlist) > 1:  # If more than one, need to use anyOf (not used at present)
                                tmp[agsiobj]['properties'][attribute]['items'] = {}
                                tmp[agsiobj]['properties'][attribute]['items']['anyOf'] = []
                                for aobjectname in aobjectlist:
                                    tmp[agsiobj]['properties'][attribute]['items']['anyOf'].append({'$ref' : aobjectname}) 
                            else:
                                tmp[agsiobj]['properties'][attribute]['items'] = {}
                                tmp[agsiobj]['properties'][attribute]['items']['$ref'] = aobjectlist[0]
                        elif addreq.lower() == 'coordinate tuple':
                            tmp[agsiobj]['properties'][attribute]['$ref'] = '#/$defs/coordinateTuple'
                        # TODO check this - not sure about my change
                        elif addreq.lower() == 'of coordinate tuples':
                            tmp[agsiobj]['properties'][attribute]['items'] = {}
                            tmp[agsiobj]['properties'][attribute]['items']['$ref'] = '#/$defs/valueProfile'
                        elif addreq.lower() == 'profile':
                            tmp[agsiobj]['properties'][attribute]['$ref'] = '#/$defs/valueProfile'
                    # Back to main event. Now do the Example
                    if example is not None:
                        if atype == 'array':
                            exarray = examplearray(addreq,example)
                            tmp[agsiobj]['properties'][attribute]['example'] = exarray
                        else:
                            tmp[agsiobj]['properties'][attribute]['example'] = example
                    # Add to list if REQUIRED field (special cases dealt with later)
                    if isreq.lower() == 'required':
                        tmp[agsiobj]['required'].append(attribute)
                    # All done, now increment row for while loop
                    r = r + 1
                    attribute = str(ws.cell(row=r, column=1).value)
                    ##### end of while loop #####

        # Before we leave the sheet/object, need to process special 'required' incl blank
            condition = str(ws.cell(row=2, column=7).value)  # defined to right of main spreadsheet table, at top
            if condition in ['anyOf', 'allOf', 'oneOf']:
                del tmp[agsiobj]['required'] # need to get rid of this first
                r = 3
                attrliststr = str(ws.cell(row=r, column=7).value)
                tmpreq[agsiobj] = {}
                tmp[agsiobj][condition] = []
                while attrliststr != 'None':
                    attrlist = attrliststr.split(',')
                    tmpreq[agsiobj][str(r)] = {}
                    tmpreq[agsiobj][str(r)]['required'] = attrlist
                    tmp[agsiobj][condition].append(tmpreq[agsiobj][str(r)])
                    r = r + 1
                    attrliststr = str(ws.cell(row=r, column=7).value)
            elif len(tmp[agsiobj]['required']) == 0: #If empty, we don't want it in the Json
                del tmp[agsiobj]['required']

        # ALL DONE. Now map to correct dict for geneating the Json
        root['$defs'] = tmp

        # Compile to md and save as text file
        #Write Json from dict to file
        with open(outputfile, 'w') as jsonfile:
            json.dump(root, jsonfile, indent=4)
