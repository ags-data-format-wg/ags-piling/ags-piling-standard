# -*- coding: utf-8 -*-
"""
Created on Wed Dec 13 17:24:57 2023

@author: DigitalGeotechnical
"""

# -*- coding: utf-8 -*-
"""
Created on Fri Sep 20 21:16:33 2019
Modified for AGS Piling from 13/12/23

@author: neil.chadwick


"""

#TODO
# change case of true/false for bool example

#TODO
#obj names italics missed in a few pages when in text
# other sheets
#where do we put extra guidance?

import openpyxl
import os
from tkinter import Tk, filedialog
import csv

from replacement import replaceObjectLink, replaceSimple


#Get input file
root = Tk()
inputfile=filedialog.askopenfilename(initialdir = '',
                                    title = 'Select input file',
                                    filetypes = (('Excel files','*.xls'),
                                                 ('Excel files','*.xlsx')))

#Read workbook
wb = openpyxl.load_workbook(inputfile, data_only=True)
sheetnames = wb.sheetnames

for sheet in sheetnames:
    print(sheet)