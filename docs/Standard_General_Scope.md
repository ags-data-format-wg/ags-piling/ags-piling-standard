# AGS Piling Standard / 1. General

*This is a placeholder page, copied from AGSi. Requires review and edit to make it suitable for AGS Piling.*

## 1.3. Scope

### 1.3.1. Scope of AGSi

AGSi is intended for use in the **ground**
[domain](./Standard_General_Definitions.md#domain-of-model)
for the sharing or transfer of
[interpreted data](./Standard_General_Definitions.md#interpreted-data)
including, but not limited to,
[ground models](./Standard_General_Definitions.md#ground-model).


In an AGSi context a
[domain](./Standard_General_Definitions.md#domain-of-model)
is a specified sphere of activity or knowledge.
The **ground domain** is a parent
[domain](./Standard_General_Definitions.md#domain-of-model)
that includes:

* geotechnical engineering
* geology
* hydrogeology
* geoenvironmental
* other supporting disciplines

!!! Note
    AGSi is intended to serve all of the above, but the initial development work has focused on geotechnical engineering applications which are expected to provide the main use case.  

The data collected from ground investigation and monitoring is considered to be
[factual data](./Standard_General_Definitions.md#factual-data).
 This data is then collated, studied and processed to create
[interpreted data](./Standard_General_Definitions.md#interpreted-data).
The outputs from interpretation may include the following:

* Succession of strata/units in the ground, including:
    * Plots and/or sections showing the strata/units encountered at each exploratory hole
    * Estimation of location of boundaries of strata/units between exploratory holes
    * Recommended location and extent of strata/units for analysis and design

* Test data (in situ or laboratory), including:
    * Statistical summaries of the factual data (range, mean, etc.)
    * Derived data, based on correlations
    * Plots of the data
    * Recommended parameters for analysis and design

* Monitoring data (piezometric or environmental):
    * Plots of the data
    * Appraisal of readings, e.g. range, or comparison against thresholds, as applicable
    * Recommended piezometric levels/pressures for design
    * Recommendations in relation to environmental data



An output from interpretation may be one or more
[ground models](./Standard_General_Definitions.md#ground-model),
a digital visual representation of the ground identifying the different strata/units, often incorporating linked data. Ground models are typically 3D, but 2D cross sections and simple 1D profiles are also considered to be ground models for the purposes of AGSi.

There are potentially many different types of ground model that can be created, and there may be several models for each project. The
<a href="https://ags-data-format-wg.gitlab.io/AGSi_Documentation/Guidance_Model_Types">Guidance</a>
provides further information on the different types of model. In particular, attention is drawn to the difference between
[observational models](./Standard_General_Definitions.md#observational-model)
and
[analytical (or design) models](./Standard_General_Definitions.md#analytical-model).

AGSi is intended to be used for the process and output from interpretation.

AGSi should not be used for the transfer of factual data from ground investigations or monitoring. The <a href="https://www.ags.org.uk/data-format/" target="_blank">AGS (factual) data format</a>
should be used for transfer of factual data. The relevant AGS (factual) data files should be identified as documents linked/referenced from AGSi.

It is sometimes useful to include selected field observations or test results from ground investigations in models. These will sometimes comprise factual data although in other cases the data may be interpreted having have undergone some processing. Examples include:

  * Record of strata/units encountered in exploratory holes, which may incorporate re-interpretation of the original data
  * SPT N values, which may incorporate extrapolation and/or energy ratio correction

Inclusion of field observations or test results for visualisation within a model is permitted in AGSi using the
[Observation group](./Standard_Observation_Intro.md).
However, it is recommended that such data only be provided where it adds value to the model.

Where
[parameters](./Standard_General_Definitions.md#parameter)
for analysis/design are shared using AGSi, the type of parameter to be shared should be determined and agreed by the user(s). It is recommended that parameters for analysis/design shared should be Eurocode 7 characteristic values or equivalent, not (factored) Eurocode 7 design values.

### 1.3.2. Scope exclusions

The processes of interpretation and modelling and their outputs are outside the scope of this standard.

Details of construction works, whether existing or proposed (i.e. the design), are not covered by AGSi.

!!! Note
    It is common for ground models to incorporate earthworks, i.e. cut/fill and proposed ground surface.
    Whilst construction is outside the scope of AGSi, it is recognised this is a reasonable use case for AGSi.
    However, it should be noted that there may be aspects of earthworks construction that are not fully covered by AGSi.


!!! Note
    Several existing open standards cover modelling of construction works, e.g.
    <a href="https://www.buildingsmart.org/standards/bsi-standards/industry-foundation-classes/" target="_blank">IFC (buildingSMART)</a> and
    <a href="https://www.ogc.org/docs/is" target="_blank">OGC standards</a>.

    AGSi is targeted at the modelling of the ground itself, i.e rock, soil, water, etc.
    The intention is for AGSi to provide a standard for ground models that complements the existing open standards for the exchange of building and infrastructure models, improving interoperability in the BIM/GIS environment.   

Detailed modelling of man-made obstructions within the ground is outside the scope of AGSi. However, AGSi may be used for simplified models of obstructed ground, e.g. identification of areas/volumes with different risk profiles.

AGSi may be used to share ground
[parameters](./Standard_General_Definitions.md#parameter)
for analysis/design where assessment of these is required as part of the interpretation.
AGSi should not be used to share parameters that are outside of the scope of the interpretation, such as correlation factors that are to be determined by the designer.
