


# AGS Piling Standard / X. Design

## X.X. agspileDesignSchedule

### X.X.1. Object description

Package of pile design, covering inputs and output. Full details of the piles covered by this package are incorporated as embedded child objects.  Includes metadata for the design activity. Includes Employer Requirements for pile design. Used for both Employer and Contractor pile design. There may be more than one agspileDesign object instance in a project.

The parent object of [agspileDesignSchedule](./Standard_Design_agspileDesignSchedule.md) is [root](./Standard_Root_Intro.md)

[agspileDesignSchedule](./Standard_Design_agspileDesignSchedule.md) contains the following embedded child objects: 

- [agspileDesignRequirement](./Standard_Design_agspileDesignRequirement.md)
- [agspileDesignElement](./Standard_Design_agspileDesignElement.md)
- [agspileDesignLoad](./Standard_Design_agspileDesignLoad.md)
- [agspileDesignRft](./Standard_Design_agspileDesignRft.md)

[agspileDesignSchedule](./Standard_Design_agspileDesignSchedule.md) has associations (reference links) with the following objects: 

- [agsProjectCoordinateSystem](./Standard_Project_agsProjectCoordinateSystem.md)
- [agsProjectDocumentSet](./Standard_Project_agsProjectDocumentSet.md)

[agspileDesignSchedule](./Standard_Design_agspileDesignSchedule.md) has the following attributes:


- [designID](#designid)
- [designName](#designname)
- [description](#description)
- [coordSystemID](#coordsystemid)
- [alignmentDescription](#alignmentdescription)
- [sectionSizeUnits](#sectionsizeunits)
- [typeSeparator](#typeseparator)
- [signConvention](#signconvention)
- [designStandard](#designstandard)
- [loadUnits](#loadunits)
- [responsibilityScheme](#responsibilityscheme)
- [responsibilityMethod](#responsibilitymethod)
- [responsibilityElements](#responsibilityelements)
- [employer](#employer)
- [employerDesigner](#employerdesigner)
- [contractor](#contractor)
- [contractorDesigner](#contractordesigner)
- [agspileDesignRequirement](#agspiledesignrequirement)
- [agspileDesignElement](#agspiledesignelement)
- [agspileDesignLoad](#agspiledesignload)
- [agspileDesignRft](#agspiledesignrft)
- [documentSetID](#documentsetid)
- [remarks](#remarks)


### X.X.2. Attributes

##### designID
Identifier, possibly a UUID. This is optional and is not referenced anywhere else in the schema, but it is beneficial to include this to help with data control and integrity, and some software/applications may require it.  
*Type:* string  
*Example:* ``Ph1Design``

##### designName
Short name of design activity, e.g. package name, for piles embedded in this object.  
*Type:* string  
*Example:* ``Phase 1 design``

##### description
Further description, if required.  
*Type:* string  
*Example:* ``Phase 1 pile design``

##### coordSystemID
Reference to coordinate system applicable to this object including child objects (agsProjectCoordinateSystem object). Coordinate system defines datum for elevations and units for pile length and depth.  
*Type:* string (reference to systemID of  [agsProjectCoordinateSystem](./Standard_Project_agsProjectCoordinateSystem.md) object)  
*Example:* ``MetroXYZ``

##### alignmentDescription
Description of alignment used to define chainage and offset in child element objects  
*Type:* string  


##### sectionSizeUnits
Units for section size used for piles in [agspileDesignRequirement](./Standard_Design_agspileDesignRequirement.md) and [agspileDesignElement](./Standard_Design_agspileDesignElement.md)  
*Type:* string (units)  
*Example:* ``mm``

##### typeSeparator
Text character used to separate items in list of permitted pile types in child agsileDesignRequirement object  
*Type:* string  
*Example:* ``+``

##### signConvention
Clairification of sign conventions used, if required. Typically applicable to load direction, especially lateral and moment loads.  
*Type:* string  


##### designStandard
Code(s) and/or standard(s) that this design has been prepared in accordance with  
*Type:* string  
*Example:* ``EC7``

##### loadUnits
Units for load used by agspileDesignLoadComponent objects  
*Type:* string (units)  
*Example:* ``kN``

##### responsibilityScheme
Party with responsibility for design of foundation scheme inlcuding representative loads on piles and pile locations. Employer or Contractor.  
*Type:* string (recommend using term from [vocabulary](./Codes_Vocab.md))  
*Example:* ``Employer``

##### responsibilityMethod
Party with responsibility for choice of piling or walling method. Employer or Contractor.  
*Type:* string (recommend using term from [vocabulary](./Codes_Vocab.md))  
*Example:* ``Employer``

##### responsibilityElements
Party with responsibility for design of piles or wall elements to carry specific loadings. Employer or Contractor.  
*Type:* string (recommend using term from [vocabulary](./Codes_Vocab.md))  
*Example:* ``Contractor``

##### employer
Employer  
*Type:* string  


##### employerDesigner
Employer's designer  
*Type:* string  


##### contractor
Contractor. Only applicable if Contractor has a design role.  
*Type:* string  


##### contractorDesigner
Contractor's designer. Only applicable if Contractor has a design role.  
*Type:* string  


##### agspileDesignRequirement
Array of embedded [agspileDesignRequirement](./Standard_Design_agspileDesignRequirement.md) objects typically used to provide Employer's Requirements for contractor design. Requirements may be generic or specified for individial piles  
*Type:* array (of [agspileDesignRequirement](./Standard_Design_agspileDesignRequirement.md) objects)  


##### agspileDesignElement
Array of embedded [agspileDesignElement](./Standard_Design_agspileDesignElement.md) objects used to define loads to be applied. Individual load components and required load combinations covered by embedded child objects.  
*Type:* array (of [agspileDesignElement](./Standard_Design_agspileDesignElement.md) objects)  


##### agspileDesignLoad
Array of embedded [agspileDesignLoad](./Standard_Design_agspileDesignLoad.md) objects typically used to provide Design loads to be applied to pile/wall elements. Loads may be generic or specified for individial piles  
*Type:* array (of [agspileDesignLoad](./Standard_Design_agspileDesignLoad.md) objects)  


##### agspileDesignRft
Array of embedded [agspileDesignRft](./Standard_Design_agspileDesignRft.md) objects used to provide full details of design intent for pile reinforcement.  
*Type:* array (of [agspileDesignRft](./Standard_Design_agspileDesignRft.md) objects)  


##### documentSetID
Reference to documentation relating to design (reference to [agsProjectDocumentSet](./Standard_Project_agsProjectDocumentSet.md) object), e.g. specification, schedules, drawings, design reports.  
*Type:* string (reference to documentSetID of [agsProjectDocumentSet](./Standard_Project_agsProjectDocumentSet.md) object)  
*Example:* ``Ph1PileDesign``

##### remarks
Additional remarks, if required.  
*Type:* string  
*Example:* ``Some additional remarks``

