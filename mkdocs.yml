site_name: 'AGS Piling (Draft in development)'
site_url: 'https://ags-data-format-wg.gitlab.io/AGSPiling_Standard'

site_description: 'AGS Piling Standard'
site_author: 'AGS DMWG'

theme:
  name: 'material'
  palette:
    primary: 'blue'
    accent: 'blue'
  logo: 'images/AGSPiling_Logo.png'
  favicon: 'images/AGSPiling_favicon.png'
  feature:
    tabs: false

extra_javascript:
  - js/version-select.js

extra:
  font: false
  version:
    provider: mike

# Copyright
copyright: 'Copyright &copy; 2023 AGS'


extra_css:
    - 'stylesheets/extra.css'

nav:
- 'Home': https://ags-data-format-wg.gitlab.io/AGSPiling_Documentation/index
- 'Standard':
   - '1. General':
      - 'Version information': 'Standard_General_Version.md'
      - '1.1. Introduction': 'index.md'
      - '1.2. Definitions': 'Standard_General_Definitions.md'
      - '1.3. Scope': 'Standard_General_Scope.md'
      - '1.4. Schema': 'Standard_General_Schema.md'
      - '1.5. General rules and conventions': 'Standard_General_Rules.md'
      - '1.6. Data input formats': 'Standard_General_Formats.md'
      - '1.7. Specification': 'Standard_General_Specification.md'
      - '1.8. Transfer of AGS Piling data': 'Standard_General_Transfer.md'
   - '2. Root':
      - '2.1. Root schema overview': 'Standard_Root_Intro.md'
      - '2.2. agsSchema': 'Standard_Root_agsSchema.md'
      - '2.3. agsFile': 'Standard_Root_agsFile.md'
   - '3. Project':
      - '3.1. Project group overview': 'Standard_Project_Intro.md'
      - '3.2. Project rules and conventions': 'Standard_Project_Rules.md'
      - '3.3. agsProject': 'Standard_Project_agsProject.md'
      - '3.4. agsProjectCoordinateSystem': 'Standard_Project_agsProjectCoordinateSystem.md'
      - '3.5. agsProjectDocumentSet': 'Standard_Project_agsProjectDocumentSet.md'
      - '3.6. agsProjectDocument': 'Standard_Project_agsProjectDocument.md'
      - '3.7. agsProjectCodeSet': 'Standard_Project_agsProjectCodeSet.md'
      - '3.8. agsProjectCode': 'Standard_Project_agsProjectCode.md'
   - 'X. Design':
      - 'X.X. agspileDesignSchedule': 'Standard_Design_agspileDesignSchedule.md'
      - 'X.X. agspileDesignRequirement': 'Standard_Design_agspileDesignRequirement.md'
      - 'X.X. agspileDesignElement': 'Standard_Design_agspileDesignElement.md'
      - 'X.X. agspileDesignLoad': 'Standard_Design_agspileDesignLoad.md'
      - 'X.X. agspileDesignLoadAction': 'Standard_Design_agspileDesignLoadAction.md'
      - 'X.X. agspileDesignLoadCombination': 'Standard_Design_agspileDesignLoadCombination.md'
      - 'X.X. agspileDesignRft': 'Standard_Design_agspileDesignRft.md'
      - 'X.X. agspileDesignRftMain': 'Standard_Design_agspileDesignRftMain.md'
      - 'X.X. agspileDesignRftShear': 'Standard_Design_agspileDesignRftShear.md'
   - 'X. Construction':
      - 'X.X. agspileConstructionSchedule': 'Standard_Construction_agspileConstructionSchedule.md'
   - 'X. Pile test':
      - 'X.X. agspileTest': 'Standard_PileTest_agspileTest.md'
      - 'X.X. DataBlock for Load-Movement ': 'Standard_PileTest_DataBlock for Load-Movement .md'
      - 'X.X. DataBlock for Instr Data': 'Standard_PileTest_DataBlock for Instr Data.md'
      - 'X.X. agspileTestCode': 'Standard_PileTest_agspileTestCode.md'
      - 'X.X. agspileTestInstrument': 'Standard_PileTest_agspileTestInstrument.md'
   - 'X. Bored':
      - 'X.X. agspileBoredElement': 'Standard_Bored_agspileBoredElement.md'
      - 'X.X. agspileBoredProgress': 'Standard_Bored_agspileBoredProgress.md'
      - 'X.X. agspileBoredDepth': 'Standard_Bored_agspileBoredDepth.md'
      - 'X.X. agspileBoredPourRecord': 'Standard_Bored_agspileBoredPourRecord.md'
   - 'X. CFA':
      - 'X.X. agspileCFAElement': 'Standard_CFA_agspileCFAElement.md'
      - 'X.X. agspileCFAProgress': 'Standard_CFA_agspileCFAProgress.md'
      - 'X.X. agspileCFADepth': 'Standard_CFA_agspileCFADepth.md'
      - 'X.X. agspileCFAPourRecord': 'Standard_CFA_agspileCFAPourRecord.md'
      - 'X.X. DataBlock for CFA rig data': 'Standard_CFA_DataBlock for CFA rig data.md'
      - 'X.X. agspileCFACode': 'Standard_CFA_agspileCFACode.md'
   - 'X. Material':
      - 'X.X. agspileConcrete': 'Standard_Material_agspileConcrete.md'
      - 'X.X. agspileConcreteStrengthTest': 'Standard_Material_agspileConcreteStrengthTest.md'
      - 'X.X. agspileFluid': 'Standard_Material_agspileFluid.md'
   - 'X. Common':
      - 'X.X. agspileGeology': 'Standard_Common_agspileGeology.md'
      - 'X.X. agspileRftMain': 'Standard_Common_agspileRftMain.md'
      - 'X.X. agspileRftShear': 'Standard_Common_agspileRftShear.md'
      - 'X.X. agspileInstallation': 'Standard_Common_agspileInstallation.md'
      - 'X.X. agspileIntegrity': 'Standard_Common_agspileIntegrity.md'
      - 'X.X. agspileObservation': 'Standard_Common_agspileObservation.md'
      - 'X.X. agspileInspection': 'Standard_Common_agspileInspection.md'
      - 'X.X. agspileNonConformance': 'Standard_Common_agspileNonConformance.md'
      - 'X.X. agspileDataValue': 'Standard_Common_agspileDataValue.md'

   - 'X. Encoding':
      - 'X.1. Encoding general': 'Standard_Encoding_Intro.md'
      - 'X.2. JSON encoding': 'Standard_Encoding_JSON.md'
      - 'X.3. JSON Schema': 'Standard_Encoding_JSONSchema.md'
   - 'X. Codes and vocabulary':
      - 'X.1. Code list': 'Codes_Codelist.md'
      - 'X.2. Vocabulary': 'Codes_Vocab.md'
- 'Guidance': https://ags-data-format-wg.gitlab.io/AGSPiling_Documentation/Guidance_Intro/

markdown_extensions:
    - admonition
    - footnotes
    - codehilite
    - pymdownx.highlight
    - pymdownx.inlinehilite
    - pymdownx.superfences
    - pymdownx.snippets
    - toc:
        permalink: true
